$(function () {
    $('body').on('click', '.user-block', function (e) {
        e.preventDefault();
        var tr= $(this).closest('tr');
        var key = tr.attr('data-key');
        var url = "/admin/user-x/block";
        var _this=this;
        var msg = prompt('Объясните блокировку', '');
        $.ajax({
            url: url,
            type: 'get',
            data:{id:key,message:msg},
            dataType: 'json',
            success: function (data) {
                console.log(data );
                if (data.result == 'ok') {
                    tr.children('[data-col-seq=9]').text('blocked');
                    $(_this).html('<span class="glyphicon  glyphicon-lock blocked"></span>')
                        .removeClass('user-block')
                        .addClass('user-deblock')
                        .attr('title', 'Разблокировать');
                } else {
                    alert(data.message);
                }
            }
        });
        return false;
    });
    $('body').on('click', '.user-deblock', function (e) {
        e.preventDefault();
        var tr= $(this).closest('tr');
        var key = tr.attr('data-key');
        var url = "/admin/user-x/deblock";
        var _this=this;
        $.ajax({
            url: url,
            type: 'get',
            data:{id:key},
            dataType: 'json',
            success: function (data) {
                console.log(data );
                if (data.result == 'ok') {
                    tr.children('[data-col-seq=9]').text('active');
                    $(_this).html('<span class="glyphicon  glyphicon-user  not-blocked"></span>')
                        .removeClass('user-deblock')
                        .addClass('user-block')
                        .attr('title', 'Блокировать');
                } else {
                    alert(data.message);
                }
            }
        });
        return false;
    })
});
