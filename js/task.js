$(function () {
    $('body').on('click', '.public-action', function (e) {
        e.preventDefault();
        var tr= $(this).closest('tr');
        var key = tr.attr('data-key');
        var url = "/admin/task/public";
        var _this=this;
        $.ajax({
            url: url,
            type: 'get',
            data:{id:key},
            dataType: 'json',
            success: function (data) {
                console.log(data );
                if (data.result == 'ok') {
                    tr.children('[data-col-seq=5]').text('Опубликовано');
                    $(_this).html('<span class="glyphicon  glyphicon-remove not-blocked"></span>')
                        .removeClass('public-action')
                        .addClass('archive-action')
                        .attr('title', 'В архив');
                } else {
                    alert(data.message);
                }
            }
        });
        return false;
    });
    $('body').on('click', '.archive-action', function (e) {
        e.preventDefault();
        var tr= $(this).closest('tr');
        var key = tr.attr('data-key');
        var url = "/admin/task/archive";
        var _this=this;
        $.ajax({
            url: url,
            type: 'get',
            data:{id:key},
            dataType: 'json',
            success: function (data) {
                console.log(data );
                if (data.result == 'ok') {
                    tr.children('[data-col-seq=5]').text('В архиве');
                    $(_this).html('<span class="glyphicon  glyphicon-ok  blocked"></span>')
                        .removeClass('archive-action')
                        .addClass('public-action')
                        .attr('title', 'Публиковать');
                } else {
                    alert(data.message);
                }
            }
        });
        return false;
    })
});
