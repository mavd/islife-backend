<?php

use yii\db\Schema;
use yii\db\Migration;

class m150807_131730_add_new_fields_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'isAdmin', Schema::TYPE_BOOLEAN);
        $this->addColumn('{{%user}}', 'photo', Schema::TYPE_STRING . '(256)');
        $this->addColumn('{{%user}}', 'gender', Schema::TYPE_STRING . '(1)');
        $this->addColumn('{{%user}}', 'age', Schema::TYPE_SMALLINT);
        $this->addColumn('{{%user}}', 'city', Schema::TYPE_STRING . '(256)');
        $this->addColumn('{{%user}}', 'path', Schema::TYPE_STRING . '(256)');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'isAdmin');
        $this->dropColumn('{{%user}}', 'photo');
        $this->dropColumn('{{%user}}', 'gender');
        $this->dropColumn('{{%user}}', 'age');
        $this->dropColumn('{{%user}}', 'city');
        $this->dropColumn('{{%user}}', 'path');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
