-- --------------------------------------------------------
-- ����:                         127.0.0.1
-- ������ �������:               5.5.40-MariaDB-log - mariadb.org binary distribution
-- �� �������:                   Win32
-- HeidiSQL ������:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- ���� ��������� ��� ������� islife.action
CREATE TABLE IF NOT EXISTS `action` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '��.',
  `task_id` int(11) DEFAULT NULL COMMENT '������',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '����� 1',
  `user2_id` int(11) NOT NULL DEFAULT '0' COMMENT '����� 2 ��� ������ �������',
  `action_time_start` datetime DEFAULT NULL COMMENT '����� ������ ��������� �������',
  `action_time_end` datetime DEFAULT NULL COMMENT '����� ��������� ����������',
  `action_status` enum('new','ready','started','reject','cancel','finished','reported','estimated','done','blocked') NOT NULL DEFAULT 'new' COMMENT '������',
  `action_estimate_sum` tinyint(4) NOT NULL DEFAULT '0' COMMENT '����� ���������� ������',
  `action_estimate_count` tinyint(4) NOT NULL DEFAULT '0' COMMENT '���������� ������',
  `action_bonuses` varchar(50) NOT NULL DEFAULT '0' COMMENT '������ ��� �����',
  `action_comment` varchar(255) NOT NULL DEFAULT '0' COMMENT '��������',
  `action_created_at` datetime DEFAULT NULL COMMENT '����� �������� �������',
  `action_updated_at` datetime DEFAULT NULL COMMENT '����� ���������� ���������',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='������� ��� �������� ������ � �������� �������� �������, � ����������� �� ����������\r\n�������:\r\n''new'',\r\n''ready'' - ������ �������, ��� ��������� ���� �����,\r\n''started'' - ������ ���������� � ����������,\r\n''reject'' - ����� ��������� \r\n''cancel'' - �� ������� ���������\r\n''finished'' - ������� ���������\r\n''reported'' - ����� �����������\r\n''estimated'' - ������\r\n''done'' - ����� �������\r\n''blocked'' - ������������';

-- ���� ������ ������� islife.action: ~0 rows (��������������)
/*!40000 ALTER TABLE `action` DISABLE KEYS */;
/*!40000 ALTER TABLE `action` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.device
CREATE TABLE IF NOT EXISTS `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '��.',
  `user_id` int(11) NOT NULL COMMENT '�� ������������',
  `device_access_token` varchar(50) NOT NULL COMMENT '�� ������',
  `device_access_token_expire` datetime NOT NULL COMMENT '����� ��������� ���������� ������',
  `device_name` varchar(512) NOT NULL COMMENT '����� � ������',
  `device_os` varchar(512) NOT NULL COMMENT '��',
  `device_api_id` varchar(512) NOT NULL COMMENT '�� ���������� �� �������������',
  `device_created_at` datetime NOT NULL COMMENT '����� �����������',
  `device_last_login_at` datetime DEFAULT NULL COMMENT '����� ���������� ������',
  PRIMARY KEY (`id`),
  KEY `device_api_id` (`device_api_id`(255)),
  KEY `device_access_token` (`device_access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='���������� � ������� ������ ������/������';

-- ���� ������ ������� islife.device: ~0 rows (��������������)
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
/*!40000 ALTER TABLE `device` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.estimate
CREATE TABLE IF NOT EXISTS `estimate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '��.',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '����� - �����������',
  `action_id` int(11) NOT NULL DEFAULT '0' COMMENT '���������� �����',
  `estimate_value` int(11) NOT NULL DEFAULT '0' COMMENT '������',
  `estimate_status` enum('new','ready','rejected') NOT NULL DEFAULT 'new' COMMENT '������',
  `estomate_comment` varchar(255) DEFAULT NULL COMMENT '�����������',
  `estimate_created_at` datetime NOT NULL COMMENT '����� �������� ',
  `estimate_updated_at` datetime NOT NULL COMMENT '����� ���������� ���������',
  PRIMARY KEY (`id`),
  KEY `action_id` (`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='������ ������� ��������������';

-- ���� ������ ������� islife.estimate: ~0 rows (��������������)
/*!40000 ALTER TABLE `estimate` DISABLE KEYS */;
/*!40000 ALTER TABLE `estimate` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.login_log
CREATE TABLE IF NOT EXISTS `login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `login_ip` varchar(16) NOT NULL DEFAULT '0',
  `login_phoneModel` varchar(256) NOT NULL DEFAULT '0',
  `login_OS` varchar(64) NOT NULL DEFAULT '0',
  `login_versionOS` varchar(64) NOT NULL DEFAULT '0',
  `login_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ���� ������ ������� islife.login_log: ~0 rows (��������������)
/*!40000 ALTER TABLE `login_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_log` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.message
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL COMMENT '��.',
  `message_type` enum('private','public') DEFAULT NULL COMMENT '���',
  `user_id` int(11) DEFAULT NULL COMMENT '����������',
  `message_header` varchar(2048) DEFAULT NULL COMMENT '���������',
  `message_text` varchar(2048) DEFAULT NULL COMMENT '�����',
  `message_status` enum('new','send') DEFAULT NULL COMMENT '������',
  `created_at` datetime DEFAULT NULL COMMENT '����� ��������',
  `send_at` datetime DEFAULT NULL COMMENT '����� ��������',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='���������, ����������';

-- ���� ������ ������� islife.message: ~0 rows (��������������)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.message_template
CREATE TABLE IF NOT EXISTS `message_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '��.',
  `mt_type` enum('email','push','inform') NOT NULL DEFAULT 'email' COMMENT '������ ��������',
  `mt_template` varchar(2048) NOT NULL COMMENT '������',
  `mt_subject_template` varchar(2048) NOT NULL COMMENT '������ ���������',
  `mt_created_at` datetime NOT NULL COMMENT '����� ��������',
  `mt_updated_at` datetime NOT NULL COMMENT '����� ���������',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='������� ���������\r\n� ������� �������� ����������� ��������\r\n{user_name} - ��� ����������\r\n{partner_name} - ��� �������� �� ������ ����\r\n{value} - �������� ���������';

-- ���� ������ ������� islife.message_template: ~0 rows (��������������)
/*!40000 ALTER TABLE `message_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `message_template` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.migration
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ���� ������ ������� islife.migration: ~9 rows (��������������)
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1438951006),
	('m140209_132017_init', 1438951014),
	('m140403_174025_create_account_table', 1438951014),
	('m140504_113157_update_tables', 1438951015),
	('m140504_130429_create_token_table', 1438951016),
	('m140830_171933_fix_ip_field', 1438951016),
	('m140830_172703_change_account_table_name', 1438951016),
	('m141222_110026_update_ip_field', 1438951016),
	('m150807_131730_add_new_fields_to_user', 1438961019);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.profile
CREATE TABLE IF NOT EXISTS `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `public_email` varchar(255) DEFAULT NULL,
  `gravatar_email` varchar(255) DEFAULT NULL,
  `gravatar_id` varchar(32) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `bio` text,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ���� ������ ������� islife.profile: ~2 rows (��������������)
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`) VALUES
	(1, NULL, NULL, 'admin@email.ru', '978c9bafafdaf3b1d9df657bd57d8a27', NULL, NULL, NULL),
	(2, NULL, NULL, 'admin@email1.ru', 'b4d16fd405c690b5c390813b394031bb', NULL, NULL, NULL);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.purchase
CREATE TABLE IF NOT EXISTS `purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '��.',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '�����',
  `service_id` int(11) NOT NULL DEFAULT '0' COMMENT '������',
  `purchase_amount` int(11) NOT NULL DEFAULT '0' COMMENT '���������',
  `purchase_action_id` int(11) NOT NULL DEFAULT '0' COMMENT '�����',
  `purchase_status` enum('new','actived','used') NOT NULL DEFAULT 'new' COMMENT '������',
  `purchase_valid_to` datetime NOT NULL COMMENT '����� ��������� ��������',
  `purchase_valid_from` datetime NOT NULL COMMENT '����� ������ ��������',
  `purchase_created_at` datetime NOT NULL COMMENT '����� ��������',
  `purchase_updated_at` datetime NOT NULL COMMENT '����� ���������',
  PRIMARY KEY (`id`),
  KEY `purchase_status` (`purchase_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='������� �������';

-- ���� ������ ������� islife.purchase: ~0 rows (��������������)
/*!40000 ALTER TABLE `purchase` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.report
CREATE TABLE IF NOT EXISTS `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '��.',
  `user_id` int(11) NOT NULL COMMENT '������������',
  `action_id` int(11) NOT NULL COMMENT '�����',
  `report_photo` varchar(255) DEFAULT NULL COMMENT '������ �� ����',
  `report_comment` varchar(255) DEFAULT NULL COMMENT '�������',
  `report_created_at` datetime DEFAULT NULL COMMENT '����� ��������',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `action_id` (`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='�������� � ���������� �������';

-- ���� ������ ������� islife.report: ~0 rows (��������������)
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
/*!40000 ALTER TABLE `report` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.service
CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '��.',
  `service_type` enum('one','pair','common') NOT NULL DEFAULT 'common' COMMENT '��� ������',
  `service_action_type` enum('action','hours','days') NOT NULL DEFAULT 'action' COMMENT '�� ��� ���������������� ������',
  `service_action_value` int(11) NOT NULL DEFAULT '24' COMMENT '�������� ',
  `service_code` varchar(50) NOT NULL DEFAULT 'srv_show_photo' COMMENT '���������� ���',
  `service_name` varchar(50) NOT NULL COMMENT '��������',
  `service_amount` int(11) DEFAULT NULL COMMENT '���������',
  `service_order` int(11) DEFAULT NULL COMMENT '������� ������',
  `service_status` enum('active','not active') DEFAULT NULL COMMENT '������',
  `service_created_at` datetime DEFAULT NULL COMMENT '����� ��������',
  `service_updated_at` datetime DEFAULT NULL COMMENT '����� ���������',
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_code` (`service_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='��������� �����';

-- ���� ������ ������� islife.service: ~0 rows (��������������)
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
/*!40000 ALTER TABLE `service` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.social_account
CREATE TABLE IF NOT EXISTS `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `data` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  KEY `fk_user_account` (`user_id`),
  CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ���� ������ ������� islife.social_account: ~0 rows (��������������)
/*!40000 ALTER TABLE `social_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_account` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.task
CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '��.',
  `task_name` varchar(50) DEFAULT NULL COMMENT '��������',
  `task_description` varchar(8096) DEFAULT NULL COMMENT '��������',
  `task_type` enum('���������','������','���') DEFAULT NULL COMMENT '���',
  `task_timer` int(11) DEFAULT NULL COMMENT '����� �� ����������',
  `task_status` enum('��������','��������','�������','�����') DEFAULT NULL COMMENT '������',
  `task_comment_reject` varchar(256) DEFAULT NULL COMMENT '�������',
  `task_author_id` int(11) DEFAULT NULL COMMENT '�����',
  `task_health` int(11) DEFAULT NULL COMMENT '��������',
  `task_determination` int(11) DEFAULT NULL COMMENT '���������',
  `task_communication` int(11) DEFAULT NULL COMMENT '�������������',
  `task_skill` int(11) DEFAULT NULL COMMENT '����',
  `task_created_at` timestamp NULL DEFAULT NULL COMMENT '����� ��������',
  `task_updated_at` timestamp NULL DEFAULT NULL COMMENT '����� ���������� ��������������',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ���� ������ ������� islife.task: ~0 rows (��������������)
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
/*!40000 ALTER TABLE `task` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.token
CREATE TABLE IF NOT EXISTS `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`),
  CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ���� ������ ������� islife.token: ~4 rows (��������������)
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` (`user_id`, `code`, `created_at`, `type`) VALUES
	(1, '6kUkahudVIBaW9pgvZisiyjfoKy_4_5u', 1438951441, 0),
	(1, 'KzNQpTk4obqz1TIPSo4V05_sBmrb4hpP', 1438951912, 1),
	(1, 'L3SjQAvu1tu9WoU4e1ccEJTFERgQfCrY', 1438951913, 1),
	(2, '7IDDUxZbKLtpcOZohpzM4JTjqttWlaOa', 1438952681, 0);
/*!40000 ALTER TABLE `token` ENABLE KEYS */;


-- ���� ��������� ��� ������� islife.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '��.',
  `username` varchar(25) NOT NULL COMMENT '���',
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `user_isAdmin` tinyint(1) DEFAULT '1',
  `user_photo` varchar(256) DEFAULT NULL COMMENT '����',
  `user_gender` enum('M','F') DEFAULT NULL COMMENT '���',
  `user_birthday` date DEFAULT NULL COMMENT '���� ��������',
  `user_city` varchar(256) DEFAULT NULL COMMENT '�����',
  `user_path` varchar(256) DEFAULT NULL COMMENT '����',
  `user_status` enum('����������������','������������������','���������������') DEFAULT '������������������' COMMENT '������',
  `user_blocked_comment` varchar(256) DEFAULT NULL COMMENT '����������� � ����������',
  `user_health` int(11) DEFAULT NULL COMMENT '��������',
  `user_determination` int(11) DEFAULT NULL COMMENT '���������',
  `user_communication` int(11) DEFAULT NULL COMMENT '�������������',
  `user_skill` int(11) DEFAULT NULL COMMENT '����',
  `user_last_device_id` int(11) DEFAULT NULL COMMENT '��������� ����������, � �������� ������� ������������',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_username` (`username`),
  UNIQUE KEY `user_unique_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ���� ������ ������� islife.user: ~2 rows (��������������)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `user_isAdmin`, `user_photo`, `user_gender`, `user_birthday`, `user_city`, `user_path`, `user_status`, `user_blocked_comment`, `user_health`, `user_determination`, `user_communication`, `user_skill`, `user_last_device_id`) VALUES
	(1, 'admin', 'admin@email.ru', '$2y$12$7.bByY/FXp2D/Io0tmvjHOrHDHl/KJjTwSjcHh5e5gveockW5O8CC', '9KSDNL0ZPuBdsvvnr-P4ZoNhMkqT2UTa', 1438951441, NULL, NULL, '127.0.0.1', 1438951441, 1438951441, 0, 1, NULL, NULL, NULL, NULL, NULL, '������������������', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'admin1', 'admin@email1.ru', '$2y$12$xbnSP0JTqg7vK4jk54LZSu9wnt7HtZMnvvlpOiGiya0vTyzEo.EFq', 'FN9I-KFGDQRhl6hJzYR6JghcwDwbU-Mj', NULL, NULL, NULL, '127.0.0.1', 1438952681, 1438952681, 0, 0, NULL, NULL, NULL, NULL, NULL, '������������������', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
