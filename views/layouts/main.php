<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\models\UserX;

AppAsset::register($this);
Yii::$app->user->isGuest;
Yii::info(print_r($_SESSION, 1), 'api');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php
$items = [

];
if (YII_ENV == 'dev') {
    $items[] = [
        'label' => 'Devel',
        'items' => [
            [
                'label' => 'Отчет об тестировании API',
                'url' => '/tests/codeception/_log/api.html',
                'linkOptions' => [ 'target' => '_blank' ]
            ],
            [
                'label' => 'Документация',
                'url' => '/runtime/doc/index.html',
                'linkOptions' => [ 'target' => '_blank' ]
            ]      ,
            [ 'label' => 'Оценки', 'url' => '/admin/estimate' ],
            [ 'label' => 'Отчеты', 'url' => '/admin/report' ],
        ]
    ];
}
if (!Yii::$app->user->isGuest) {
    $user = UserX::find()
                 ->where([ 'id' => Yii::$app->user->id ])->one();
}
if (Yii::$app->user->isGuest) {
    $items[] = [ 'label' => 'Вход', 'url' => [ '/user/security/login' ] ];
    $items[] = [ 'label' => 'Регистрация', 'url' => [ '/user/registration/register' ] ];
}
if (!Yii::$app->user->isGuest && $user->user_isAdmin && $user->user_status !== 'blocked') {

    $items[] = [
        'label' => 'Статистика',
        'items' => [
            [ 'label' => 'Отчеты по покупкам', 'url' => '/admin/purchase' ],
            [ 'label' => 'Игры', 'url' => '/admin/play' ],
            [ 'label' => 'Устройства', 'url' => '/admin/device' ],
            [ 'label' => 'Журнал авторизации', 'url' => '/admin/login-log' ],
        ]
    ];

    $items[] = [
        'label' => 'Игра',
        'items' => [
            [ 'label' => 'Фотоотчеты',  'url' => '/admin/play' ],
            [ 'label' => 'Задания', 'url' => '/admin/task' ],

        ]
    ];


    $items[] = [
        'label' => 'Пользователи',
        'items' => [
            [ 'label' => 'Игроки', 'url' => '/admin/user-x' ],
            [ 'label' => 'Администраторы', 'url' => '/user/admin/list' ],
           // [ 'label' => 'Профиль', 'url' => '/user/settings/profile' ],
        ]
    ];
        $items[] = [
            'label' => 'Настройки',
            'items' => [
                [ 'label' => 'Сообщения', 'url' => '/admin/message' ],
                [ 'label' => 'Шаблоны сообщений', 'url' => '/admin/message-template' ],
                [ 'label' => 'Платные услуги', 'url' => '/admin/service' ],
                [ 'label' => 'Уровни игры', 'url' => '/admin/skill-level' ]
            ]
        ];

//    $items[] = [
//        'label' => 'IsLife',
//        'items' => [
//            [ 'label' => 'Устройства', 'url' => '/admin/device' ],
//            [ 'label' => 'Оценки', 'url' => '/admin/estimate' ],
//            [ 'label' => 'Продажи', 'url' => '/admin/purchase' ],
//            [ 'label' => 'Игры', 'url' => '/admin/play' ],
//            [ 'label' => 'Отчеты', 'url' => '/admin/report' ],
//            [ 'label' => 'Лог логинов', 'url' => '/admin/login-log' ],
//        ]
//    ];

}
if (!Yii::$app->user->isGuest) {
    $items[] = [
        'label' => 'Выход (' . Yii::$app->user->identity->username . ')',
        'url' => [ '/user/security/logout' ],
        'linkOptions' => [ 'data-method' => 'post' ]
    ];
}
?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'is Life',
        'brandUrl' => '/admin/task',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => [ 'class' => 'navbar-nav navbar-right' ],
        'items' => $items,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [ ],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
