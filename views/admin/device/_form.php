<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Device */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'LoginLog', 
        'relID' => 'login-log', 
        'value' => \yii\helpers\Json::encode($model->loginLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="device-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'user_name'),
        'options' => ['placeholder-x' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'device_access_token')->textInput(['maxlength' => true, 'placeholder-x' => ' ']) ?>

<!--    --><?//= $form->field($model, 'device_access_token_expire')->widget(\kartik\widgets\DateTimePicker::classname(), [
//        'options' => ['placeholder-x' => 'Choose Device Access Token Expire'],
//        'pluginOptions' => [
//            'autoclose' => true,
//            'format' => 'mm/dd/yyyy hh:ii:ss'
//        ]
//    ]) ?>

    <?= $form->field($model, 'device_name')->textInput(['maxlength' => true, 'placeholder-x' => ' ']) ?>

    <?= $form->field($model, 'device_os')->textInput(['maxlength' => true, 'placeholder-x' => ' ']) ?>

    <?= $form->field($model, 'device_api_id')->textInput(['maxlength' => true, 'placeholder-x' => ' ']) ?>

    <?= $form->field($model, 'device_created_at')->widget(\kartik\widgets\DateTimePicker::classname(), [
        'options' => ['placeholder-x' => 'Choose Device Created At'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'mm/dd/yyyy hh:ii:ss'
        ]
    ]) ?>

    <?= $form->field($model, 'device_last_login_at')->widget(\kartik\widgets\DateTimePicker::classname(), [
        'options' => ['placeholder-x' => 'Choose Device Last Login At'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'mm/dd/yyyy hh:ii:ss'
        ]
    ]) ?>

    <div class="form-group" id="add-login-log"></div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
