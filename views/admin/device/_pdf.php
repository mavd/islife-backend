<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Device */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Device', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Device'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'user_id',
        'device_access_token',
        'device_access_token_expire',
        'device_name',
        'device_os',
        'device_api_id',
        'device_created_at',
        'device_last_login_at',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnLoginLog = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'user.id',
            'label' => 'Device',
        ],
        [
            'attribute' => 'device.id',
            'label' => 'Device',
        ],
        'login_ip',
        'login_created_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerLoginLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode('Login Log'.' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnLoginLog
    ]);
?>
    </div>
</div>