<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DeviceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="device-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'device_access_token') ?>

    <?= $form->field($model, 'device_access_token_expire') ?>

    <?= $form->field($model, 'device_name') ?>

    <?php // echo $form->field($model, 'device_os') ?>

    <?php // echo $form->field($model, 'device_api_id') ?>

    <?php // echo $form->field($model, 'device_created_at') ?>

    <?php // echo $form->field($model, 'device_last_login_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
