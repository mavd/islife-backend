<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PlaySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="play-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'task_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'user2_id') ?>

    <?= $form->field($model, 'play_time_start') ?>

    <?php // echo $form->field($model, 'play_time_end') ?>

    <?php // echo $form->field($model, 'play_status') ?>

    <?php // echo $form->field($model, 'play_result') ?>

    <?php // echo $form->field($model, 'play_type') ?>

    <?php // echo $form->field($model, 'play_purchase_id') ?>

    <?php // echo $form->field($model, 'play_purchase2_id') ?>

    <?php // echo $form->field($model, 'play_estimate_sum') ?>

    <?php // echo $form->field($model, 'play_estimate_count') ?>

    <?php // echo $form->field($model, 'play_bonuses') ?>

    <?php // echo $form->field($model, 'play_comment') ?>

    <?php // echo $form->field($model, 'play_created_at') ?>

    <?php // echo $form->field($model, 'play_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
