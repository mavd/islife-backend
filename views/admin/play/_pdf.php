<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Play */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Plays'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="play-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Play').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'task.id',
            'label' => Yii::t('app', 'Task'),
        ],
        [
            'attribute' => 'user.id',
            'label' => Yii::t('app', 'User'),
        ],
        'user2_id',
        'play_time_start',
        'play_time_end',
        'play_status',
        'play_result',
        'play_type',
        'play_purchase_id',
        'play_purchase2_id',
        'play_estimate_sum',
        'play_estimate_count',
        'play_bonuses',
        'play_comment',
        'play_created_at',
        'play_updated_at',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnEstimate = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'play.id',
            'label' => Yii::t('app', 'Play'),
        ],
        [
            'attribute' => 'user.id',
            'label' => Yii::t('app', 'User'),
        ],
        'estimate_value',
        'estimate_status',
        'estimate_comment',
        'estimate_created_at',
        'estimate_updated_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEstimate,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode(Yii::t('app', 'Estimate').' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnEstimate
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnRatingLog = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'user.id',
            'label' => Yii::t('app', 'User'),
        ],
        [
            'attribute' => 'play.id',
            'label' => Yii::t('app', 'Play'),
        ],
        'rate_play_type',
        'play_one_count',
        'play_pair_count',
        'rate_value',
        'rate_sum',
        'rate_created_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerRatingLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode(Yii::t('app', 'Rating Log').' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnRatingLog
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnReport = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'user.id',
            'label' => Yii::t('app', 'User'),
        ],
        [
            'attribute' => 'play.id',
            'label' => Yii::t('app', 'Play'),
        ],
        'report_photo',
        'report_comment',
        'report_created_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerReport,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode(Yii::t('app', 'Report').' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnReport
    ]);
?>
    </div>
</div>