<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Play */

$this->title                   = $model->id;
$this->params['breadcrumbs'][] = [ 'label' => Yii::t('app', 'Plays'), 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="play-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Play') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('app', 'Update'), [ 'update', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ]) ?>
            <?= Html::a(Yii::t('app', 'Delete'), [ 'delete', 'id' => $model->id ], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <?php
        $gridColumn = [
            [ 'attribute' => 'id', 'hidden' => true ],

            [
                'attribute' => 'task_id',
                'value' => $model->task_id . ' :: ' . $model->task->task_name
            ],
            [
                'attribute' => 'user_id',
                'value' => $model->user_id . ' :: ' . $model->user->user_name
            ],
            [
                'attribute' => 'user2_id',
                'value' => $model->user2_id . ' :: ' . $model->user2->user_name
            ],

            'play_time_start',
            'play_time_end',
            [
                'attribute' => 'play_status',
                'value' => substr(Yii::t('play', $model->play_status), 3),
            ],
            [
                'attribute' => 'play_result',
                'value' => Yii::t('play', 'r-' . $model->play_result),
            ],
            [
                'attribute' => 'play_type',
                'value' => Yii::t('play', $model->play_type),
            ],

            //'play_purchase_id',
            //'play_purchase2_id',
            'play_estimate_sum',
            'play_estimate_count',
//            'play_bonuses',
            'play_comment',
            'play_created_at',
            'play_updated_at',
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn
        ]);
        ?>
    </div>
    
    <div class="row">
        <?php
        $gridColumnEstimate = [
            [ 'class' => 'yii\grid\SerialColumn' ],
            [ 'attribute' => 'id', 'hidden' => true ],
            [
                'attribute' => 'play.id',
                'label' => Yii::t('app', 'Игра'),
            ],
            [
                'attribute' => 'user.id',
                'label' => Yii::t('app', 'Игрок'),
            ],
            [
                'attribute' => 'estimate_value',

                'label' => 'Оценка',
            ],
            [
                'attribute' => 'estimate_status',
                'label' => 'Статув',
                'format' => 'text',
                'filter' => [ 'new' => 'новая',
                              'ready' => 'готово',
                              'rejected' => 'отклонено' ],
                'content' => function ($data) {
                    return Yii::t('estimate', $data->estimate_status);
                }


            ],

            [
                'attribute' => 'estimate_created_at',
                'format' => 'datetime',
                'label' => 'Выдана для оценки',
            ],

            [
                'attribute' => 'estimate_updated_at',
                'format' => 'datetime',
                'label' => 'Время выставления',
            ],

        ];
        echo Gridview::widget([
            'dataProvider' => $providerEstimate,
            'pjax' => true,
            'pjaxSettings' => [ 'options' => [ 'id' => 'kv-pjax-container' ] ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode(Yii::t('app', 'Estimate') . ' ' . $this->title) . ' </h3>',
            ],
            'columns' => $gridColumnEstimate
        ]);
        ?>
    </div>

    <div class="row">
        <?php
        $gridColumnReport = [
            [ 'class' => 'yii\grid\SerialColumn' ],
            [ 'attribute' => 'id', 'hidden' => true ],

            [
                'attribute' => 'play.id',
                'label' => Yii::t('app', 'Play'),
            ],
            [
                'attribute' => 'report_thumb',
                'format' => 'image',
                'label' => 'Фото',
            ],
            [
                'attribute' => 'report_comment',
                'format' => 'text',
                'label' => 'Комментарий',
            ],

            [
                'attribute' => 'report_created_at',
                'format' => 'text',
                'label' => 'Дата создания',
            ],

        ];
        echo Gridview::widget([
            'dataProvider' => $providerReport,
            'pjax' => true,
            'pjaxSettings' => [ 'options' => [ 'id' => 'kv-pjax-container' ] ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode(Yii::t('app', 'Report') . ' ' . $this->title) . ' </h3>',
            ],
            'columns' => $gridColumnReport
        ]);
        ?>
    </div>
</div>