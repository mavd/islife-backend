<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\PlaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('app', 'Plays');
$this->params['breadcrumbs'][] = $this->title;
$search
                               = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
$this->registerJsFile(Yii::getAlias('@web/js/jquery.lighter.js'), [ 'depends' => [
    'yii\web\YiiAsset',
    'yii\bootstrap\BootstrapAsset'
],
]);
$this->registerCssFile(Yii::getAlias('@web/css/jquery.lighter.css'));
?>
<style>

    td.play-status select{
        min-width: 190px;
    }
    td.play-result select{
        min-width: 115px;
    }
    td.play-type select{
        min-width: 125px;
    }

    td img{
        padding:5px;
    }
</style>
<div class="play-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?php
    $gridColumn = [
        [ 'class' => 'yii\grid\SerialColumn' ],
        [ 'attribute' => 'id', 'hidden' => true ],
        'task_id',
        'taskName',

        'user_id',
        'userName',

        'user2_id',
        [
            'attribute'=>'user2Name',
            'label'=>'Имя'
        ],


        'play_time_start',
        [
            'filterOptions' => ['class' => 'play-status'],
            'attribute' => 'play_status',
            'filter' => [
                'blocked' => '1. Блокировано',
                'canceled' => '2. Прервано',
                'done' => '3. Обработано',
                'estimated' => '4. Оценено',
                'finished' => '5. Игра окончена',
                'invited' => '6. Приглашение',
                'new' => '7. Новая',
                'ready' => '8. Готов к игре',
                'rejected' => '9. Отказ от игры',
                'reported' => 'A. Отчет готов',
                'started' => 'B. В игре',
            ],
            'content' => function ($data) {
                return Yii::t('play', $data->play_status);
            },
        ],
        [
            'label'=>'Фотоотчет',
            'content'=>function($data){
                $html='';
                if ($data->reports) {
                  foreach($data->reports as $report){
                      $thumb=$report->report_thumb;
                      $img=$report->report_photo;
                      if (!file_exists( $_SERVER['DOCUMENT_ROOT'] . $thumb) && file_exists( $_SERVER['DOCUMENT_ROOT'] . $img) ) {
                          $thumb=$img;
                      }
                      if (!file_exists( $_SERVER['DOCUMENT_ROOT'] . $img) && file_exists( $_SERVER['DOCUMENT_ROOT'] . $thumb) ) {
                          $img=$thumb;
                      }
                      if ($img && $thumb) {
                          $html.=" <a href='$img' data-lighter>
                                        <img src='$thumb' />
                                </a>  ";

                      }
                  }

                }
                if ($html){
                    return $html;
                }
                return ('Нет');
            }

        ],
        [
            'filterOptions' => ['class' => 'play-result'],
            'attribute' => 'play_result',
            'filter' => [
                'new' => 'Новая',
                'success' => 'Успешно',
                'reject' => 'Отказ',
                'cancel' => 'Прервано',
                'fail' => 'Неудовлетворительно',
            ],
            'content' => function ($data) {
                return Yii::t('play', 'r-' . $data->play_result);
            },
        ],
        [
            'filterOptions' => ['class' => 'play-type'],
            'attribute' => 'play_type',
            'filter' => [
                'both' => 'Любая',
                'one' => 'Одиночная',
                'pair' => 'Парная',
            ],
            'content' => function ($data) {
                return Yii::t('play', $data->play_type);
            },
        ],
        'play_estimate',
        //'play_estimate_sum',
        //'play_estimate_count',
        //'play_purchase_id',
        //'play_purchase2_id',
//        'play_bonuses',
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pager' => [
            'firstPageLabel' => '<span class="glyphicon glyphicon-triangle-left"></span>',
            'lastPageLabel' => '<span class="glyphicon glyphicon-triangle-right"></span>'

        ],
        'pjax' => true,
        'pjaxSettings' => [ 'options' => [ 'id' => 'kv-pjax-container' ] ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode($this->title) . ' </h3>',
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Страница',
            'fontAwesome' => true,
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Все',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Экспорт всей таблицы</li>',
                    ],
                ],
            ]),
        ],
    ]); ?>

</div>
