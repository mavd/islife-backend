<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Play */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Estimate', 
        'relID' => 'estimate', 
        'value' => \yii\helpers\Json::encode($model->estimates),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'RatingLog', 
        'relID' => 'rating-log', 
        'value' => \yii\helpers\Json::encode($model->ratingLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Report', 
        'relID' => 'report', 
        'value' => \yii\helpers\Json::encode($model->reports),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="play-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'task_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Task::find()->orderBy('id')->asArray()->all(), 'id', 'task_name'),
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Task')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'user_name'),
        'options' => ['placeholder-x' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'user2_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'user_name'),
        'options' => ['placeholder-x' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'play_time_start')->widget(\kartik\widgets\DateTimePicker::classname(), [
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Play Time Start')],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'mm/dd/yyyy hh:ii:ss'
        ]
    ]) ?>

    <?= $form->field($model, 'play_time_end')->widget(\kartik\widgets\DateTimePicker::classname(), [
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Play Time End')],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'mm/dd/yyyy hh:ii:ss'
        ]
    ]) ?>

    <?= $form->field($model, 'play_status')->dropDownList(
        [
            'blocked'=>'Блокоировано',
            'canceled'=>'Прервано',
            'done'=>'Обработано',
            'estimated'=>'Оценено',
            'finished'=>'Игра окончена',
            'invited'=>'Приглашение',
            'new'=>'Новая',
            'ready'=>'Готов к игре',
            'rejected'=>'Отказ от игры',
            'reported'=>'Отчет готов',
            'started'=>'В игре'
        ], ['prompt' => '']) ?>

    <?= $form->field($model, 'play_result')->dropDownList(
        [
            'new'=>'Новая',
            'success'=>'Успешно',
            'reject'=>'Отказ',
            'cancel'=>'Прервано',
            'fail'=>'Неудовлетворительно',
        ], ['prompt' => '']) ?>

    <?= $form->field($model, 'play_type')->dropDownList([
        'both'=>'Любая',
        'one'=>'Одиночная',
        'pair'=>'Парная',
    ], ['prompt' => '']) ?>

<!--    --><?//= $form->field($model, 'play_purchase_id')->textInput(['placeholder-x' => 'Play Purchase']) ?>
<!---->
<!--    --><?//= $form->field($model, 'play_purchase2_id')->textInput(['placeholder-x' => 'Play Purchase2']) ?>

    <?= $form->field($model, 'play_estimate_sum')->textInput(['placeholder-x' => 'Play Estimate Sum']) ?>

    <?= $form->field($model, 'play_estimate_count')->textInput(['placeholder-x' => 'Play Estimate Count']) ?>
<!---->
<!--    --><?//= $form->field($model, 'play_bonuses')->textInput(['maxlength' => true, 'placeholder-x' => 'Play Bonuses']) ?>

    <?= $form->field($model, 'play_comment')->textInput(['maxlength' => true, 'placeholder-x' => 'Play Comment']) ?>





    <div class="form-group" id="add-estimate"></div>

    <div class="form-group" id="add-rating-log"></div>

    <div class="form-group" id="add-report"></div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
