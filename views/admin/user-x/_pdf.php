<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\UserX */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Xes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-x-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'User X'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'username',
        'email:email',
        'password_hash',
        'auth_key',
        'confirmed_at',
        'unconfirmed_email:email',
        'blocked_at',
        'registration_ip',
        'flags',
        'user_isAdmin',
        'user_name',
        'user_photo',
        'user_gender',
        'user_birthday',
        'user_city',
        'user_path',
        'user_status',
        'user_blocked_comment',
        'user_health',
        'user_determination',
        'user_communication',
        'user_skill',
        'user_amount',
        'user_bonuses',
        'user_last_device_id',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnEstimate = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'play.id',
            'label' => 'Play',
        ],
        [
            'attribute' => 'user.id',
            'label' => 'User',
        ],
        'estimate_value',
        'estimate_status',
        'estimate_comment',
        'estimate_created_at',
        'estimate_updated_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEstimate,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode('Estimate'.' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnEstimate
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnMessage = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'user.id',
            'label' => 'User',
        ],
        'message_type',
        'message_mode',
        'message_header',
        'message_text',
        'message_status',
        'send_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMessage,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode('Message'.' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnMessage
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnPlay = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'task.id',
            'label' => 'Task',
        ],
        [
            'attribute' => 'user.id',
            'label' => 'User',
        ],
        'user2_id',
        'play_time_start',
        'play_time_end',
        'play_status',
        'play_result',
        'play_type',
        'play_purchase_id',
        'play_purchase2_id',
        'play_estimate_sum',
        'play_estimate_count',
        'play_bonuses',
        'play_comment',
        'play_created_at',
        'play_updated_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlay,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode('Play'.' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnPlay
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnPurchase = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'user.id',
            'label' => 'User',
        ],
        [
            'attribute' => 'service.id',
            'label' => 'Service',
        ],
        'purchase_amount',
        'purchase_play_id',
        'purchase_status',
        'purchase_valid_to',
        'purchase_valid_from',
        'purchase_created_at',
        'purchase_updated_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPurchase,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode('Purchase'.' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnPurchase
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnRatingLog = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'user.id',
            'label' => 'User',
        ],
        [
            'attribute' => 'play.id',
            'label' => 'Play',
        ],
        'rate_play_type',
        'play_one_count',
        'play_pair_count',
        'rate_value',
        'rate_sum',
        'rate_created_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerRatingLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode('Rating Log'.' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnRatingLog
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnReport = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'user.id',
            'label' => 'User',
        ],
        [
            'attribute' => 'play.id',
            'label' => 'Play',
        ],
        'report_photo',
        'report_comment',
        'report_created_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerReport,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode('Report'.' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnReport
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnSocialAccount = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'user.id',
            'label' => 'User',
        ],
        'provider',
        'client_id',
        'data:ntext',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSocialAccount,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode('Social Account'.' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnSocialAccount
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnToken = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'user.id',
            'label' => 'User',
        ],
        'code',
        'type',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerToken,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode('Token'.' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnToken
    ]);
?>
    </div>
</div>