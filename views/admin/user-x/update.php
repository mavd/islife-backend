<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserX */

$this->title = 'Редактирование игрока : ' . ' ' . $model->id . ' ' . $model->user_name;
$this->params['breadcrumbs'][] = ['label' => 'Игрок', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="user-x-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
