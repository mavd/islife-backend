<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\UserX */
/* @var $device app\models\Device */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget([ 'viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
                                        'viewParams' => [
                                            'class' => 'Estimate',
                                            'relID' => 'estimate',
                                            'value' => \yii\helpers\Json::encode($model->estimates),
                                            'isNewRecord' => ($model->isNewRecord) ? 1 : 0
                                        ]
]);
\mootensai\components\JsBlock::widget([ 'viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
                                        'viewParams' => [
                                            'class' => 'Message',
                                            'relID' => 'message',
                                            'value' => \yii\helpers\Json::encode($model->messages),
                                            'isNewRecord' => ($model->isNewRecord) ? 1 : 0
                                        ]
]);
\mootensai\components\JsBlock::widget([ 'viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
                                        'viewParams' => [
                                            'class' => 'Play',
                                            'relID' => 'play',
                                            'value' => \yii\helpers\Json::encode($model->plays),
                                            'isNewRecord' => ($model->isNewRecord) ? 1 : 0
                                        ]
]);
\mootensai\components\JsBlock::widget([ 'viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
                                        'viewParams' => [
                                            'class' => 'Purchase',
                                            'relID' => 'purchase',
                                            'value' => \yii\helpers\Json::encode($model->purchases),
                                            'isNewRecord' => ($model->isNewRecord) ? 1 : 0
                                        ]
]);
\mootensai\components\JsBlock::widget([ 'viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
                                        'viewParams' => [
                                            'class' => 'RatingLog',
                                            'relID' => 'rating-log',
                                            'value' => \yii\helpers\Json::encode($model->ratingLogs),
                                            'isNewRecord' => ($model->isNewRecord) ? 1 : 0
                                        ]
]);
\mootensai\components\JsBlock::widget([ 'viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
                                        'viewParams' => [
                                            'class' => 'Report',
                                            'relID' => 'report',
                                            'value' => \yii\helpers\Json::encode($model->reports),
                                            'isNewRecord' => ($model->isNewRecord) ? 1 : 0
                                        ]
]);
?>
<?php if ($model->user_photo && file_exists($_SERVER['DOCUMENT_ROOT'] . $model->user_photo)) { ?>
    <img src="<?= $model->user_photo ?>" style="max-width: 200px;float:right;">
<?php } ?>
<div class="user-x-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', [ 'template' => '{input}' ])->textInput([ 'style' => 'display:none' ]); ?>



    <?= $form->field($model, 'user_isAdmin')->checkbox() ?>

    <?= $form->field($model, 'user_name')->textInput([ 'maxlength' => true, 'placeholder-x' => 'Имя' ]) ?>




    <?= $form->field($model, 'user_gender')->dropDownList([ 'M' => 'М', 'F' => 'Ж', ], [ 'prompt' => '' ]) ?>

    <?= $form->field($model, 'user_birthday')->widget(\kartik\widgets\DatePicker::classname(), [
        'options' => [ 'placeholder-x' => 'Choose User Birthday' ],
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-m-d'
        ]
    ]); ?>

    <?= $form->field($model, 'user_city')->textInput([ 'maxlength' => true, 'placeholder-x' => 'User City' ]) ?>

    <?= $form->field($model, 'user_path')->textInput([ 'maxlength' => true, 'placeholder-x' => 'User Path' ]) ?>

    <?= $form->field($model, 'user_status')->dropDownList([
        'new' => 'Новый',
        'verify' => 'Проверен',
        'blocked' => 'Блокирован',
    ], [ 'prompt' => '' ]) ?>

    <?= $form->field($model, 'user_blocked_comment')->textInput([ 'maxlength' => true, 'placeholder-x' => 'User Blocked Comment' ]) ?>

    <!--    --><? //= $form->field($model, 'user_health')->textInput(['placeholder-x' => 'User Health']) ?>

    <?= $form->field($model, 'user_determination')->textInput([ 'placeholder-x' => 'User Determination' ]) ?>

    <?= $form->field($model, 'user_communication')->textInput([ 'placeholder-x' => 'User Communication' ]) ?>

    <?= $form->field($model, 'user_skill')->textInput([ 'placeholder-x' => 'User Skill' ]) ?>

    <?= $form->field($model, 'user_amount')->textInput([ 'placeholder-x' => 'User Amount' ]) ?>

    <!--    --><? //= $form->field($model, 'user_bonuses')->textInput(['placeholder-x' => 'User Bonuses']) ?>
    <?php if ($device){
        $dname=$device->id . '::' . $device->device_name;
    } else {
        $dname='нет';
    }

    ?>
    <div class="form-group" id="device"><label>Последнее используемое устройство: </label> <?= $dname ?> </div>
<!--    --><?//= $form->field( $device, 'device_name')->textInput( ['disabled' => true,'title'=>'qq']) ?>

    <div class="form-group" id="add-estimate"></div>

    <div class="form-group" id="add-message"></div>

    <div class="form-group" id="add-play"></div>

    <div class="form-group" id="add-purchase"></div>

    <div class="form-group" id="add-rating-log"></div>

    <div class="form-group" id="add-report"></div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
