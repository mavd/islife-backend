<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserXSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Игроки';
$this->params['breadcrumbs'][] = $this->title;
$search
                               = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
$this->registerJsFile(Yii::getAlias('@web/js/user-x.js'), [ 'depends' => [
    'yii\web\YiiAsset',
    'yii\bootstrap\BootstrapAsset'
],
]);
?>
<style>

    td.user-status select {
        min-width: 130px;
    }
</style>
<div class="user-x-index">
    <style>
        .user-x-index td img {
            max-height: 40px;
            min-height: 40px;
        }

        .user_block {

            cursor: pointer;
        }

        .user-deblock, .user-deblock:hover {

            cursor: pointer;
        }
    </style>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <style>
        td.user-gender select {
            min-width: 70px;

    </style>
    <?php
    $url = \yii\helpers\Url::toRoute([ '#' ]);

    $gridColumn = [
        [ 'class' => 'yii\grid\SerialColumn' ],
        [ 'attribute' => 'id', 'hidden' => true ],
        'id',
        'user_name',
        'email:email',
        [
            'attribute' => 'user_photo',
            'format' => ($data->user_photo)?'image':'text',
            'content' => function ($data) {
                if ($data->user_photo && file_exists($_SERVER['DOCUMENT_ROOT'] . $data->user_photo)){
                    return '<image src="' .  $data->user_photo . '">';
                }
                return '<span class="not-set">(фото не загружено)</span>';

            },
        ],
        [

            'filterOptions' => [ 'class' => 'user-gender' ],
            'attribute' => 'user_gender',
            'filter' => [ 'M' => 'М', 'F' => 'Ж' ],
            'content' => function ($data) {
                $genders = [ 'M' => 'М', 'F' => 'Ж' ];
                return $genders[$data->user_gender];

            },
        ],

        'user_birthday',
        'user_city',
        'user_path',

        [
            'filterOptions' => [ 'class' => 'user-status' ],
            'attribute' => 'user_status',
            'filter' => [ 'new' => 'Новый',
                          'verify' => 'Проверен',
                          'blocked' => 'Блокирован', ],
            'content' => function ($data) {
                return Yii::t('app', $data->user_status);
            },
        ],
        'user_skill',
        'user_amount',
        [
            'class' => 'yii\grid\ActionColumn',
            'headerOptions' => [ 'style' => 'width:90px;text-align:center;' ],
            'template' => '{update} {delete}<br> {block} {send}',
            'buttons' => [
                'block' => function ($url, $model) {

                    if ($model->user_status == 'blocked') {

                        return Html::a('<span class="glyphicon  glyphicon-lock blocked"></span>', $url, [
                            'title' => 'Разблокировать',
                            'data-pjax' => '0', // нужно для отключения для данной ссылки стандартного обработчика pjax. Поверьте, он все портит
                            'class' => 'user-deblock' // указываем ссылке класс, чтобы потом можно было на него повесить нужный JS-обработчик
                        ]);
                    } else {
                        return Html::a('<span class="glyphicon  glyphicon-user not-blocked"></span>', $url, [
                            'title' => 'Блокировать',
                            'data-pjax' => '0', // нужно для отключения для данной ссылки стандартного обработчика pjax. Поверьте, он все портит
                            'class' => 'user-block' // указываем ссылке класс, чтобы потом можно было на него повесить нужный JS-обработчик
                        ]);
                    }
                },
                'send' => function ($id, $model) {
                    return Html::a('<span class="glyphicon glyphicon glyphicon-envelope"></span>',
                        Url::toRoute('/admin/message/create?user_id=' . $model->id), [
                            'title' => 'Послать сообщение',
                            'data-pjax' => '0', // нужно для отключения для данной ссылки стандартного обработчика pjax. Поверьте, он все портит
                            'class' => 'send-action' // указываем ссылке класс, чтобы потом можно было на него повесить нужный JS-обработчик
                        ]);
                }
            ]
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pager' => [
            'firstPageLabel' => '<span class="glyphicon glyphicon-triangle-left"></span>',
            'lastPageLabel' => '<span class="glyphicon glyphicon-triangle-right"></span>'

        ],
        'pjax' => true,
        'pjaxSettings' => [ 'options' => [ 'id' => 'kv-pjax-container' ] ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode($this->title) . ' </h3>',
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Страница',
            'fontAwesome' => true,
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Все',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Экспорт всей таблицы</li>',
                    ],
                ],
            ]),
        ],
    ]); ?>

</div>