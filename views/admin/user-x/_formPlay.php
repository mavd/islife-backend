<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

Pjax::begin();
$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Play',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions'=>['hidden'=>true]],
        'task_id' => [
            'label' => 'Task',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Task::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder-x' => 'Choose Task'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'user_id' => [
            'label' => 'User',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder-x' => 'Choose User'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'user2_id' => ['type' => TabularForm::INPUT_TEXT],
        'play_time_start' => ['type' => TabularForm::INPUT_WIDGET,
        'widgetClass' => \kartik\widgets\DateTimePicker::classname(),
        'options' => [
            'options' => ['placeholder-x' => 'Choose Play Time Start'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'hh:ii:ss dd-M-yyyy'
            ]
        ]
],
        'play_time_end' => ['type' => TabularForm::INPUT_WIDGET,
        'widgetClass' => \kartik\widgets\DateTimePicker::classname(),
        'options' => [
            'options' => ['placeholder-x' => 'Choose Play Time End'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'hh:ii:ss dd-M-yyyy'
            ]
        ]
],
        'play_status' => ['type' => TabularForm::INPUT_DROPDOWN_LIST,
                    'options' => [
                        'items' => [ 'new' => 'New', 'invited' => 'Invited', 'ready' => 'Ready', 'started' => 'Started', 'rejected' => 'Rejected', 'canceled' => 'Canceled', 'reported' => 'Reported', 'finished' => 'Finished', 'done' => 'Done', 'blocked' => 'Blocked', 'estimated' => 'Estimated', ],
                        'columnOptions' => ['width' => '185px'],
                        'options' => ['placeholder-x' => 'Choose Play Status'],
                    ]
        ],
        'play_result' => ['type' => TabularForm::INPUT_DROPDOWN_LIST,
                    'options' => [
                        'items' => [ 'new' => 'New', 'success' => 'Success', 'reject' => 'Reject', 'cancel' => 'Cancel', 'fail' => 'Fail', ],
                        'columnOptions' => ['width' => '185px'],
                        'options' => ['placeholder-x' => 'Choose Play Result'],
                    ]
        ],
        'play_type' => ['type' => TabularForm::INPUT_DROPDOWN_LIST,
                    'options' => [
                        'items' => [ 'one' => 'One', 'pair' => 'Pair', 'both' => 'Both', ],
                        'columnOptions' => ['width' => '185px'],
                        'options' => ['placeholder-x' => 'Choose Play Type'],
                    ]
        ],
        'play_purchase_id' => ['type' => TabularForm::INPUT_TEXT],
        'play_purchase2_id' => ['type' => TabularForm::INPUT_TEXT],
        'play_estimate_sum' => ['type' => TabularForm::INPUT_TEXT],
        'play_estimate_count' => ['type' => TabularForm::INPUT_TEXT],
        'play_bonuses' => ['type' => TabularForm::INPUT_TEXT],
        'play_comment' => ['type' => TabularForm::INPUT_TEXT],
        'play_created_at' => ['type' => TabularForm::INPUT_WIDGET,
        'widgetClass' => \kartik\widgets\DateTimePicker::classname(),
        'options' => [
            'options' => ['placeholder-x' => 'Choose Play Created At'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'hh:ii:ss dd-M-yyyy'
            ]
        ]
],
        'play_updated_at' => ['type' => TabularForm::INPUT_WIDGET,
        'widgetClass' => \kartik\widgets\DateTimePicker::classname(),
        'options' => [
            'options' => ['placeholder-x' => 'Choose Play Updated At'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'hh:ii:ss dd-M-yyyy'
            ]
        ]
],
        'del' => [
            'type' => TabularForm::INPUT_STATIC,
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowPlay(' . $key . '); return false;', 'id' => 'play-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i> ' . 'Play' . '  </h3>',
            'type' => GridView::TYPE_INFO,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Row', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowPlay()']),
        ]
    ]
]);
Pjax::end();
?>