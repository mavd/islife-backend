<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\UserX */

$this->title = $model->id . ' ' . $model->user_name;
$this->params['breadcrumbs'][] = ['label' => 'Игрок', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .user-x-view td > img{
        max-width: 70px;
    }
</style>
<div class="user-x-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Игрок'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
                        
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        //'username',
        'email:email',
        //'password_hash',
        //'auth_key',
        //'confirmed_at',
        //'unconfirmed_email:email',
        //'blocked_at',
        //'registration_ip',
        //'flags',
        //'user_isAdmin',
        'user_name',
        'user_photo:image',
        'user_gender',
        'user_birthday',
        'user_city',
        'user_path',
        'user_status',
        'user_blocked_comment',
        //'user_health',
        'user_determination',
        'user_communication',
        'user_skill',
        'user_amount',
        //'user_bonuses',
        'user_last_device_id',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>

    

</div>