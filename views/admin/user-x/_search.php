<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserXSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-x-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'password_hash') ?>

    <?= $form->field($model, 'auth_key') ?>

    <?php // echo $form->field($model, 'confirmed_at') ?>

    <?php // echo $form->field($model, 'unconfirmed_email') ?>

    <?php // echo $form->field($model, 'blocked_at') ?>

    <?php // echo $form->field($model, 'registration_ip') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'flags') ?>

    <?php // echo $form->field($model, 'user_isAdmin') ?>

    <?php // echo $form->field($model, 'user_name') ?>

    <?php // echo $form->field($model, 'user_photo') ?>

    <?php // echo $form->field($model, 'user_gender') ?>

    <?php // echo $form->field($model, 'user_birthday') ?>

    <?php // echo $form->field($model, 'user_city') ?>

    <?php // echo $form->field($model, 'user_path') ?>

    <?php // echo $form->field($model, 'user_status') ?>

    <?php // echo $form->field($model, 'user_blocked_comment') ?>

    <?php // echo $form->field($model, 'user_health') ?>

    <?php // echo $form->field($model, 'user_determination') ?>

    <?php // echo $form->field($model, 'user_communication') ?>

    <?php // echo $form->field($model, 'user_skill') ?>

    <?php // echo $form->field($model, 'user_amount') ?>

    <?php // echo $form->field($model, 'user_bonuses') ?>

    <?php // echo $form->field($model, 'user_last_device_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
