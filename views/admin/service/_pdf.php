<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Service */

$this->title = $model->service_type;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Service').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'service_type',
        'service_play_type',
        'service_play_value',
        'service_code',
        'service_name',
        'service_amount',
        'service_order',
        'service_status',
        'service_created_at',
        'service_updated_at',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnPurchase = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'user.id',
            'label' => Yii::t('app', 'User'),
        ],
        [
            'attribute' => 'service.service_type',
            'label' => Yii::t('app', 'Service'),
        ],
        'purchase_amount',
        'purchase_play_id',
        'purchase_status',
        'purchase_valid_to',
        'purchase_valid_from',
        'purchase_created_at',
        'purchase_updated_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPurchase,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode(Yii::t('app', 'Purchase').' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnPurchase
    ]);
?>
    </div>
</div>