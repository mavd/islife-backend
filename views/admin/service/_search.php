<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model ServiceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'service_type') ?>

    <?= $form->field($model, 'service_play_type') ?>

    <?= $form->field($model, 'service_play_value') ?>

    <?= $form->field($model, 'service_code') ?>

    <?php // echo $form->field($model, 'service_name') ?>

    <?php // echo $form->field($model, 'service_amount') ?>

    <?php // echo $form->field($model, 'service_order') ?>

    <?php // echo $form->field($model, 'service_status') ?>

    <?php // echo $form->field($model, 'service_created_at') ?>

    <?php // echo $form->field($model, 'service_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
