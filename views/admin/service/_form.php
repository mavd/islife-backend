<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\UserX;

/* @var $this yii\web\View */
/* @var $model app\models\Service */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget([ 'viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
                                        'viewParams' => [
                                            'class' => 'Purchase',
                                            'relID' => 'purchase',
                                            'value' => \yii\helpers\Json::encode($model->purchases),
                                            'isNewRecord' => ($model->isNewRecord) ? 1 : 0
                                        ]
]);
$user = UserX::find()->me()->one();
?>

<div class="service-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'service_type')->dropDownList([
        'common' => 'Общая',
        'one' => 'Одиночная',
        'pair' => 'Парная'
    ], [ 'prompt' => '' ]) ?>
    <?php if ($user & $user->username == 'kev')
        echo $form->field($model, 'service_play_type')->dropDownList([ 'action' => 'Action', 'hours' => 'Hours', 'days' => 'Days', ], [ 'prompt' => '' ]) ?>

    <?php if ($user & $user->username == 'kev')
        echo  $form->field($model, 'service_play_value')->textInput([ 'placeholder-x' => '' ]) ?>

    <?php
        echo $form->field($model, 'service_code')->textInput([ 'maxlength' => true, 'placeholder-x' => ' ' ]) ?>

    <?= $form->field($model, 'service_name')->textInput([ 'maxlength' => true, 'placeholder-x' => ' ' ]) ?>

    <?= $form->field($model, 'service_amount')->textInput([ 'placeholder-x' => ' ' ]) ?>

    <?php if ($user & $user->username == 'kev')
        echo  $form->field($model, 'service_order')->textInput([ 'placeholder-x' => '' ]) ?>

    <?php if ($user & $user->username == 'kev')
        echo  $form->field($model, 'service_status')->dropDownList([ 'active' => 'Active', 'not active' => 'Not active', ], [ 'prompt' => '' ]) ?>


    <div class="form-group" id="add-purchase"></div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
