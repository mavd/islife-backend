<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use app\models\UserX;

/* @var $this yii\web\View */
/* @var $model app\models\Service */

$this->title                   = $model->service_name;
$this->params['breadcrumbs'][] = [ 'label' => Yii::t('app', 'Services'), 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
$user                          = UserX::find()->me()->one();
?>
<div class="service-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Service') . ': ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('app', 'Update'), [ 'update', 'id' => $model['id'] ], [ 'class' => 'btn btn-primary' ]) ?>
            <?php
            if ($user & $user->username == 'kev') {
                echo Html::a(Yii::t('app', 'Delete'), [ 'delete', 'id' => $model['id'] ], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]);
            }
            ?>
        </div>
    </div>
    <div class="row">
        <?php
        if ($user & $user->username == 'kev') {
            $gridColumn = [
                [ 'attribute' => 'service_type',

                  'value' => Yii::t('app', $model->service_type)

                ],
                'service_play_type',
                'service_play_value',
                'service_code',
                'service_name',
                'service_amount',
                'service_order',
                'service_status',
                'service_created_at',
                'service_updated_at',
            ];
        } else {
            $gridColumn = [
                [ 'attribute' => 'service_type',

                  'value' => Yii::t('app', $model->service_type)

                ],
                //'service_play_type',
                //'service_play_value',
                //'service_code',
                'service_name',
                'service_amount',
                //'service_order',
                //'service_status',
                //'service_created_at',
                //'service_updated_at',
            ];
        }
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn
        ]);
        ?>
    </div>
    
    <div class="row">
        <?php
        if ($user & $user->username == 'kev') {
            $gridColumnPurchase = [
                [ 'class' => 'yii\grid\SerialColumn' ],
                [
                    'attribute' => 'user.id',
                    'label' => Yii::t('app', 'User'),
                ],
                [
                    'attribute' => 'service.service_type',
                    'label' => Yii::t('app', 'Service'),
                ],
                'purchase_amount',
                'purchase_play_id',
                'purchase_status',
                'purchase_valid_to',
                'purchase_valid_from',
                'purchase_created_at',
                'purchase_updated_at',
            ];
            echo Gridview::widget([
                'dataProvider' => $providerPurchase,
                'pjax' => true,
                'pjaxSettings' => [ 'options' => [ 'id' => 'kv-pjax-container' ] ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode(Yii::t('app', 'Purchase') . ': ' . $this->title) . ' </h3>',
                ],
                'columns' => $gridColumnPurchase
            ]);
        }
        ?>
    </div>
</div>