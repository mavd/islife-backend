<?php

use yii\helpers\Html;
use app\models\UserX;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('app', 'Services');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>

    td.service-type select {
        min-width: 110px;
    }
</style>
<div class="service-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Service'), [ 'create' ], [ 'class' => 'btn btn-success' ]) ?>
    </p>

    <?php
    $user = UserX::find()->me()->one();
    if ($user & $user->username == 'kev') {
        $gridColumn = [
            [ 'class' => 'yii\grid\SerialColumn' ],
            'service_type',
            'service_play_type',
            'service_play_value',
            'service_code',
            'service_name',
            'service_amount',
            'service_order',
            'service_status',
            //'service_created_at',
            //'service_updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ];
    } else {
        $gridColumn = [
            [ 'class' => 'yii\grid\SerialColumn' ],
            [ 'attribute' => 'service_type',
              'filterOptions' => [ 'class' => 'service-type' ],

              'filter' => [
                  'common' => 'Общая',
                  'one' => 'Одиночная',
                  'pair' => 'Парная' ],
              'content' => function ($data) {
                  return Yii::t('app', $data->service_type);
              }
            ],
            //'service_play_type',
            //'service_play_value',
            'service_code',
            'service_name',
            'service_amount',
            //'service_order',
            //'service_status',
            //'service_created_at',
            //'service_updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ];
    }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pager' => [
            'firstPageLabel' => '<span class="glyphicon glyphicon-triangle-left"></span>',
            'lastPageLabel' => '<span class="glyphicon glyphicon-triangle-right"></span>'

        ],
        'pjax' => true,
        'pjaxSettings' => [ 'options' => [ 'id' => 'kv-pjax-container' ] ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode($this->title) . ' </h3>',
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Страница',
            'fontAwesome' => true,
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Все',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Экспорт всей таблицы</li>',
                    ],
                ],
            ]),
        ],
    ]); ?>

</div>
