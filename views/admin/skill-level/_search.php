<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SkillLevelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="skill-level-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sl_name') ?>

    <?= $form->field($model, 'sl_level') ?>

    <?= $form->field($model, 'sl_low_edge') ?>

    <?= $form->field($model, 'sl_high_edge') ?>

    <?php // echo $form->field($model, 'sl_note') ?>

    <?php // echo $form->field($model, 'sl_created_at') ?>

    <?php // echo $form->field($model, 'sl_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
