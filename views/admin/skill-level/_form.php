<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SkillLevel */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="skill-level-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'sl_name')->textInput(['maxlength' => true, 'placeholder-x' => 'Sl Name']) ?>

    <?= $form->field($model, 'sl_level')->textInput(['placeholder-x' => 'Sl Level']) ?>

    <?= $form->field($model, 'sl_low_edge')->textInput(['placeholder-x' => 'Sl Low Edge']) ?>

    <?= $form->field($model, 'sl_high_edge')->textInput(['placeholder-x' => 'Sl High Edge']) ?>

    <?= $form->field($model, 'sl_note')->textInput(['placeholder-x' => 'Sl Note']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
