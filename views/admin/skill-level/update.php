<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SkillLevel */

$this->title = 'Редактировать Уровень: ' . ' ' . $model->sl_name;
$this->params['breadcrumbs'][] = ['label' => 'Уровни игры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="skill-level-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
