<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\SkillLevel */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => '������ ����', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="skill-level-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Skill Level'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'sl_name',
        'sl_level',
        'sl_low_edge',
        'sl_high_edge',
        'sl_note',
        'sl_created_at',
        'sl_updated_at',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>