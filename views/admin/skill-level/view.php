<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\SkillLevel */

$this->title =   $model->sl_name;
$this->params['breadcrumbs'][] = ['label' => 'Уровни игры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="skill-level-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Уровень:'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
                        
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'sl_name',
        'sl_level',
        'sl_low_edge',
        'sl_high_edge',
        'sl_note',


    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>