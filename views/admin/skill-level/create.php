<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SkillLevel */

$this->title = 'Добавить новый Уровень';
$this->params['breadcrumbs'][] = ['label' => 'Уровни игры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="skill-level-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
