<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SkillLevelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Уровни игры';
$this->params['breadcrumbs'][] = $this->title;
$search
                               = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="skill-level-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить новый', [ 'create' ], [ 'class' => 'btn btn-success' ]) ?>

    </p>


    <?php
    $gridColumn = [
        [ 'class' => 'yii\grid\SerialColumn' ],
        [ 'attribute' => 'id', 'hidden' => true ],
        'sl_name',
        'sl_level',
        'sl_low_edge',
        'sl_high_edge',
        'sl_note',

      //  'sl_updated_at',
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pager' => [
            'firstPageLabel' => '<span class="glyphicon glyphicon-triangle-left"></span>',
            'lastPageLabel' => '<span class="glyphicon glyphicon-triangle-right"></span>'

        ],
        'pjax' => true,
        'pjaxSettings' => [ 'options' => [ 'id' => 'kv-pjax-container' ] ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode($this->title) . ' </h3>',
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Страница',
            'fontAwesome' => true,

            'exportConfig' => [
                GridView::CSV => [ 'label' => 'Save as CSV' ],
                GridView::HTML => [ ],// html settings],
                GridView::PDF => [ false],// pdf settings],
                ExportMenu::FORMAT_PDF => false,

            ],
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false,
                ],
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Все',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Экспорт всей таблицы</li>',
                    ],
                ],
            ]),
        ],
    ]); ?>

</div>
