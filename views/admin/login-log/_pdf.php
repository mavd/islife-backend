<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\LoginLog */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Login Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-log-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Login Log').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'user.id',
            'label' => Yii::t('app', 'Device'),
        ],
        [
            'attribute' => 'device.id',
            'label' => Yii::t('app', 'Device'),
        ],
        'login_ip',
        'login_created_at',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>