<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LoginLog */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="login-log-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Device::find()->orderBy('user_id')->asArray()->all(), 'user_id', 'id'),
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Device')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'device_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Device::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Device')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'login_ip')->textInput(['maxlength' => true, 'placeholder-x' => 'Login Ip']) ?>

    <?= $form->field($model, 'login_created_at')->textInput(['placeholder-x' => 'Login Created At']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
