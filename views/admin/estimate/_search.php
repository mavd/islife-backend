<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EstimateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estimate-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'play_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'estimate_value') ?>

    <?= $form->field($model, 'estimate_status') ?>

    <?php // echo $form->field($model, 'estimate_comment') ?>

    <?php // echo $form->field($model, 'estimate_created_at') ?>

    <?php // echo $form->field($model, 'estimate_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
