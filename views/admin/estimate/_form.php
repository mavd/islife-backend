<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Estimate */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="estimate-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'play_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Play::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Play')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder-x' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'estimate_value')->textInput(['placeholder' => 'Estimate Value']) ?>

    <?= $form->field($model, 'estimate_status')->dropDownList([ 'new' => 'New', 'ready' => 'Ready', 'rejected' => 'Rejected', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'estimate_comment')->textInput(['maxlength' => true, 'placeholder' => 'Estimate Comment']) ?>

    <?= $form->field($model, 'estimate_created_at')->widget(\kartik\widgets\DateTimePicker::classname(), [
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Estimate Created At')],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'mm/dd/yyyy hh:ii:ss'
        ]
    ]) ?>

    <?= $form->field($model, 'estimate_updated_at')->widget(\kartik\widgets\DateTimePicker::classname(), [
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Estimate Updated At')],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'mm/dd/yyyy hh:ii:ss'
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
