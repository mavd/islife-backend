<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'task_name') ?>

    <?= $form->field($model, 'task_name2') ?>

    <?= $form->field($model, 'task_description') ?>

    <?= $form->field($model, 'task_type') ?>

    <?php // echo $form->field($model, 'task_timer_task') ?>
    <?php // echo $form->field($model, 'task_timer_minute') ?>

    <?php // echo $form->field($model, 'task_status') ?>

    <?php // echo $form->field($model, 'task_comment_reject') ?>

    <?php // echo $form->field($model, 'task_author_id') ?>

    <?php // echo $form->field($model, 'task_health') ?>

    <?php // echo $form->field($model, 'task_determination') ?>

    <?php // echo $form->field($model, 'task_communication') ?>

    <?php // echo $form->field($model, 'task_skill') ?>

    <?php // echo $form->field($model, 'task_created_at') ?>

    <?php // echo $form->field($model, 'task_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
