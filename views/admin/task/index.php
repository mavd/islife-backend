<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Задания';
$this->params['breadcrumbs'][] = $this->title;
$search
                               = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);

$this->registerJsFile(Yii::getAlias('@web/js/task.js'), [ 'depends' => [
    'yii\web\YiiAsset',
    'yii\bootstrap\BootstrapAsset'
],
]);
?>
<style>
    td.task-type select{
        min-width: 125px;
    }
    td.task-status select{
        min-width: 180px;
    }
</style>
<div class="task-index">

    <h1><?=  $this->title ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать Задание', [ 'create' ], [ 'class' => 'btn btn-success' ]) ?>

    </p>



    <?php
    $url        = \yii\helpers\Url::toRoute([ '#' ]);
    $gridColumn = [
        [ 'class' => 'yii\grid\SerialColumn' ],
        [ 'attribute' => 'id', 'hidden' => true ],
        'task_name',

        [
            'filterOptions' => ['class' => 'task-type'],

            'attribute'=> 'task_type',
            'filter'=>[  'both'=>'любое',
                         'one'=>'одиночное',
                         'pair'=>'парное'],
            'content'=>function($data){
                return Yii::t('task', $data->task_type)   ;
            },
        ],
        'task_timer_hour',
        'task_timer_minute',
        [
            'filterOptions' => ['class' => 'task-status'],
            'label' => 'Статус',
            'content'=>function($data){
                return Yii::t('task', $data->task_status)   ;
            },
            'attribute'=>'task_status',
            'filter'=>[ 'dirty'=>'Требует модерации','draft' => 'Черновик', 'rejected' => 'Отклонено', 'active' => 'Опубликовано', 'archived' => 'В архиве', ],
        ],
        //'task_status',
        //'task_health',
        'task_determination',
        'task_communication',
        'task_skill',
        [
            'class' => 'yii\grid\ActionColumn',

            'headerOptions' => [ 'style' => 'width:90px;text-align:center;' ],
            'template' => '{view} {update} {delete} {public}',
            'buttons' => [
                'public' => function ($url, $model) {

                    if ($model->task_status != 'active') {
                        $url = \yii\helpers\Url::toRoute(['/admin/task/public', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon   glyphicon-ok blocked"></span>',$url , [
                            'title' => 'Опубликовать',
                            'data-pjax' => '0', // нужно для отключения для данной ссылки стандартного обработчика pjax. Поверьте, он все портит
                            'class' => 'public-action' // указываем ссылке класс, чтобы потом можно было на него повесить нужный JS-обработчик
                        ]);
                    } else {
                        $url = \yii\helpers\Url::toRoute(['/admin/task/archive', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon  glyphicon-remove not-blocked"></span>', $url, [
                            'title' => 'В архив',
                            'data-pjax' => '0', // нужно для отключения для данной ссылки стандартного обработчика pjax. Поверьте, он все портит
                            'class' => 'archive-action' // указываем ссылке класс, чтобы потом можно было на него повесить нужный JS-обработчик
                        ]);
                    }
                },
            ]
        ]
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pager' => [
            'firstPageLabel' => '<span class="glyphicon glyphicon-triangle-left"></span>',
            'lastPageLabel' => '<span class="glyphicon glyphicon-triangle-right"></span>'

        ],
        'pjax' => true,
        'pjaxSettings' => [ 'options' => [ 'id' => 'kv - pjax - container' ] ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode($this->title) . ' </h3>',
        ],

    ]); ?>

</div>
