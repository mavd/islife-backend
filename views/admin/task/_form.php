<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Play', 
        'relID' => 'play', 
        'value' => \yii\helpers\Json::encode($model->plays),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'task_name')->textInput(['maxlength' => true, 'placeholder-x' => 'Task Name']) ?>

    <?= $form->field($model, 'task_name2')->textInput(['maxlength' => true, 'placeholder-x' => 'Task Name2']) ?>

    <?= $form->field($model, 'task_description')->textInput(['maxlength' => true, 'placeholder-x' => 'Task Description']) ?>

    <?= $form->field($model, 'task_type')->dropDownList([ 'one' => 'One', 'pair' => 'Pair', 'both' => 'Both', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'task_timer_hour')->textInput(['placeholder-x' => 'Часы на выполнение на задания']) ?>
    <?= $form->field($model, 'task_timer_minute')->textInput(['placeholder-x' => 'Минуты на выполнение на задания']) ?>

    <?= $form->field($model, 'task_status')->dropDownList(
        [
            'active' =>   'Опубликовано',
            'archived' => 'В архиве',
            'dirty'=>     'На модерации',
            'rejected' => 'Отклонен',
            'x_draft' =>  'Черновик',
        ],
        ['prompt' => 'Выберите статус']) ?>

    <?= $form->field($model, 'task_comment_reject')->textInput(['maxlength' => true, 'placeholder-x' => 'Task Comment Reject']) ?>

    <?= $form->field($model, 'task_author_id')->textInput(['placeholder-x' => 'Task Author']) ?>

<!--    --><?//= $form->field($model, 'task_health')->textInput(['placeholder-x' => 'Task Health']) ?>

    <?= $form->field($model, 'task_determination')->textInput(['placeholder-x' => 'Task Determination']) ?>

    <?= $form->field($model, 'task_communication')->textInput(['placeholder-x' => 'Task Communication']) ?>

    <?= $form->field($model, 'task_skill')->textInput(['placeholder-x' => 'Task Skill']) ?>



    <div class="form-group" id="add-play"></div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
