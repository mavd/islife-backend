<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Task'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'task_name',
        'task_name2',
        'task_description',
        'task_type',
        'task_timer_hour',
        'task_timer_minute',
        'task_status',
        'task_comment_reject',
        'task_author_id',
        'task_health',
        'task_determination',
        'task_communication',
        'task_skill',
        'task_created_at',
        'task_updated_at',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnPlay = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'task.id',
            'label' => 'Task',
        ],
        [
            'attribute' => 'user.id',
            'label' => 'User',
        ],
        'user2_id',
        'play_time_start',
        'play_time_end',
        'play_status',
        'play_result',
        'play_type',
        'play_purchase_id',
        'play_purchase2_id',
        'play_estimate_sum',
        'play_estimate_count',
        'play_bonuses',
        'play_comment',
        'play_created_at',
        'play_updated_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlay,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode('Play'.' '. $this->title) . ' </h3>',
        ],
        'columns' => $gridColumnPlay
    ]);
?>
    </div>
</div>