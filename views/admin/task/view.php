<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->title = $model->id . ' ' .  $model->task_name;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Задание:'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
                        
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'task_name',
        'task_name2',
        'task_description',

        [
            'label'  => 'Тип',
            'value' => Yii::t('task', $model->task_type)

        ],
        [
            'label'  => 'Время на выполнение часов',
            'value'  => $model->task_timer_hour ,

        ],
        [
            'label'  => 'Время на выполнение минут',
            'value'  =>  $model->task_timer_minute ,

        ],
        [
            'label'  => 'Статус',
            'value' => Yii::t('task', $model->task_status)

        ],

        'task_comment_reject',
        'task_author_id',
       // 'task_health',
        'task_determination',
        'task_communication',
        'task_skill',

    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
</div>