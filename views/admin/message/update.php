<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Message */
/* @var $user app\models|UserX */
$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Сообщения',
]) . ' ' . $model->id . ' ' . $model->message_header;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="message-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'user'=>$user
    ]) ?>

</div>
