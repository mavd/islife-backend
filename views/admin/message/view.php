<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Message */
/* @var $user app\models|UserX */
$this->title                   = $model->id . ' :: ' . $model->message_header;
$this->params['breadcrumbs'][] = [ 'label' => Yii::t('app', 'Messages'), 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Message') . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a(Yii::t('app', 'Update'), [ 'update', 'id' => $model->id ], [ 'class' => 'btn btn-primary' ]) ?>
            <?= Html::a(Yii::t('app', 'Delete'), [ 'delete', 'id' => $model->id ], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <?php
        $gridColumn = [
            [ 'attribute' => 'id', 'hidden' => true ],
            [
                'label' => 'Получатель',
                'value' => $user ?
                    "{$model->user_id} :: {$model->user->user_name} :: {$model->user->email}" :
                    "Сообщество IsLife"
            ],

            'message_type',

            'message_header',

            'message_status',
            'send_at',
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn  ,

        ]);
        ?>

        <div style="background-color: #d6e9c6;border:1px dodgerblue dotted">
           <?= strlen($model->message_text) > 500 ?
            substr($model->message_text, 0, 499) . ' ...':
            $model->message_text
            ?>
        </div>
    </div>
</div>