<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Message */
/* @var $user app\models|UserX */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="message-form">

    <?php

    if ($user) {
        echo "<h3>Получатель: {$user->id} :: {$user->user_name} :: {$user->email}</h3>";
    } else {
        echo "<h3>Получатель: Сообщество IsLife</h3>";
    }
    ?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'user_id', [ 'template' => '{input}' ])->hiddenInput() ?>
    <?= $form->field($model, 'message_mode', [ 'template' => '{input}' ])->hiddenInput() ?>

    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', [ 'template' => '{input}' ])->textInput([ 'style' => 'display:none' ]); ?>

    <?= $form->field($model, 'message_type')->dropDownList([ 'email' => 'Email', 'push' => 'Push', 'inform' => 'Inform', ], [ 'prompt' => '' ]) ?>

    <?= $form->field($model, 'message_header')->textInput([ 'maxlength' => true, 'placeholder-x' => 'Message Header' ]) ?>

      <?=
    $form->field($model, 'message_text')->widget(letyii\tinymce\Tinymce::className(), [
        'options' => [
            'class' => 'your_class',
        ]
    ]);
    ?>
    <?= $form->field($model, 'message_status')->dropDownList([ 'new' => 'New', 'send' => 'Send', ], [ 'prompt' => '' ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Изменить'), [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
