<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
$search
                               = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<style>
    td.message-status select{
        min-width: 110px;

</style>
<div class="message-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Message'), [ 'create' ], [ 'class' => 'btn btn-success' ]) ?>

    </p>



    <?php
    $gridColumn = [
        [ 'class' => 'yii\grid\SerialColumn' ],
        [ 'attribute' => 'id', 'hidden' => true ],
        'user_id',
        'message_type',
        'message_mode',
        'message_header',
        [
            'label' => 'Текст',
            'content' => function ($data) {
                if (strlen($data->message_text) > 500) {
                    return substr($data->message_text, 0, 499) . ' ...';
                } else {
                    return $data->message_text;
                }

            },
            'contentOptions' =>function ($model, $key, $index, $column){
                return ['class' => 'message_column'];
            },
        ],

        [ 'attribute' => 'message_status',
          'filterOptions' => ['class' => 'message-status'],
          'filter' => [
              'new' => 'Новое',
              'send' => 'Послано'
          ],
          'content' => function ($data) {
              return Yii::t('app', $data->message_status);
          },
        ],
        'send_at',
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pager' => [
            'firstPageLabel' => '<span class="glyphicon glyphicon-triangle-left"></span>',
            'lastPageLabel' => '<span class="glyphicon glyphicon-triangle-right"></span>'

        ],
        'pjax' => true,
        'pjaxSettings' => [ 'options' => [ 'id' => 'kv-pjax-container' ] ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode($this->title) . ' </h3>',
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Страница',
            'fontAwesome' => true,
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Все',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Экспорт всей таблицы</li>',
                    ],
                ],
            ]),
        ],
    ]); ?>

</div>
