<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel PurchaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('app', 'Отчеты по покупкам');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>

    td.purchase-status select {
        min-width: 110px;
    }
</style>
<div class="purchase-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?php
    $gridColumn
        = [ [ 'class' => 'yii\grid\SerialColumn' ],
            'user_id',
            'userName',
            'service_id',

            'serviceName',
            'purchase_amount',
            //'purchase_play_id',
            //[
            //    'filterOptions' => [ 'class' => 'purchase-status' ],
            //    'attribute' => 'purchase_status',
            //    'filter' => [
            //        'new' => 'Новая',
            //        'actived' => 'Активная',
            //        'used' => 'В архиве' ],
            //    'content' => function ($data) {
            //        return Yii::t('purchase', $data->purchase_status);
            //    } ],
            'purchase_valid_to',
            'purchase_valid_from',
            'purchase_created_at',
            //'purchase_updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
            ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pager' => [
            'firstPageLabel' => '<span class="glyphicon glyphicon-triangle-left"></span>',
            'lastPageLabel' => '<span class="glyphicon glyphicon-triangle-right"></span>'

        ],
        'pjax' => true,
        'pjaxSettings' => [ 'options' => [ 'id' => 'kv-pjax-container' ] ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ' . Html::encode($this->title) . ' </h3>',
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Страница',
            'fontAwesome' => true,
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Все',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Экспорт всей таблицы</li>',
                    ],
                ],
            ]),
        ],
    ]); ?>

</div>
