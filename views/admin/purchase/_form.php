<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Purchase */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="purchase-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'user_name'),
        'options' => ['placeholder-x' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'service_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Service::find()->orderBy('id')->asArray()->all(), 'id', 'service_name'),
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Service')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'purchase_amount')->textInput(['placeholder-x' => 'Purchase Amount']) ?>

<!--    --><?//= $form->field($model, 'purchase_play_id')->textInput(['placeholder-x' => 'Purshase Action']) ?>
<!---->
<!--    --><?//= $form->field($model, 'purchase_status')->dropDownList([ 'new' => 'Новая', 'actived' => 'Активная', 'used' => 'В архиве', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'purchase_valid_to')->widget(\kartik\widgets\DateTimePicker::classname(), [
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Purshase Valid To')],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'mm/dd/yyyy hh:ii:ss'
        ]
    ]) ?>

    <?= $form->field($model, 'purchase_valid_from')->widget(\kartik\widgets\DateTimePicker::classname(), [
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Purchase Valid From')],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'mm/dd/yyyy hh:ii:ss'
        ]
    ]) ?>

    <?= $form->field($model, 'purchase_created_at')->widget(\kartik\widgets\DateTimePicker::classname(), [
        'options' => ['placeholder-x' => Yii::t('app', 'Choose Purchase Created At')],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'mm/dd/yyyy hh:ii:ss'
        ]
    ]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
