<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MessageTemplate */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="message-template-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'mt_code')->textInput(['maxlength' => true, 'placeholder-x' => 'Mt Code']) ?>

    <?= $form->field($model, 'mt_type')->dropDownList([ 'email' => 'Email', 'push' => 'Push', 'inform' => 'Inform', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'mt_subject_template')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'mt_template')->textarea(['rows' => 6]) ?>





    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
