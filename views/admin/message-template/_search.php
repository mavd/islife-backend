<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MessageTemplateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="message-template-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'mt_code') ?>

    <?= $form->field($model, 'mt_type') ?>

    <?= $form->field($model, 'mt_template') ?>

    <?= $form->field($model, 'mt_subject_template') ?>

    <?php // echo $form->field($model, 'mt_created_at') ?>

    <?php // echo $form->field($model, 'mt_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
