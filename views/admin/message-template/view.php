<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MessageTemplate */

$this->title = $model->id . ' ' . $model->mt_code;
$this->params['breadcrumbs'][] = ['label' => 'Шаблон сообщений', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-template-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Шаблон'.': '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
                        
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'mt_code',
        'mt_type',
        'mt_template:ntext',
        'mt_subject_template:ntext',
        'mt_created_at',
        'mt_updated_at',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>