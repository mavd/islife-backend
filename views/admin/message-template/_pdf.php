<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MessageTemplate */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Message Template', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-template-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Message Template'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'mt_code',
        'mt_type',
        'mt_template:ntext',
        'mt_subject_template:ntext',
        'mt_created_at',
        'mt_updated_at',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>