<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name' => 'Is Life',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [ 'log' ],

    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',

    'components' => [
        'i18n' => [
            'translations' => [
                'task' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
                'play' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
                'purchase' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
                'estimate' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],

            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Ug6wKVElj0IS-Vw39oemyK-fq_MrGpDt',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => 3,//YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => [ 'error', 'warning' ],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info','trace' ],
                    'categories' => [ 'api' ],
                    'logFile' => '@app/runtime/logs/api.log',
                    'maxFileSize' => 1024 * 512,
                    'maxLogFiles' => 20,
                ],

            ],
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                [ 'class' => 'yii\rest\UrlRule',
                  'controller' => [ 'api/user', 'api/task', ]
                ],
                '' => 'site/index',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>'
            ]
        ],

        'apns' => [
            'class' => 'bryglen\apnsgcm\Apns',
            'environment' => \bryglen\apnsgcm\Apns::ENVIRONMENT_SANDBOX,
            'pemFile' => dirname(__FILE__).'/apnssert/aisLife-dev-cert.pem',
            // 'retryTimes' => 3,
            'options' => [
                'sendRetryTimes' => 5
            ]
        ],
        'gcm' => [
            'class' => 'bryglen\apnsgcm\Gcm',
            'apiKey' => 'AIzaSyCxg9l2xMgoHye8xm87vNSVlYS0KfTYRiA',
        ],
        // using both gcm and apns, make sure you have 'gcm' and 'apns' in your component
        'apnsGcm' => [
            'class' => 'bryglen\apnsgcm\ApnsGcm',
            // custom name for the component, by default we will use 'gcm' and 'apns'
            //'gcm' => 'gcm',
            //'apns' => 'apns',
        ],
    ],

    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'modelMap' => [
                'User' => 'app\models\User',
            ],
            'controllerMap' => [
                'admin' => 'app\controllers\user\AdminController'
            ],
            'admins' => [ 'admin','kev' ]
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
    ],
    'params' => $params,
];
if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
    $dbs = require(__DIR__ . '/dbLocal.php');
} else {
    $dbs = require(__DIR__ . '/db.php');
}
$config['components'] = array_merge($config['components'], $dbs);

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][]      = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][]    = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}
//var_dump($config) ;
return $config;
