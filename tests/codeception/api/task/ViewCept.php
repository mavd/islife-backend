<?php
$I = new ApiGuyTester($scenario);
$I->wantTo(' check view task detail from  REST API ');
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task','play'
    ]);
$I->comment('I fill db tasks, devices, users');

$I->sendGET('task/view',[
    'access-token'=>'q-1',
    'id'=>'-3',
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);

$at = $I->grabDataFromResponseByJsonPath('$.id');
$I->seeMyVar($at);
$I->seeResponseContainsJson(['id' => -3]);

?>
