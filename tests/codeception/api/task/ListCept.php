<?php
$I = new ApiGuyTester($scenario);
$I->wantTo(' check search task for play REST API ');
$c = $I->runSQLQueries(
    '/api/testDataDelete'
    );
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task'
    ]);
$I->comment('I fill db tasks, devices, users');

$I->sendGET('task/find',[
    'access-token'=>'q-1',
    'cnt'=>'3',
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseJsonMatchesJsonPath('$.one[0].id');
$at = $I->grabDataFromResponseByJsonPath('$.one[0].id');
$I->seeMyVar($at);

$I->seeResponseJsonMatchesJsonPath('$.pair[0].id');
$at = $I->grabDataFromResponseByJsonPath('$.pair[0].id');
$I->seeMyVar($at);
$I->comment('Player has tasks type one and pair');
$at = $I->dontSeeResponseJsonMatchesJsonPath('$.invite[0].id');
$I->comment('Player  don\'t has invites');

$I->comment('check Invited user Player-2');
$I->sendGET('task/find',[
    'access-token'=>'q-2',
    'cnt'=>'3',
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseJsonMatchesJsonPath('$.one[0].id');
$at = $I->grabDataFromResponseByJsonPath('$.one[0].id');
$I->seeMyVar($at);
$I->seeResponseJsonMatchesJsonPath('$.pair[0].id');
$at = $I->grabDataFromResponseByJsonPath('$.pair[0].id');
$I->seeMyVar($at);
$I->comment('Player has tasks type one and pair');
$at = $I->grabDataFromResponseByJsonPath('$.invite[0].id');
$I->seeMyVar($at);
$I->comment('Player  has invites also');

?>
