﻿<?php
$play_id = -3;
$task_id = -2;
$user_id = -1;
$user2_id = -2;
$I = new ApiGuyTester($scenario);
$I->wantTo(' check  reject for one play by REST API ');

$c = $I->runSQLQueries(
    '/api/testDataDelete' );
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task','play'
    ]);
$I->comment('I fill db tasks, devices, users,plays  ');

$I->seeInDatabase('play', [
    'id' => $play_id,
    'task_id'=>$task_id,
    'play_status' => 'started',
    'play_type'=>'one'
 ]);

$I->seeInDatabase('task', [
    'id' => $task_id,
    'task_skill' =>10
]);
$I->seeInDatabase('user', [
    'id' => $user_id,
    'user_skill' =>10
]);

$I->sendGET('play/set-rejected/', [
    'access-token' => 'q-1',
    'play_id' => $play_id
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->seeInDatabase('play', [ 'id' => $play_id, 'play_status' => 'done','play_result'=>'reject' ]);
$I->seeInDatabase('user', [
    'id' =>$user_id,
    'user_skill' =>5
]);
