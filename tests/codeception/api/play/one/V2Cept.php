﻿<?php
$play_id = -3;
$task_id = -2;
$user_id = -1;
$user2_id = -2;
$I = new ApiGuyTester($scenario);
$I->wantTo(' check  record and reject for one play by REST API V2');

$c = $I->runSQLQueries( '/api/testDataDelete' );
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task','play'
    ]);
$I->comment('I fill db tasks, devices, users,plays  ');

$I->seeInDatabase('play', [
    'id' => $play_id,
    'task_id'=>$task_id,
    'play_status' => 'started',
    'play_type'=>'one'
 ]);

$I->seeInDatabase('task', [
    'id' => $task_id,
    'task_skill' =>10
]);
$I->seeInDatabase('user', [
    'id' => $user_id,
    'user_skill' =>10
]);
$I->comment('I register new finished play ');
$I->sendPOST('play/record?access-token=q-1', [
    'task_id' => $play_id ,
    'mode' => 'one',
    'time_start' => date('Y-m-d H:i:s'),
    'time_end' => date('Y-m-d H:i:s',time()+600),
    'paid' => 0,
    'user2_id' => 0
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->comment('I register new rejected play ');
$I->sendPOST('play/reject?access-token=q-1', [
    'task_id' => $play_id ,
    'mode' => 'one',
    'time_start' => date('Y-m-d H:i:s'),
    'time_end' => date('Y-m-d H:i:s',time()+600),
    'paid' => 0,
    'user2_id' => 0,
    'paid2' => 0,
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
