<?php
$I = new ApiGuyTester($scenario);
$I->wantTo(' check search list of  plays REST API ');

$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task','play'
    ]);
$I->comment('I fill db tasks, devices, users');

$I->sendGET('play/list',[
    'access-token'=>'q-1',

]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseJsonMatchesJsonPath('$.plays.[0].id');


$I->comment('for Invited');
$I->sendGET('play/list',[
    'access-token'=>'q-2',

]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseJsonMatchesJsonPath('$.plays.[0].id');



?>
