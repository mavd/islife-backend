<?php

$task_id = -2;
$I = new ApiGuyTester($scenario);
$I->wantTo(' pair play life with SEND_REJECT via REST API ');
$c = $I->runSQLQueries(
    '/api/testDataDelete');
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user', 'task', 'estimate'
    ], true);
$I->comment('Created test  user,task,play');
$I->deleteFromDatabase('play', [ 'task_id' => $task_id, 'user_id' => -1 ]);
$I->dontSeeInDatabase('play', [ 'task_id' => $task_id, 'user_id' => -1 ]);
$I->sendPOST('play/go?access-token=q-1', [
    'task_id' => $task_id
]);

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->seeInDatabase('play', [ 'task_id' => $task_id, 'user_id' => -1 ]);

$play_id = $I->grabDataFromResponseByJsonPath('$.play.id')[0];
$I->seeMyVar(print_r($play_id, 1));
$I->comment('New play was created with id ' . $play_id);


//----------------------------------------------

//-----------------Искать второго игрока-----------------------------
$I->sendGET('play/search-second',[
    'access-token' => 'q-1'
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$at = $I->grabDataFromResponseByJsonPath('$.user.id');
$I->seeMyVar($at);


$user2_id = -2; //
$I->comment('В партнеры выбираем игрока с id=-2 ');
//----------------------------------------------
$query = 'update play set user2_id=' . $user2_id .
         ' where  id = ' . $play_id;
$I->execSQLQueries($query);
//-------------------Послать инвайт---------------------------
$I->sendPOST('play/send-invite?access-token=q-1',[
    'user_id' =>  $user2_id   ,
    'play_id' => $play_id
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);

//-------------------Послать отказ---------------------------
$I->sendPOST('play/send-reject?access-token=q-2',[

    'play_id' => $play_id
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);



$I->sendGET('play/set-started/', [
    'access-token' => 'q-1',
    'play_id' => $play_id
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'fail' ]);

