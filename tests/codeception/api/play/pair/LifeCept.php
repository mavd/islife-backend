<?php

$task_id = -2;
$I = new ApiGuyTester($scenario);
$I->wantTo(' pair play life circle via REST API ');
$c = $I->runSQLQueries(
    '/api/testDataDelete');
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user', 'task', 'estimate'
    ], true);
$I->comment('Created test  user,task,play');
$I->deleteFromDatabase('play', [ 'task_id' => $task_id, 'user_id' => -1 ]);
$I->dontSeeInDatabase('play', [ 'task_id' => $task_id, 'user_id' => -1 ]);
$I->sendPOST('play/go?access-token=q-1', [
    'task_id' => $task_id
]);

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->seeInDatabase('play', [ 'task_id' => $task_id, 'user_id' => -1 ]);

$play_id = $I->grabDataFromResponseByJsonPath('$.play.id')[0];
$I->seeMyVar(print_r($play_id, 1));
$I->comment('New play was created with id ' . $play_id);


//----------------------------------------------

//-----------------Искать второго игрока-----------------------------
$I->sendGET('play/search-second',[
    'access-token' => 'q-1'
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$at = $I->grabDataFromResponseByJsonPath('$.user.id');
$I->seeMyVar($at);


$user2_id = -2; //
$I->comment('В партнеры выбираем игрока с id=-2 ');
//----------------------------------------------
$query = 'update play set user2_id=' . $user2_id .
         ' where  id = ' . $play_id;
$I->execSQLQueries($query);
//-------------------Послать инвайт---------------------------
$I->sendPOST('play/send-invite?access-token=q-1',[
    'user_id' =>  $user2_id   ,
    'play_id' => $play_id
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);

//-------------------Послать согласиет---------------------------
$I->sendPOST('play/send-confirm?access-token=q-2',[

    'play_id' => $play_id
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);



$I->sendGET('play/set-started/', [
    'access-token' => 'q-1',
    'play_id' => $play_id
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->seeInDatabase('play', [ 'id' => $play_id, 'play_status' => 'started' ]);
//-----------------------------------------------
$I->sendGET('play/set-finished/', [
    'access-token' => 'q-1',
    'play_id' => $play_id
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->seeInDatabase('play', [ 'id' => $play_id, 'play_status' => 'finished' ]);

//----------------------------------------------
$I->sendGET('play/set-started/', [
    'access-token' => 'q-1',
    'play_id' => $play_id
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->seeInDatabase('play', [ 'id' => $play_id, 'play_status' => 'started' ]);
//-----------------------------------------------
$I->sendGET('play/set-finished/', [
    'access-token' => 'q-1',
    'play_id' => $play_id
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->seeInDatabase('play', [ 'id' => $play_id, 'play_status' => 'finished' ]);
//--------------------------------------------------
$I->comment('Report start');
$file  = codecept_data_dir('//files/095.png');
$file1 = codecept_data_dir('//files/049.png');
$file2 = codecept_data_dir('//files/051.png');
$I->sendPOST('play/report/?access-token=q-1',
    [
        'play_id' => $play_id
    ], [ 'file' => $file ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->seeNumRecords(1, 'report', [ 'play_id' => $play_id ]);

$I->sendPOST('play/report/?access-token=q-1',
    [
        'play_id' => $play_id
    ], [ 'file' => $file1 ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->seeNumRecords(2, 'report', [ 'play_id' => $play_id ]);

$I->sendPOST('play/report/?access-token=q-1',
    [
        'play_id' => $play_id
    ], [ 'file' => $file2 ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->seeNumRecords(3, 'report', [ 'play_id' => $play_id ]);

//-----------------------------------------------
$I->sendGET('play/set-reported/', [
    'access-token' => 'q-1',
    'play_id' => $play_id
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
$I->seeInDatabase('play', [ 'id' => $play_id, 'play_status' => 'reported' ]);
//--------------------------------------------------
// Оценки
//--------------------------------------------------

$I->sendGET('estimate/next', [
    'access-token' => 'e-11',

]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->dontSeeResponseContainsJson([ 'result' => 'fail' ]);


$play_e_id = $I->grabDataFromResponseByJsonPath('$.play.play_id')[0];
$I->seeMyVar(print_r($play_id, 1));
$query = 'update estimate set play_id=' . $play_id .
         ' where play_id = ' . $play_e_id . ' and user_id=-11; ';
$I->execSQLQueries($query);
$query = 'update play set play_estimate_sum=10, play_estimate_count= 4' .
         ' where  id = ' . $play_id . ' and user_id=-1';

$I->execSQLQueries($query);
$I->sendPOST('estimate/next?access-token=e-11',
    [
        'play_id' => $play_id,
        'estimate' => 3

    ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
