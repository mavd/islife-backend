<?php
$I = new ApiGuyTester($scenario);
$I->wantTo(' check view play detail from  REST API ');

$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user', 'task', 'play'
    ]);
$I->comment('I fill db tasks, devices, users');
$I->comment('check from author of play');
$I->sendGET('play/details', [
    'access-token' => 'q-1',
    'play_id'      => '-1',
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);

$at = $I->grabDataFromResponseByJsonPath('$.id');
$I->seeMyVar($at);
$I->seeResponseContainsJson(['id' => -1]);

$I->comment('check from invited side');
$I->sendGET('play/details', [
    'access-token' => 'q-2',
    'play_id'      => '-1',
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);

$at = $I->grabDataFromResponseByJsonPath('$.id');
$I->seeMyVar($at);
$I->seeResponseContainsJson(['id' => -1]);

$I->comment('check from other side');
$I->sendGET('play/details', [
    'access-token' => 'q-3',
    'play_id'      => '-1',
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);

$at = $I->grabDataFromResponseByJsonPath('$.result');
$I->seeMyVar($at);
$I->seeResponseContainsJson(['result' => 'fail']);

?>
