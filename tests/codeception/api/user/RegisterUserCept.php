<?php

$I = new ApiGuyTester($scenario);
$I->wantTo(' register new user via REST API');
$device = '{"name":"dev1", "os":"win2","api_id":"1234"}';
$I->deleteFromDatabase('user', ['email' => 'test5@codeception.com']);
$I->dontSeeInDatabase('user',['email' => 'test5@codeception.com']);
$I->comment('Игрок не зарегистрирован в БД');
$file = codecept_data_dir('//files/095.png');
$file1 = codecept_data_dir('//files/049.png');
$I->seeMyVar(['file'=>$file]);
$I->sendPOST('user-x/register',    [
        //'username' => 'test5',
        'email'     => 'test5@codeception.com',
        'password' => 'qqqq123',
        //'device'=>'device_id'
    ],
    ['file'=>$file]
);
//$I->seeResponseCodeIs(201);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(['result' => 'ok']);
$I->comment('Ответ на клиенте корректный');
$I->seeInDatabase('user',['email' => 'test5@codeception.com']);
$I->comment('В БД игрок зарегистрирован');
//----------------------------------------
$I->sendPOST('user-x/login',[
    'email'=>'test5@codeception.com',
    'password'=>'qqqq123',
    'device'=>$device
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(["result" => "ok"]);

$at = $I->grabDataFromResponseByJsonPath('$.access_token');
$I->seeMyVar($at);

$I->sendPOST('user-x/profile?access-token=' . $at[0],    [
    'user_name' => 'яtest5',
    'email'     => 'test5@codeception.com',
    'password' => 'qqqq123',
    'device'=>$device ,
    'user_birthday'=>'2000-01-01',
    'user_city'=>'qqqqq',
    'user_path'=>'dao'  ,
    'user_gender'=>'M'  ,
    'user_bonuses'=>200
],
    ['file'=>$file1]
);
//$I->seeResponseCodeIs(201);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(['result' => 'ok']);
$I->sendGET('user-x/profile?access-token=' . $at[0] );

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);



/******************************************/

$I->sendPOST('user-x/profile?access-token=' . $at[0],    [

],
    ['file'=>$file]
);
//$I->seeResponseCodeIs(201);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(['result' => 'ok']);
$I->sendGET('user-x/profile?access-token=' . $at[0] );

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
?>
