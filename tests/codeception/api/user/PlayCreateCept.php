<?php
$I = new ApiGuyTester($scenario);
$I->wantTo(' create new Play via REST API ');
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task','play'
    ]);
$device = '{"name":"dev1", "os":"win2","api_id":"1234"}';
$I->sendPOST('user-x/login',[
    'email'=>'test4@codeception.com',
    'password'=>'qqqq123',
    'device'=>$device
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeResponseContainsJson(["result" => "ok"]);
$at = $I->grabDataFromResponseByJsonPath('$.access_token');
$I->seeMyVar($at);
/*------------------------------------------*/
$I->comment('Login done');
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task','play'
    ]);
$I->comment('В БД добавлены игры с кодами -1,-2,-3');
$I->sendPOST('play/go?access-token=' . $at[0],[
    'task_id' => '-1'
]);

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(['result' => 'ok']);
?>
