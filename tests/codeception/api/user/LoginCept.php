<?php
$I = new ApiGuyTester($scenario);
$device = '{"name":"dev1", "os":"win2","api_id":"1234"}';
$I->wantTo(' check Login via REST API & view User Profile');
$I->sendPOST('user-x/login',[
    'email'=>'q@qq.ru',
    'password'=>'1',
    //'device'=>$device
    ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(["result" => "fail"]);


/*------------------------------------------*/

$I->sendPOST('user-x/login',[
    'email'=>'test5q@qqq.ru',
    'password'=>'bad password',
    'device'=>$device
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(["result" => "fail"]);


/*------------------------------------------*/

$I->sendPOST('user-x/login',[
    'email'=>'test4@codeception.com',
    'password'=>'qqqq123',
    'device'=>$device
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(["result" => "ok"]);

$at = $I->grabDataFromResponseByJsonPath('$.access_token');
$I->seeMyVar($at);
/*------------------------------------------*/
$I->sendGET('user-x/profile?access-token=' . $at[0] );

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);

?>
