﻿<?php
$I = new ApiGuyTester($scenario);
$I->wantTo(' check rating list of  gamer REST API ');

$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task','play'
    ]);
$I->comment('I fill db tasks, devices, users');

$I->sendGET('user-x/rate-list',[
    'access-token'=>'q-1',

]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(["result" => "ok"]);

?>
