<?php
$I = new ApiGuyTester($scenario);
$I->wantTo(' check Recovery via REST API');
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task','play'
    ]);
$I->sendPOST('user-x/recovery',[
    'email'=>'test5@errrormail.ru',
    ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(["result" => "fail"]);


/*------------------------------------------*/

$I->sendPOST('user-x/recovery',[
    'email'=>'test-5@codeception.com',
]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(["result" => "ok"]);



?>
