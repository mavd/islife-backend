<?php
$I = new ApiGuyTester($scenario);
$I->seeMyVar($scenario);

$I->wantTo(' check new uniques of username and email via REST API');

$I->sendPOST('user-x/check',['email'    => 'test-2@codeception.com',]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(["result" => "fail"]);

/*------------------------------------------*/
$I->sendPOST('user-x/check',['username' => 'test-1',]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(["result" => "fail"]);
/*------------------------------------------*/
$I->sendPOST('user-x/check',['email'    => 'testqqqqqqqq2@qqqqqqqqqqcodeception.com',]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(["result" => "ok"]);

?>
