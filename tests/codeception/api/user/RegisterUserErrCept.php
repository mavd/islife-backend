<?php
$I = new ApiGuyTester($scenario);
$I->seeMyVar($scenario);

$I->wantTo(' register new user via REST API with error');

$I->sendPOST('user-x/register',
    [
        'username' => 'test5',
        'email'    => 'test-1@codeception.com',
        'password' => 'qqqq123',
    ]
);

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->sendPOST('user-x/register',
    [
        'username' => 'test5',
        'email'    => 'test-1@codeception.com',
        'password' => 'qqqq123',
    ]
);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);

$I->seeResponseContainsJson(["result" => "fail"]);


/*------------------------------------------*/
$I->sendPOST('user-x/register',
    [
        'username' => 'test-1',
        'email'    => 'test-2@codeception.com',
        //'password' => 'qqqq123',
    ]
);

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson(["result" => "fail"]);


?>
