﻿<?php

$user_id = -1;
$user2_id = -2;
$I = new ApiGuyTester($scenario);
$I->wantTo(' check  add cash by REST API ');

$c = $I->runSQLQueries(
    '/api/testDataDelete' );
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user'
    ]);
$I->comment('I fill db   users,plays  ');



$I->seeInDatabase('user', [
    'id' => $user_id,
    'user_skill' =>10,
    'user_amount' =>100
]);
$I->sendPOST('purchase/cash?access-token=q-1', [
    'debit' => 111
]);

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);

$I->seeInDatabase('user', [
    'id' => $user_id,
    'user_skill' =>10,
    'user_amount' =>211
]);