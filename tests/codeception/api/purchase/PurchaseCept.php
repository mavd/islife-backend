<?php

$user_id = -1;
$user2_id = -2;
$I = new ApiGuyTester($scenario);
$I->wantTo(' check  purchase by REST API ');

$c = $I->runSQLQueries(
    '/api/testDataDelete' );
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user'
    ]);
$I->comment('I fill db   users,plays  ');



$I->seeInDatabase('user', [
    'id' => $user_id,
    'user_skill' =>10,
    'user_amount' =>100
]);
$I->dontSeeInDatabase('purchase', [
    'user_id' => $user_id

]);
$I->sendGET('user-x/profile?access-token=q-1'   );

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->sendPOST('purchase/on-store?access-token=q-1', [
    'service_code' => '1',
    'service_param' => '5',
    'date_purchase' => '2015-10-17 16:44:03',
    'date_start' => '',
    'date_end' => '2015-10-17 18:09:23'
]);

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);

$I->seeInDatabase('purchase', [
    'user_id' => $user_id

]);
$I->sendGET('user-x/profile?access-token=q-1'   );

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);

$at = $I->grabDataFromResponseByJsonPath('$.x2.one');
$I->seeMyVar($at);

$I->sendPOST('purchase/on-store?access-token=q-1', [
    'service_code' => '1',
    'service_param' => '5',
    'date_purchase' => '2015-10-17 16:44:03',
    'date_start' => '',
    'date_end' => '2015-10-18 18:09:23'
]);

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
/***************************************/
//$I->sendPOST('purchase/on-cash?access-token=q-1', [
//    'service_id' => 6
//]);
//
//$I->seeResponseIsJson();
//$res = $I->grabResponse();
//$I->seeMyVar($res);
//$I->seeResponseContainsJson([ 'result' => 'ok' ]);
//
//$I->seeInDatabase('purchase', [
//    'user_id' => $user_id
//
//]);
//$I->sendGET('user-x/profile?access-token=q-1'   );
//
//$I->seeResponseIsJson();
//$res = $I->grabResponse();
//$I->seeMyVar($res);
//
//$at = $I->grabDataFromResponseByJsonPath('$.x2.pair');
//$I->seeMyVar($at);