﻿<?php

$user_id = -1;
$user2_id = -2;
$I = new ApiGuyTester($scenario);
$I->wantTo(' I want to get list services by REST API ');


$I->sendGet('purchase/services',[
    'access-token'=>'q-1'
]);

$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->seeResponseContainsJson([ 'result' => 'ok' ]);
