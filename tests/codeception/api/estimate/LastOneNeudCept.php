<?php
$I = new ApiGuyTester($scenario);
$I->wantTo(' check  last estimate of play and Neud by REST API ');

$c = $I->runSQLQueries(
    '/api/testDataDelete',
    [
        'user','task','play'
    ]);
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task','play','estimate'
    ]);
$I->comment('I fill db tasks, devices, users,plays, and estimate ');

$I->seeInDatabase('play', [
    'id' => -2,
    'task_id'=>-1,
    'play_status' => 'reported',
    'play_estimate_sum'=>8,
    'play_estimate_count'=>4]);
$I->seeInDatabase('estimate', [
    'play_id' => -2,
    'user_id' => -11,
    'estimate_value' => 0,
    'estimate_status'=>'new'
]);
$I->seeInDatabase('task', [
    'id' => -1,
    'task_skill' =>10
]);
$I->seeInDatabase('user', [
    'id' => -1,
    'user_skill' =>10
]);
$I->sendPOST('estimate/next?access-token=e-11',
    [
        'play_id' => -2,
        'estimate' => 1

    ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->dontSeeResponseContainsJson([ 'result' => 'fail' ]);
$I->seeInDatabase('play', [
    'id' => -2,
    'play_status' => 'done',
    'play_result' => 'fail',
    'play_estimate_sum'=>9,
    'play_estimate_count'=>5]);
$I->seeInDatabase('estimate', [
    'play_id' => -2,
    'user_id' => -11,
    'estimate_value' => 1,
    'estimate_status'=>'ready'
]);
$I->comment("если 'withdrawal_percentage' => 50 //процент снятия баллов с пользователя при неудачной игре') то следующее утверждение сработает");
$I->seeInDatabase('user', [
    'id' => -1,
    'user_skill' =>5
]);
$I->comment('estimate success!<br> User and Play updated correctly');



