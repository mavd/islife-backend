<?php
$play_id = -4;
$I       = new ApiGuyTester($scenario);
$I->wantTo(' check  last estimate of play by REST API ');

$c = $I->runSQLQueries('/api/testDataDelete');
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user', 'task', 'play', 'estimate'
    ]);
$I->comment('I fill db tasks, devices, users,plays, and estimate ');

$I->seeInDatabase('play', [
    'id' => $play_id,
    'task_id' => -1,
    'play_status' => 'reported',
    'play_estimate_sum' => 8,
    'play_estimate_count' => 4 ]);
$I->seeInDatabase('estimate', [
    'play_id' => $play_id,
    'user_id' => -11,
    'estimate_value' => 0,
    'estimate_status' => 'new'
]);
$I->seeInDatabase('task', [
    'id' => -1,
    'task_skill' => 10
]);
$I->seeInDatabase('user', [
    'id' => -1,
    'user_skill' => 10
]);
$I->seeInDatabase('user', [
    'id' => -2,
    'user_skill' => 10
]);
$I->sendPOST('estimate/next?access-token=e-11',
    [
        'play_id' => $play_id,
        'estimate' => 5

    ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->dontSeeResponseContainsJson([ 'result' => 'fail' ]);
$I->seeInDatabase('play', [
    'id' => $play_id,
    'play_status' => 'done',
    'play_result' => 'success',
    'play_estimate_sum' => 13,
    'play_estimate_count' => 5 ]);
$I->seeInDatabase('estimate', [
    'play_id' => $play_id,
    'user_id' => -11,
    'estimate_value' => 5,
    'estimate_status' => 'ready'
]);
$I->seeInDatabase('user', [
    'id' => -1,
    'user_skill' => 20
]);
$I->seeInDatabase('user', [
    'id' => -2,
    'user_skill' => 20
]);
$I->comment('estimate success!<br> User and Play updated correctly');



