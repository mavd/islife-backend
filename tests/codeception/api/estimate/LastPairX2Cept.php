<?php

$I = new ApiGuyTester($scenario);
$I->wantTo(' check  last estimate of play with Pair and X2 by REST API ');

$c = $I->runSQLQueries(
    '/api/testDataDelete',
    [
        'user','task','play'
    ]);
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task','play','estimate'
    ]);
$I->comment('I fill db tasks, devices, users,plays, and estimate ');
$play_id =-6;
$I->seeInDatabase('play', [
    'id' => $play_id,
    'task_id'=>-1,
    'user_id'=>-1,
    'user2_id'=>-2,
    'play_status' => 'reported',
    'play_purchase_id'=>1,
    'play_purchase2_id'=>0,
    'play_estimate_sum'=>8,
    'play_estimate_count'=>4]);
$I->comment('В игре 2 игрока, опция X2 только у первого');
$I->seeInDatabase('estimate', [
    'play_id' => $play_id,
    'user_id' => -11,
    'estimate_value' => 0,
    'estimate_status'=>'new'
]);
$I->seeInDatabase('task', [
    'id' => -1,
    'task_skill' =>10
]);
$I->seeInDatabase('user', [
    'id' => -1,
    'user_skill' =>10
]);
$I->seeInDatabase('user', [
    'id' => -2,
    'user_skill' =>10
]);
$I->sendPOST('estimate/next?access-token=e-11',
    [
        'play_id' => $play_id,
        'estimate' => 5

    ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->dontSeeResponseContainsJson([ 'result' => 'fail' ]);
$I->seeInDatabase('play', [
    'id' =>$play_id,
    'play_status' => 'done',
    'play_result' => 'success',
    'play_estimate_sum'=>13,
    'play_estimate_count'=>5]);
$I->seeInDatabase('estimate', [
    'play_id' =>$play_id,
    'user_id' => -11,
    'estimate_value' => 5,
    'estimate_status'=>'ready'
]);
$I->seeInDatabase('user', [
    'id' => -1,
    'user_skill' =>30
]);
$I->seeInDatabase('user', [
    'id' => -2,
    'user_skill' =>20
]);
$I->comment('Проверена ситуация, у первого игрока есть X2, у вторго нет<br> estimate success!<br> User and Play updated correctly');

$play_id =-7;
$I->seeInDatabase('play', [
    'id' => $play_id,
    'task_id'=>-1,
    'user_id'=>-1,
    'user2_id'=>-2,
    'play_status' => 'reported',
    'play_purchase_id'=>0,
    'play_purchase2_id'=>1,
    'play_estimate_sum'=>8,
    'play_estimate_count'=>4]);
$I->comment('В игре 2 игрока, опция X2 только у второго');
$I->seeInDatabase('estimate', [
    'play_id' => $play_id,
    'user_id' => -11,
    'estimate_value' => 0,
    'estimate_status'=>'new'
]);
$I->seeInDatabase('task', [
    'id' => -1,
    'task_skill' =>10
]);
$I->seeInDatabase('user', [
    'id' => -1,
    'user_skill' =>30
]);
$I->seeInDatabase('user', [
    'id' => -2,
    'user_skill' =>20
]);
$I->sendPOST('estimate/next?access-token=e-11',
    [
        'play_id' => $play_id,
        'estimate' => 5

    ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->dontSeeResponseContainsJson([ 'result' => 'fail' ]);
$I->seeInDatabase('play', [
    'id' =>$play_id,
    'play_status' => 'done',
    'play_result' => 'success',
    'play_estimate_sum'=>13,
    'play_estimate_count'=>5]);
$I->seeInDatabase('estimate', [
    'play_id' =>$play_id,
    'user_id' => -11,
    'estimate_value' => 5,
    'estimate_status'=>'ready'
]);
$I->seeInDatabase('user', [
    'id' => -1,
    'user_skill' =>40
]);
$I->seeInDatabase('user', [
    'id' => -2,
    'user_skill' =>40
]);
$I->comment('Проверена ситуация, у первого игрока нет X2, у вторго есть<br> estimate success!<br> User and Play updated correctly');



$play_id =-8;
$I->seeInDatabase('play', [
    'id' => $play_id,
    'task_id'=>-1,
    'user_id'=>-1,
    'user2_id'=>-2,
    'play_status' => 'reported',
    'play_purchase_id'=>1,
    'play_purchase2_id'=>1,
    'play_estimate_sum'=>8,
    'play_estimate_count'=>4]);
$I->comment('В игре 2 игрока, опция X2 у обоих игроков');
$I->seeInDatabase('estimate', [
    'play_id' => $play_id,
    'user_id' => -11,
    'estimate_value' => 0,
    'estimate_status'=>'new'
]);
$I->seeInDatabase('task', [
    'id' => -1,
    'task_skill' =>10
]);
$I->seeInDatabase('user', [
    'id' => -1,
    'user_skill' =>40
]);
$I->seeInDatabase('user', [
    'id' => -2,
    'user_skill' =>40
]);
$I->sendPOST('estimate/next?access-token=e-11',
    [
        'play_id' => $play_id,
        'estimate' => 5

    ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->dontSeeResponseContainsJson([ 'result' => 'fail' ]);
$I->seeInDatabase('play', [
    'id' =>$play_id,
    'play_status' => 'done',
    'play_result' => 'success',
    'play_estimate_sum'=>13,
    'play_estimate_count'=>5]);
$I->seeInDatabase('estimate', [
    'play_id' =>$play_id,
    'user_id' => -11,
    'estimate_value' => 5,
    'estimate_status'=>'ready'
]);
$I->seeInDatabase('user', [
    'id' => -1,
    'user_skill' =>60
]);
$I->seeInDatabase('user', [
    'id' => -2,
    'user_skill' =>60
]);
$I->comment('Проверена ситуация, у первого игрока нет X2, у вторго есть<br> estimate success!<br> User and Play updated correctly');




