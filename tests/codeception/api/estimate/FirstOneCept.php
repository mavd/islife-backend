<?php
$I = new ApiGuyTester($scenario);
$I->wantTo(' check first estimate of play by REST API ');
$c = $I->runSQLQueries(
    '/api/testDataDelete',
    [
        'user','task','play'
    ]);
$c = $I->runSQLQueries(
    '/api/testDataCreate',
    [
        'user','task','play','estimate'
    ]);
$I->comment('I fill db tasks, devices, users,plays, and estimate');

$I->seeInDatabase('play', [
    'id' => -1,
    'play_status' => 'reported',
    'play_estimate_sum'=>0,
    'play_estimate_count'=>0]);
$I->seeInDatabase('estimate', [
    'play_id' => -1,
    'user_id' => -11,
    'estimate_value' => 0,
    'estimate_status'=>'new'
    ]);
$I->sendPOST('estimate/next?access-token=e-11',
    [
        'play_id' => -1,
        'estimate' => 3

    ]);
$I->seeResponseIsJson();
$res = $I->grabResponse();
$I->seeMyVar($res);
$I->dontSeeResponseContainsJson([ 'result' => 'fail' ]);
$I->seeInDatabase('play', [
    'id' => -1,
    'play_status' => 'reported',
    'play_estimate_sum'=>3,
    'play_estimate_count'=>1]);
$I->seeInDatabase('estimate', [
    'play_id' => -1,
    'user_id' => -11,
    'estimate_value' => 3,
    'estimate_status'=>'ready'
]);
$I->comment('estimate success!')


?>
