<?php
namespace Codeception\Module;

// here you can define custom functions for WebGuy 

class DbHelper extends \Codeception\Module
{

    public function execSQLQueries($sql)
    {
        if (!is_array($sql)){
           // $sql = [$sql];
        }
        $dbh    = $this->getModule('Db')->dbh;
        $sth = $dbh->prepare($sql);
        return $sth->execute();
        //$dbh = $this->getModule('Db');
        //$dbh->driver->load( $sql );
    }

    /**
     * Run all sql files in a given directory
     */
    public function runSQLQueries($dir = null, $subDirs = [ ], $toLog = false)
    {

        if (!is_null($dir)) {

            /**
             * We need the Db module to run the queries
             */
            $dbh = $this->getModule('Db');

            /**
             * The Db driver load function requires an array
             */
            $queries = array();
            $path    = codecept_data_dir() . $dir;
            /**
             * Get all the queries in the directory
             */
            foreach (glob($path . '/*.sql') as $sqlFile) {
                $query = file_get_contents($sqlFile);
                array_push($queries, $query);
            }

            if (count($subDirs)) {
                foreach ($subDirs as $subDir) {
                    /**
                     * Get all the queries in the sub directory
                     */
                    foreach (glob($path . '/' . $subDir . '/*.sql') as $sqlFile) {
                        $query = file_get_contents($sqlFile);
                        array_push($queries, $query);
                    }
                }
            }
            /**
             * If there are queries load them
             */
            if (count($queries) > 0) {
                if ($toLog) {
                    file_put_contents($path . '/slq_result.log', $queries);
                }


                $dbh->driver->load($queries);
            }

        }
    }

    /**
     * Delete entries from $table where $criteria conditions
     * Use: $I->deleteFromDatabase('users', array('id' => '111111', 'banned' => 'yes'));
     *
     * @param  string $table   tablename
     * @param  array $criteria conditions. See seeInDatabase() method.
     *
     * @return boolean Returns TRUE on success or FALSE on failure.
     */
    public function deleteFromDatabase($table, $criteria)
    {
        $dbh    = $this->getModule('Db')->dbh;
        $query  = "delete from %s where %s";
        $params = array();
        foreach ($criteria as $k => $v) {
            $params[] = "$k = ?";
        }
        $params = implode(' AND ', $params);
        $query  = sprintf($query, $table, $params);
        $this->debugSection('Query', $query, json_encode($criteria));
        $sth = $dbh->prepare($query);
        return $sth->execute(array_values($criteria));
    }
}
