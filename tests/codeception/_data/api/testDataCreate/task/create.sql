delete from task where id<0;
INSERT INTO `task`(
    `id`, `task_name`, `task_description`, `task_type`, `task_timer_hour`,
    `task_status`, `task_comment_reject`, `task_author_id`, `task_health`,
    `task_determination`, `task_communication`, `task_skill`, `task_created_at`, `task_updated_at`)
    VALUES (-1, 'task -1', 'task -1 task -1 task -1 task -1 ', 'one', 23,
     'active', '', 10, 10, 10, 10, 10, '2015-08-23 21:27:58', '2015-08-23 21:27:58');

INSERT INTO `task`(
    `id`, `task_name`, `task_description`, `task_type`, `task_timer_hour`,
    `task_status`, `task_comment_reject`, `task_author_id`, `task_health`,
    `task_determination`, `task_communication`, `task_skill`, `task_created_at`, `task_updated_at`)
    VALUES (-2, 'task -2', 'task -2 task -2 task -2 task -2 ', 'pair', 23,
     'active', '', 10, 10, 10, 10, 10, '2015-08-23 21:27:58', '2015-08-23 21:27:58');

INSERT INTO `task`(
    `id`, `task_name`, `task_description`, `task_type`, `task_timer_hour`,
    `task_status`, `task_comment_reject`, `task_author_id`, `task_health`,
    `task_determination`, `task_communication`, `task_skill`, `task_created_at`, `task_updated_at`)
    VALUES (-3, 'task -3', 'task -3 task -3 task -3 task -3 ', 'both', 23,
     'active', '', 1, 1, 1, 1, 1, '2015-08-23 21:27:58', '2015-08-23 21:27:58');


