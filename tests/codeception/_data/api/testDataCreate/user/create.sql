
delete from user where id<0;
INSERT INTO `user` (
`id`,                `username`            , `email`          , `password_hash`     , `auth_key`          , `confirmed_at`,
`unconfirmed_email`, `blocked_at`          , `registration_ip`, `created_at`        , `updated_at`        , `flags`,
`user_isAdmin`     , `user_photo`          , `user_gender`    , `user_birthday`     , `user_city`         , `user_path`,
`user_status`      , `user_blocked_comment`, `user_health`    , `user_determination`, `user_communication`, `user_skill`,
`user_amount`      , `user_last_device_id`)
VALUES (
-1      , 'test-1', 'test-1@codeception.com', 'x123', 'x123', 1439631966,
NULL    ,  NULL   , '127.0.0.1', 1440791903, 1440791925, 0,
0       , '/web/uploads/tests//users/12588/095.png', 'M', '2000-08-21', '������', '1',
'new',  NULL   , 10 , 10, 10, 10,
100     , -1),

 (
-2      , 'test-2', 'test-2@codeception.com', 'x123', 'x123', 1439631966,
NULL    ,  NULL   , '127.0.0.2', 1440791903, 1440791925, 0,
0       , '/web/uploads/tests//users/12588/095.png', 'M', '2000-08-22', '������', '1',
'new',  NULL   , 10 , 10, 10, 10,
100     , -2),

 (
-3      , 'test-3', 'test-3@codeception.com', 'x123', 'x123', 1439631966,
NULL    ,  NULL   , '127.0.0.3', 1440791903, 1440791925, 0,
0       , '/web/uploads/tests//users/12588/095.png', 'F', '2000-08-23', '������', '1',
'new',  NULL   , 10 , 10, 10, 10,
100     , -3),

 (
-4      , 'test-4', 'test-4@codeception.com', 'x123', 'x123', 1439631966,
NULL    ,  NULL   , '127.0.0.4', 1440791903, 1440791925, 0,
0       , '/web/uploads/tests//users/12588/095.png', 'F', '2000-08-24', '������', '1',
'new',  NULL   , 10 , 10, 10, 10,
100     , -4),


 (
-5      , 'test-5', 'test-5@codeception.com', 'x123', 'x123', 1439631966,
NULL    ,  NULL   , '127.0.0.5', 1440791903, 1440791925, 0,
0       , '/web/uploads/tests//users/12588/095.png', 'F', '2000-08-24', '������', '1',
'blocked',  NULL   , 10 , 10, 10, 10,
100     , -5),

 (
-6      , 'test-6', 'test-6@codeception.com', 'x123', 'x123', 1439631966,
NULL    ,  NULL   , '127.0.0.6', 1440791903, 1440791925, 0,
0       , '/web/uploads/tests//users/12588/095.png', 'F', '2000-08-26', '������', '1',
'new',  NULL   , 10 , 10, 10, 10,
100     , -6)
;

delete from device where id<0;
INSERT INTO `device` (`id`, `user_id`, `device_access_token`, `device_access_token_expire`, `device_name`, `device_os`, `device_api_id`, `device_created_at`, `device_last_login_at`)
VALUES
(-1, -1, 'q-1', '2017-08-29 06:58:45', 'dev1', 'win2', '1234', '2015-08-29 02:58:45', '2015-08-29 02:58:45'),
(-2, -2, 'q-2', '2017-08-29 06:58:45', 'dev1', 'win2', '1234', '2015-08-29 02:58:45', '2015-08-29 02:58:45'),
(-3, -3, 'q-3', '2017-08-29 06:58:45', 'dev1', 'win2', '1234', '2015-08-29 02:58:45', '2015-08-29 02:58:45'),
(-4, -4, 'q-4', '2017-08-29 06:58:45', 'dev1', 'win2', '1234', '2015-08-29 02:58:45', '2015-08-29 02:58:45'),
(-5, -5, 'q-5', '2017-08-29 06:58:45', 'dev1', 'win2', '1234', '2015-08-29 02:58:45', '2015-08-29 02:58:45'),
(-6, -6, 'q-6', '2017-08-29 06:58:45', 'dev1', 'win2', '1234', '2015-08-29 02:58:45', '2015-08-29 02:58:45');

