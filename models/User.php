<?php
namespace app\models;

use dektrium\user\models\User as BaseUser;
use dektrium\user\models\Token;

/**
 * Class User - расширение класса dektrium\user, добавлены новые поля, используется для совместимости
 * вся функциональность связанная с игроками вынесена в класс UserE
 *
 * @package app\models
 */
class User extends BaseUser {
    /**
     * This is the model class for table "user".
     *
     * @property integer $user_isAdmin
     * @property string $user_photo
     * @property string $user_gender
     * @property integer $user_birthday
     * @property string $user_city
     * @property string $user_path
     * @property string $user_status
     **/

    public function scenarios() {
        $scenarios = parent::scenarios();
        // add field to scenarios
        array_push($scenarios['create'],  'user_isAdmin', 'user_photo', 'user_gender', 'user_birthday', 'user_city', 'user_path');
        array_push($scenarios['update'], 'user_isAdmin', 'user_photo', 'user_gender', 'user_birthday', 'user_city', 'user_path','user_status');
        array_push($scenarios['settings'], 'user_isAdmin', 'user_photo', 'user_gender', 'user_birthday', 'user_city', 'user_path','user_status');
        array_push($scenarios['register'],  'user_isAdmin', 'user_photo', 'user_gender', 'user_birthday', 'user_city', 'user_path');

        return $scenarios;
    }
    /** @inheritdoc */


    /** @inheritdoc */
    public function rules()
    {
        return [
            // username rules
            'usernameRequired' => ['username', 'required', 'on' => ['update']],
            'usernameMatch' => ['username', 'match', 'pattern' => '/^[-a-zA-Z0-9_\.@]+$/'],
            'usernameLength' => ['username', 'string', 'min' => 3, 'max' => 25],
            'usernameUnique' => ['username', 'unique'],
            'usernameTrim' => ['username', 'trim'],

            // email rules
            'emailRequired' => ['email', 'required', 'on' => ['register', 'connect', 'create', 'update']],
            'emailPattern' => ['email', 'email'],
            'emailLength' => ['email', 'string', 'max' => 255],
            'emailUnique' => ['email', 'unique'],
            'emailTrim' => ['email', 'trim'],

            // password rules
            'passwordRequired' => ['password', 'required', 'on' => ['register']],
            'passwordLength' => ['password', 'string', 'min' => 6, 'on' => ['register', 'create']],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Login',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'confirmed_at' => 'Confirmed At',
            'unconfirmed_email' => 'Unconfirmed Email',
            'blocked_at' => 'Blocked At',
            'registration_ip' => 'Registration Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'flags' => 'Flags',
            'user_isAdmin' => 'Является администратором',
            'user_photo' => 'Фото',
            'user_gender' => 'Пол',
            'user_birthday' => 'Дата рождения',
            'user_city' => 'Город',
            'user_path' => 'Путь',
            'user_status' => 'Статус',
            'user_blocked_comment' => 'Комментарий к блокировке',
            'user_health' => 'Здоровье',
            'user_determination' => 'Смелость',
            'user_communication' => 'Общительность',
            'user_skill' => 'Опыт',
            'user_amount' => 'Остаток на счету',
            'user_last_device_id' => 'Последнее устройство, с которого заходил пользователь',
        ];
    }
    public static function findIdentityByAccessToken($token, $type = null)
    {
        if ($token==='1'){
            return static::findOne(['id' => 1]);
        }
        $user_id = Device::find()->byToken($token)->scalar();
        $log= [
            'post'=>$_POST,
            'get'=>$_GET,
            'REQUEST_URI'=>$_SERVER['REQUEST_URI'],
           // 'REMOTE_ADDR'=>$_SERVER['REMOTE_ADDR']
        ];
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/runtime/logs/' . date('Y_m_d_h') . '_queryes.log',
            "\n============". date('H:i:s') . ":::" . $_SERVER['REMOTE_ADDR'] ."=====================\n" .
            print_r($log,1),FILE_APPEND
        );
        if ($user_id){
            return static::findOne(['id' => $user_id]);
        }
        return null;
    }
    /**
     * This method is used to register new user account. If Module::enableConfirmation is set true, this method
     * will generate new confirmation token and use mailer to send it to the user. Otherwise it will log the user in.
     * If Module::enableGeneratingPassword is set true, this method will generate new 8-char password. After saving user
     * to database, this method uses mailer component to send credentials (username and password) to user via email.
     *
     * @return bool
     */
    public function register()
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        if ($this->module->enableConfirmation == false) {
            $this->confirmed_at = time();
        }



        $this->trigger(self::USER_REGISTER_INIT);

        if ($this->save()) {
            $this->trigger(self::USER_REGISTER_DONE);
            if ($this->module->enableConfirmation) {
                $token = \Yii::createObject([
                    'class' => Token::className(),
                    'type'  => Token::TYPE_CONFIRMATION,
                ]);
                $token->link('user', $this);
                $this->mailer->viewPath = '@app/views/mail';
                $this->mailer->sendConfirmationMessage($this, $token);
            } else {
                \Yii::$app->user->login($this);
            }
            if ($this->module->enableGeneratingPassword) {
                $this->mailer->sendWelcomeMessage($this);
            }
            \Yii::$app->session->setFlash('info', $this->getFlashMessage());

            return true;
        }



        return false;
    }
    /**
     * This method attempts user confirmation. It uses finder to find token with given code and if it is expired
     * or does not exist, this method will throw exception.
     *
     * If confirmation passes it will return true, otherwise it will return false.
     *
     * @param  string  $code Confirmation code.
     */
    public function attemptConfirmation($code)
    {
        /** @var Token $token */
        $token = $this->finder->findToken([
            'user_id' => $this->id,
            'code'    => $code,
            'type'    => Token::TYPE_CONFIRMATION,
        ])->one();

        if ($token === null || $token->isExpired) {
            \Yii::$app->session->setFlash('danger', \Yii::t('user', 'The confirmation link is invalid or expired. Please try requesting a new one.'));
        } else {
            $token->delete();

            $this->confirmed_at = time();

            $this->user_status = 'verify';

            \Yii::$app->user->login($this);

           // \Yii::getLogger()->log('User has been confirmed', Logger::LEVEL_INFO);

            if ($this->save(false)) {
                \Yii::$app->session->setFlash('success', \Yii::t('user', 'Thank you, registration is now complete.'));
            } else {
                \Yii::$app->session->setFlash('danger', \Yii::t('user', 'Something went wrong and your account has not been confirmed.'));
            }
        }
    }
}