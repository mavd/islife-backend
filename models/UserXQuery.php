<?php

namespace app\models;
use Yii;
/**
 * This is the ActiveQuery class for [[UserX]].
 *
 * @see UserX
 */
class UserXQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/
    public function player(){
        $this
            ->select('id,email,user_isAdmin,user_name,user_photo,user_gender,user_birthday,user_city,user_path,user_status,blocked_at,user_health,user_determination,user_communication,user_skill,user_amount,user_bonuses')
            ->asArray();
        return $this;
    }
    public function me(){
        $this
            ->where(['id'=>Yii::$app->user->id]);
        return $this;
    }
    /**
     * @inheritdoc
     * @return UserX[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserX|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}