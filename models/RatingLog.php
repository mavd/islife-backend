<?php

namespace app\models;

use Yii;
use \app\models\base\RatingLog as BaseRatingLog;

/**
 * This is the model class for table "rating_log".
 */
class RatingLog extends BaseRatingLog
{

    public function rules()
    {
        return [
            [['user_id', 'play_id', 'play_one_count', 'play_pair_count', 'rate_value', 'rate_sum'], 'integer'],
            [['rate_play_type'], 'string'],
            [['rate_created_at'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'Ид.',
            'user_id' => 'Игрок',
            'play_id' => 'Игра',
            'rate_play_type' => 'Тип игры',
            'play_one_count' => 'Сыграно одиночных',
            'play_pair_count' => 'Сыграно парных',
            'rate_value' => 'Рейтинг за игру',
            'rate_sum' => 'Накопленный рейтинг',
            'rate_created_at' => 'Дата записи',
        ];
    }
}
