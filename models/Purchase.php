<?php

namespace app\models;

use Yii;
use \app\models\base\Purchase as BasePurchase;

/**
 * This is the model class for table "purchase".
 */
class Purchase extends BasePurchase
{
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'Ид.',
            'user_id' => 'Игрок',
            'service_id' => 'Услуга',
            'purchase_amount' => 'Стоимость',
            'purchase_play_id' => 'Игра',
            'purchase_status' => 'Статус',
            'purchase_valid_to' => 'Время окончания действия',
            'purchase_valid_from' => 'Время начала действия',
            'purchase_created_at' => 'Время создания',
            'purchase_updated_at' => 'Время изменения',
        ];
    }
}
