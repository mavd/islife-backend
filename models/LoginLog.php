<?php

namespace app\models;

use Yii;
use \app\models\base\LoginLog as BaseLoginLog;

/**
 * This is the model class for table "login_log".
 */
class LoginLog extends BaseLoginLog
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'device_id'], 'integer'],
            [['login_created_at'], 'safe'],
            [['login_ip'], 'string', 'max' => 16]
        ];
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'device_id' => Yii::t('app', 'Device ID'),
            'login_ip' => Yii::t('app', 'Login Ip'),
            'login_created_at' => Yii::t('app', 'Login Created At'),
        ];
    }
}
