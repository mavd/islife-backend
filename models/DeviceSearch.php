<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Device;

/**
 * app\models\DeviceSearch represents the model behind the search form about `app\models\Device`.
 */
 class DeviceSearch extends Device
{

     public $userName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['device_access_token', 'device_access_token_expire', 'device_name', 'device_os', 'device_api_id', 'device_created_at', 'device_last_login_at'], 'safe'],
            [ [ 'userName' ], 'safe' ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Device::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = $dataProvider->getSort();

        $sort->attributes['userName'] =
            [
                'asc' => [ 'user_name' => SORT_ASC ],
                'desc' => [ 'user_name' => SORT_DESC ],
                'label' => 'Имя'
            ];

        $dataProvider->setSort([
            'attributes' => $sort->attributes]);


        if (!($this->load($params) && $this->validate())) {
            /**
             * Жадная загрузка данных
             * для работы сортировки.
             */
            $query->joinWith([ 'user' ]);
            return $dataProvider;
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'device_access_token_expire' => $this->device_access_token_expire,
            'device_created_at' => $this->device_created_at,
            'device_last_login_at' => $this->device_last_login_at,
        ]);

        $query->andFilterWhere(['like', 'device_access_token', $this->device_access_token])
            ->andFilterWhere(['like', 'device_name', $this->device_name])
            ->andFilterWhere(['like', 'device_os', $this->device_os])
            ->andFilterWhere(['like', 'device_api_id', $this->device_api_id]);

        if ( $this->userName) {
            $query->joinWith([ 'user' => function ($q) {
                $q->where('user_name LIKE "%' . $this->userName . '%"');
            } ]);
        }
        return $dataProvider;
    }
}
