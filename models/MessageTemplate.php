<?php

namespace app\models;

use Yii;
use \app\models\base\MessageTemplate as BaseMessageTemplate;

/**
 * This is the model class for table "message_template".
 */
class MessageTemplate extends BaseMessageTemplate
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mt_type', 'mt_template', 'mt_subject_template'], 'string'],
            [['mt_template' ], 'required'],
            [['mt_created_at', 'mt_updated_at'], 'safe'],
            [['mt_code'], 'string', 'max' => 50],
            
        ];
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'Ид.',
            'mt_code' => 'Код , для служебных целей',
            'mt_type' => 'Способ доставки',
            'mt_template' => 'Шаблон',
            'mt_subject_template' => 'Шаблон заголовка',
            'mt_created_at' => 'Время создания',
            'mt_updated_at' => 'Время изменения',
        ];
    }
}
