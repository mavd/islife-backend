<?php

namespace app\models;

use Yii;
use \app\models\base\Play as BasePlay;

/**
 * This is the model class for table "play".
 */
class Play extends BasePlay
{
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'Ид.',
            'task_id' => 'Задача',
            'user_id' => 'Игрок 1',
            'user2_id' => 'Игрок 2',
            'play_time_start' => 'Время начала задания',
            'play_time_end' => 'Время окончания ',
            'play_status' => 'Статус',
            'play_result' => 'Результат',
            'play_type' => 'Тип игры',
            'play_purchase_id'    => 'X2 для 1',
            'play_purchase2_id'   => 'X2 для 2',
            'play_estimate_sum' => 'Общее количество баллов',
            'play_estimate_count' => 'Количестов оценок',
            'play_bonuses' => 'Бонусы',
            'play_comment' => 'Комменты',
            'play_created_at' => 'Время создания задания',
            'play_updated_at' => 'Время последнего изменения',
            'taskName' => 'Название',
            'userName' => 'Имя',
            'user2Name  '  =>'Имя',
        ];
    }
}
