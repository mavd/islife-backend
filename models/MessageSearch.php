<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Message;

/**
 * app\models\MessageSearch represents the model behind the search form about `app\models\Message`.
 */
 class MessageSearch extends Message
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['message_type', 'message_mode', 'message_header', 'message_text', 'message_status', 'created_at', 'send_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'send_at' => $this->send_at,
        ]);

        $query->andFilterWhere(['like', 'message_type', $this->message_type])
            ->andFilterWhere(['like', 'message_mode', $this->message_mode])
            ->andFilterWhere(['like', 'message_header', $this->message_header])
            ->andFilterWhere(['like', 'message_text', $this->message_text])
            ->andFilterWhere(['like', 'message_status', $this->message_status]);

        return $dataProvider;
    }
}
