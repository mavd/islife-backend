<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Device]].
 *
 * @see Device
 */
class DeviceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    public function byUserAndApi($user_id,$api_id){
        $this->andWhere(['user_id'=>$user_id,'device_api_id'=>$api_id]);
        return $this;
    }
    /**
     * @inheritdoc
     * @return Device[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Device|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byToken($token){
        $this->andWhere(['device_access_token' => $token])
            // вечный токен ->andWhere(" device_access_token_expire >= '"  . date('Y-m-d H:i:s') . "'")
            ->select('user_id');
        return $this;
    }
}