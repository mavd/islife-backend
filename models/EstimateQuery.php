<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Estimate]].
 *
 * @see Estimate
 */
class EstimateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Estimate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Estimate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}