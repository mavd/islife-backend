<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $region_id
 * @property string $city
 * @property string $state
 * @property integer $biggest_city
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id', 'region_id', 'city'], 'required'],
            [['id', 'country_id', 'region_id', 'biggest_city'], 'integer'],
            [['city', 'state'], 'string', 'max' => 255],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
            'city' => 'City',
            'state' => 'State',
            'biggest_city' => 'Biggest City',
        ];
    }
}
