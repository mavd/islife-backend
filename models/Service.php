<?php

namespace app\models;

use Yii;
use \app\models\base\Service as BaseService;

/**
 * This is the model class for table "service".
 */
class Service extends BaseService
{
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'Ид.',
            'service_type' => 'Тип услуги',
            'service_play_type' => 'Время действия',
            'service_play_value' => 'Значение ',
            'service_code' => 'Внутренный код',
            'service_name' => 'Название',
            'service_amount' => 'Стоимость',
            'service_order' => 'Порядок показа',
            'service_status' => 'Статус',
            'service_created_at' => 'Время создания',
            'service_updated_at' => 'Время изменения',
        ];
    }
}
