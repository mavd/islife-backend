<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SkillLevel;

/**
 * app\models\SkillLevelSearch represents the model behind the search form about `app\models\SkillLevel`.
 */
 class SkillLevelSearch extends SkillLevel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sl_level', 'sl_low_edge', 'sl_high_edge', 'sl_note', 'sl_created_at', 'sl_updated_at'], 'integer'],
            [['sl_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SkillLevel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sl_level' => $this->sl_level,
            'sl_low_edge' => $this->sl_low_edge,
            'sl_high_edge' => $this->sl_high_edge,
            'sl_note' => $this->sl_note,
            'sl_created_at' => $this->sl_created_at,
            'sl_updated_at' => $this->sl_updated_at,
        ]);

        $query->andFilterWhere(['like', 'sl_name', $this->sl_name]);

        return $dataProvider;
    }
}
