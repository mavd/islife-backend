<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RatingLog]].
 *
 * @see RatingLog
 */
class RatingLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RatingLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RatingLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}