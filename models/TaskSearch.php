<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;

/**
 * app\models\TaskSearch represents the model behind the search form about `app\models\Task`.
 */
 class TaskSearch extends Task
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'task_author_id', 'task_health', 'task_determination', 'task_communication', 'task_skill'], 'integer'],
            [[ 'task_timer_hour','task_timer_minute'],'integer']      ,
            [['task_name', 'task_name2', 'task_description', 'task_type', 'task_status', 'task_comment_reject', 'task_created_at', 'task_updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'task_timer_hour' => $this->task_timer_hour,
            'task_timer_minute' => $this->task_timer_minute,
            'task_author_id' => $this->task_author_id,
            'task_health' => $this->task_health,
            'task_determination' => $this->task_determination,
            'task_communication' => $this->task_communication,
            'task_skill' => $this->task_skill,
            'task_created_at' => $this->task_created_at,
            'task_updated_at' => $this->task_updated_at,
        ]);

        $query->andFilterWhere(['like', 'task_name', $this->task_name])
            ->andFilterWhere(['like', 'task_name2', $this->task_name2])
            ->andFilterWhere(['like', 'task_description', $this->task_description])
            ->andFilterWhere(['like', 'task_type', $this->task_type])
            ->andFilterWhere(['like', 'task_status', $this->task_status])
            ->andFilterWhere(['like', 'task_comment_reject', $this->task_comment_reject]);

        return $dataProvider;
    }
}
