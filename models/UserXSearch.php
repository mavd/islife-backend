<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * app\models\UserXSearch represents the model behind the search form about `app\models\UserX`.
 */
 class UserXSearch extends UserX
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'flags', 'user_isAdmin', 'user_health', 'user_determination', 'user_communication', 'user_skill', 'user_amount', 'user_bonuses', 'user_last_device_id'], 'integer'],
            [['username', 'email', 'password_hash', 'auth_key', 'unconfirmed_email', 'registration_ip', 'user_name', 'user_photo', 'user_gender', 'user_birthday', 'user_city', 'user_path', 'user_status', 'user_blocked_comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserX::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        $query->andWhere(['user_isAdmin' => 0 ]);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'confirmed_at' => $this->confirmed_at,
            'blocked_at' => $this->blocked_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'flags' => $this->flags,
            'user_isAdmin' => $this->user_isAdmin,

            'user_health' => $this->user_health,
            'user_determination' => $this->user_determination,
            'user_communication' => $this->user_communication,
            'user_skill' => $this->user_skill,
            'user_amount' => $this->user_amount,
            'user_bonuses' => $this->user_bonuses,
            'user_last_device_id' => $this->user_last_device_id,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'unconfirmed_email', $this->unconfirmed_email])
            ->andFilterWhere(['like', 'registration_ip', $this->registration_ip])
            ->andFilterWhere(['like', 'user_name', $this->user_name])
            ->andFilterWhere(['like', 'user_photo', $this->user_photo])
            ->andFilterWhere(['like', 'user_gender', $this->user_gender])
            ->andFilterWhere(['like', 'user_city', $this->user_city])
            ->andFilterWhere(['like', 'user_path', $this->user_path])
            ->andFilterWhere(['like', 'user_status', $this->user_status])
            ->andFilterWhere(['like', 'user_birthday', $this->user_birthday])
            ->andFilterWhere(['like', 'user_blocked_comment', $this->user_blocked_comment]);

        return $dataProvider;
    }
}
