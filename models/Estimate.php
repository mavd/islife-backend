<?php

namespace app\models;

use Yii;
use \app\models\base\Estimate as BaseEstimate;

/**
 * This is the model class for table "estimate".
 */
class Estimate extends BaseEstimate
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['play_id', 'user_id', 'estimate_value'], 'integer'],
            [['estimate_status'], 'string'],
           // [['estimate_created_at', 'estimate_updated_at'], 'required'],
            [['estimate_created_at', 'estimate_updated_at'], 'safe'],
            [['estimate_comment'], 'string', 'max' => 255]
        ];
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => Yii::t('app', 'Ид.'),
            'play_id' => Yii::t('app', 'Оцениваемя акция'),
            'user_id' => Yii::t('app', 'Игрок - Оцениватель'),
            'estimate_value' => Yii::t('app', 'Оценка'),
            'estimate_status' => Yii::t('app', 'Статус'),
            'estimate_comment' => Yii::t('app', 'Комментарий'),
            'estimate_created_at' => Yii::t('app', 'Время создания '),
            'estimate_updated_at' => Yii::t('app', 'Время последнего изменения'),
        ];
    }
}
