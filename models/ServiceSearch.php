<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * ServiceSearch represents the model behind the search form about `app\models\Service`.
 */
class ServiceSearch extends Service
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'service_play_value', 'service_amount', 'service_order'], 'integer'],
            [['service_type', 'service_play_type', 'service_code', 'service_name', 'service_status', 'service_created_at', 'service_updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Service::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'service_play_value' => $this->service_play_value,
            'service_amount' => $this->service_amount,
            'service_order' => $this->service_order,
            'service_created_at' => $this->service_created_at,
            'service_updated_at' => $this->service_updated_at,
        ]);

        $query->andFilterWhere(['like', 'service_type', $this->service_type])
            ->andFilterWhere(['like', 'service_play_type', $this->service_play_type])
            ->andFilterWhere(['like', 'service_code', $this->service_code])
            ->andFilterWhere(['like', 'service_name', $this->service_name])
            ->andFilterWhere(['like', 'service_status', $this->service_status]);

        return $dataProvider;
    }
}
