<?php

namespace app\models;
use Yii;
/**
 * This is the ActiveQuery class for [[Purchase]].
 *
 * @see Purchase
 */
class PurchaseQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * Условие для поиска валидной (действующей) покупки X2
     * @return $this
     */
    public function validPurshase(){
        $this->andWhere(['in','status',['active']]);
        $this->andWhere(['user_id'=> Yii::$app->user->id]);
        $this->andWhere(['>=','purchase_valid_from', date('Y-m-d H:i:s')]);
        $this->andWhere(['<=','purchase_valid_to', date('Y-m-d H:i:s')]);
        return $this;
    }
    /**
     * @inheritdoc
     * @return Purchase[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Purchase|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}