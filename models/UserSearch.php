<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use dektrium\user\models\UserSearch as BaseUserSearch;

/**
 * UserXSearch represents the model behind the search form about `app\models\UserX`.
 */
class UserSearch extends BaseUserSearch
{

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->finder->getUserQuery();
        $query->andWhere(['user_isAdmin' => 1 ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->created_at !== null) {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['registration_ip' => $this->registration_ip]);

        return $dataProvider;
    }
}
