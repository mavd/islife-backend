<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Estimate;

/**
 * app\models\EstimateSearch represents the model behind the search form about `app\models\Estimate`.
 */
 class EstimateSearch extends Estimate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'play_id', 'user_id', 'estimate_value'], 'integer'],
            [['estimate_status', 'estimate_comment', 'estimate_created_at', 'estimate_updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Estimate::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'play_id' => $this->play_id,
            'user_id' => $this->user_id,
            'estimate_value' => $this->estimate_value,
            'estimate_created_at' => $this->estimate_created_at,
            'estimate_updated_at' => $this->estimate_updated_at,
        ]);

        $query->andFilterWhere(['like', 'estimate_status', $this->estimate_status])
            ->andFilterWhere(['like', 'estimate_comment', $this->estimate_comment]);

        return $dataProvider;
    }
}
