<?php

namespace app\models;

use Yii;
use \app\models\base\Message as BaseMessage;

/**
 * This is the model class for table "message".
 */
class Message extends BaseMessage
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['id', 'user_id'], 'integer'],
            [['message_type', 'message_mode', 'message_status'], 'string'],
            [['created_at', 'send_at'], 'safe'],
            [['message_header'], 'string', 'max' => 2048],
            [['message_text'], 'string', 'max' => 1024*2048]
        ];
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => Yii::t('app', 'Ид.'),
            'user_id' => Yii::t('app', 'Получатель'),
            'message_type' => Yii::t('app', 'Способ доставки'),
            'message_mode' => Yii::t('app', 'Тип'),
            'message_header' => Yii::t('app', 'Заголовок'),
            'message_text' => Yii::t('app', 'Текст'),
            'message_status' => Yii::t('app', 'Статус'),
            'send_at' => Yii::t('app', 'Время отправки'),
        ];
    }
}
