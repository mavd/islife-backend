<?php

namespace app\models;

use Yii;
use \app\models\base\Task as BaseTask;

/**
 * This is the model class for table "task".
 */
class Task extends BaseTask
{
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'Ид.',
            'task_name' => 'Название',
            'task_name2' => 'Название 2',
            'task_description' => 'Описание',

            'task_type' => 'Тип',
            'task_timer_hour' => 'Время на выполнение (час)',
            'task_timer_minute' => 'Время на выполнение (минут)',
            'task_status' => 'статус',
            'task_comment_reject' => 'Коммент',
            'task_author_id' => 'Автор',
            'task_health' => 'Здоровье',
            'task_determination' => 'Смелость',
            'task_communication' => 'Общительность',
            'task_skill' => 'Опыт',
            'task_created_at' => 'Время создания',
            'task_updated_at' => 'Время последнего редактирования',
        ];
    }
}
