<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base model class for table "estimate".
 *
 * @property integer $id
 * @property integer $play_id
 * @property integer $user_id
 * @property integer $estimate_value
 * @property string $estimate_status
 * @property string $estimate_comment
 * @property string $estimate_created_at
 * @property string $estimate_updated_at
 *
 * @property \app\models\Play $play
 * @property \app\models\User $user
 */
class Estimate extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estimate';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Ид.'),
            'play_id' => Yii::t('app', 'Оцениваемое задание'),
            'user_id' => Yii::t('app', 'Игрок - Оцениватель'),
            'estimate_value' => Yii::t('app', 'Оценка'),
            'estimate_status' => Yii::t('app', 'Статус'),
            'estimate_comment' => Yii::t('app', 'Комментарий'),
            'estimate_created_at' => Yii::t('app', 'Время создания '),
            'estimate_updated_at' => Yii::t('app', 'Время последнего изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlay()
    {
        return $this->hasOne(\app\models\Play::className(), ['id' => 'play_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }

/**
     * @inheritdoc
     * @return type array
     */ 
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'estimate_created_at',
                'updatedAtAttribute' => 'estimate_updated_at',
                'value' => new \yii\db\Expression( date("'Y-m-d H:i:s'")) ,
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\EstimateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\EstimateQuery(get_called_class());
    }
}
