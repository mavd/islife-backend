<?php
namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use dektrium\user\Mailer;
use dektrium\user\models\Token;
/**
 * This is the base model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * //property string $password_hash
 * //property string $auth_key
 * @property integer $confirmed_at
 * @property string $unconfirmed_email
 * @property integer $blocked_at
 * //property string $registration_ip
 * //property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 * @property integer $user_isAdmin
 * @property string $user_name
 * @property string $user_photo
 * @property string $user_gender
 * @property string $user_birthday
 * @property string $user_city
 * @property string $user_path
 * @property string $user_status
 * @property string $user_blocked_comment
 * @property integer $user_health
 * @property integer $user_determination
 * @property integer $user_communication
 * @property integer $user_skill
 * @property integer $user_amount
 * @property integer $user_bonuses
 * @property integer $user_last_device_id
 * @property string $user_lat
 * @property string $user_long
 *
 * @property \app\models\Play[] $plays
 * @property \app\models\Device[] $devices
 * @property \app\models\Estimate[] $estimates
 * @property \app\models\Message[] $messages
 * @property \app\models\Purchase[] $purchases
 * @property \app\models\Report[] $reports
 * @property \app\models\RatingLogs[] $ratingLogs
 */
class UserX extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;
    /** @var \dektrium\user\Mailer */
    protected $mailer;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }


    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [[
                'confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'flags',
                'user_isAdmin', 'user_health', 'user_determination', 'user_communication',
                'user_skill', 'user_last_device_id',
                'user_amount','user_bonuses'], 'integer'],
            [['user_gender', 'user_status'], 'string'],
            [['user_birthday'], 'safe'],
            [['username'], 'string', 'max' => 25],
            [['user_name'], 'string', 'max' => 25],
            [['email', 'unconfirmed_email'], 'string', 'max' => 255],
            //[['password_hash'], 'string', 'max' => 60],
            //[['auth_key'], 'string', 'max' => 32],
            //[['registration_ip'], 'string', 'max' => 45],
            [['user_photo', 'user_city', 'user_path', 'user_blocked_comment'], 'string', 'max' => 256],

            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Ид.',
            'username' => 'Логин',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'confirmed_at' => 'Confirmed At',
            'unconfirmed_email' => 'Unconfirmed Email',
            'blocked_at' => 'Blocked At',
            'registration_ip' => 'Registration Ip',
            'flags' => 'Flags',
            'user_isAdmin' => 'Является администратором',
            'user_name' => 'Имя',
            'user_photo' => 'Фото',
            'user_gender' => 'Пол',
            'user_birthday' => 'Дата рождения',
            'user_city' => 'Город',
            'user_path' => 'Путь',
            'user_status' => 'Статус',
            'user_blocked_comment' => 'Комментарий к блокировке',
            'user_health' => 'Здоровье',
            'user_determination' => 'Смелость',
            'user_communication' => 'Общительность',
            'user_skill' => 'Опыт',
            'user_amount' => 'Остаток на счету',
            'user_bonuses' => 'Бонусы',
            'user_last_device_id' => 'Последнее устройство, с которого заходил пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlays()
    {
        return $this->hasMany(\app\models\Play::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(\app\models\Device::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstimates()
    {
        return $this->hasMany(\app\models\Estimate::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(\app\models\Message::className(), ['user_id' => 'id']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchases()
    {
        return $this->hasMany(\app\models\Purchase::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReports()
    {
        return $this->hasMany(\app\models\Report::className(), ['user_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingLogs()
    {
        return $this->hasMany(\app\models\RatingLog::className(), ['user_id' => 'id']);
    }
    /**
     * Sends an email to a user with confirmation link.
     *
     * @param  User  $user
     * @param  Token $token
     * @return bool
     */
    public function sendConfirmationMessage(User $user, Token $token)
    {
        return $this->sendMessage($user->email,
            $this->confirmationSubject,
            'confirmation',
            ['user' => $user, 'token' => $token]
        );
    }
    /**
     * @param  string $to
     * @param  string $subject
     * @param  string $view
     * @param  array  $params
     * @return bool
     */
    protected function sendMessage($to, $subject, $view, $params = [])
    {
        $mailer = \Yii::$app->mailer;
        $mailer->viewPath = $this->viewPath;
        $mailer->getView()->theme = \Yii::$app->view->theme;

        if ($this->sender === null) {
            $this->sender = isset(\Yii::$app->params['adminEmail']) ? \Yii::$app->params['adminEmail'] : 'no-reply@example.com';
        }

        return $mailer->compose(['html' => $view, 'text' => 'text/' . $view], $params)
            ->setTo($to)
            ->setFrom($this->sender)
            ->setSubject($subject)
            ->send();
    }
    /**
     * This method is used to register new user account. If Module::enableConfirmation is set true, this method
     * will generate new confirmation token and use mailer to send it to the user. Otherwise it will log the user in.
     * If Module::enableGeneratingPassword is set true, this method will generate new 8-char password. After saving user
     * to database, this method uses mailer component to send credentials (username and password) to user via email.
     *
     * @return bool
     */
    public function register()
    {
        $this->confirmed_at = time();
        if ($this->save()) {
            $token = \Yii::createObject([
               'class' => Token::className(),
               'type'  => Token::TYPE_CONFIRMATION,
            ]);
            $token->link('user', $this);
            $this->mailer = \Yii::$container->get(Mailer::className());
            $this->mailer->sendMessage($this->email,
                'confirmation',
                'confirmation',
                ['user' => $this, 'token' => $token]
            );
            return true;
        }

        return false;
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' =>new \yii\db\Expression( time())
            ],


        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\UserXQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\UserXQuery(get_called_class());
    }
}
