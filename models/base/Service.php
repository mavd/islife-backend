<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "service".
 *
 * @property integer $id
 * @property string $service_type
 * @property string $service_play_type
 * @property integer $service_play_value
 * @property string $service_code
 * @property string $service_name
 * @property integer $service_amount
 * @property integer $service_order
 * @property string $service_status
 * @property string $service_created_at
 * @property string $service_updated_at
 *
 * @property \app\models\Purchase[] $purchases
 */
class Service extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_type', 'service_play_type', 'service_status'], 'string'],
            [['service_play_value', 'service_amount', 'service_order'], 'integer'],
            [['service_name'], 'required'],
            [['service_created_at', 'service_updated_at'], 'safe'],
            [['service_code', 'service_name'], 'string', 'max' => 50],
            [['service_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Ид.',
            'service_type' => 'Тип услуги',
            'service_play_type' => 'Время действия',
            'service_play_value' => 'Значение ',
            'service_code' => 'Код',
            'service_name' => 'Название',
            'service_amount' => 'Стоимость',
            'service_order' => 'Порядок показа',
            'service_status' => 'Статус',
            'service_created_at' => 'Время создания',
            'service_updated_at' => 'Время изменения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchases()
    {
        return $this->hasMany(\app\models\Purchase::className(), ['service_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'service_created_at',
                'updatedAtAttribute' => 'service_updated_at',
                'value' =>new \yii\db\Expression( date("'Y-m-d H:i:s'"))
            ],


        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\ServiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ServiceQuery(get_called_class());
    }
}
