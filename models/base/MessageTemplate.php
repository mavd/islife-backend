<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base model class for table "message_template".
 *
 * @property integer $id
 * @property string $mt_code
 * @property string $mt_type
 * @property string $mt_template
 * @property string $mt_subject_template
 * @property string $mt_created_at
 * @property string $mt_updated_at
 */
class MessageTemplate extends \yii\db\ActiveRecord {

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'message_template';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'                  => 'Ид.',
            'mt_code'             => 'Код , для служебных целей',
            'mt_type'             => 'Способ доставки',
            'mt_template'         => 'Шаблон',
            'mt_subject_template' => 'Шаблон заголовка',
            'mt_created_at'       => 'Время создания',
            'mt_updated_at'       => 'Время изменения',
        ];
    }

    /**
     * @inheritdoc
     * @return type array
     */
    public function behaviors() {
        return [
            [
                'class'              => TimestampBehavior::className(),

                'createdAtAttribute' => 'mt_created_at',
                'updatedAtAttribute' => 'mt_updated_at',
                'value'              => new \yii\db\Expression(date("'Y-m-d H:i:s'"))
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\MessageTemplateQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\MessageTemplateQuery(get_called_class());
    }
}
