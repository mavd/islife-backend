<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base model class for table "skill_level".
 *
 * @property integer $id           'Ид.',
 * @property string $sl_name        'Название уровня',
 * @property integer $sl_level       'Уровень',
 * @property integer $sl_low_edge    'Нижняя граница',
 * @property integer $sl_high_edge     'Верхняя граница',
 * @property integer $sl_note          'Примечание',
 * @property integer $sl_created_at    'Дата создания',
 * @property integer $sl_updated_at      'Дата последнего изменения',
 */
class SkillLevel extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'skill_level';
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Ид.',
            'sl_name' => 'Название уровня',
            'sl_level' => 'Уровень',
            'sl_low_edge' => 'Нижняя граница',
            'sl_high_edge' => 'Верхняя граница',
            'sl_note' => 'Примечание',
            'sl_created_at' => 'Дата создания',
            'sl_updated_at' => 'Дата последнего изменения',
        ];
    }

/**
     * @inheritdoc
     * @return type array
     */ 
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'sl_created_at',
                'updatedAtAttribute' => 'sl_updated_at',
                'value' =>new \yii\db\Expression( date("'Y-m-d H:i:s'"))
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\SkillLevelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SkillLevelQuery(get_called_class());
    }
}
