<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base model class for table "rating_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $play_id
 * @property string $rate_play_type
 * @property integer $play_one_count
 * @property integer $play_pair_count
 * @property integer $rate_value
 * @property integer $rate_sum
 * @property string $rate_created_at
 *
 * @property \app\models\Play $play
 * @property \app\models\User $user
 */
class RatingLog extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rating_log';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'play_id', 'rate_value', 'rate_sum'], 'integer'],
            [['rate_created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Ид.',
            'user_id' => 'Игрок',
            'play_id' => 'Игра',
            'rate_play_type' => 'Тип игры',
            'play_one_count' => 'Сыграно одиночных',
            'play_pair_count' => 'Сыграно парных',
            'rate_value' => 'Рейтитг за игру',
            'rate_sum' => 'Накопленный рейтинг',
            'rate_created_at' => 'Дата записи',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlay()
    {
        return $this->hasOne(\app\models\Play::className(), ['id' => 'play_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'rate_created_at',
                'updatedAtAttribute' => 'rate_created_at',
                'value' =>new \yii\db\Expression( date("'Y-m-d H:i:s'"))
            ],
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\RatingLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\RatingLogQuery(get_called_class());
    }
}
