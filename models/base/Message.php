<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base model class for table "message".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $message_type
 * @property string $message_mode
 * @property string $message_header
 * @property string $message_text
 * @property string $message_status
 * @property string $created_at
 * @property string $send_at
 *
 * @property \app\models\User $user
 */
class Message extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Ид.'),
            'user_id' => Yii::t('app', 'Получатель'),
            'message_type' => Yii::t('app', 'Способ доставки'),
            'message_mode' => Yii::t('app', 'Тип'),
            'message_header' => Yii::t('app', 'Заголовок'),
            'message_text' => Yii::t('app', 'Текст'),
            'message_status' => Yii::t('app', 'Статус'),
            'send_at' => Yii::t('app', 'Время отправки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }

/**
     * @inheritdoc
     * @return type array
     */ 
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'send_at',
                'value' =>new \yii\db\Expression( date("'Y-m-d H:i:s'"))
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\MessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MessageQuery(get_called_class());
    }
}
