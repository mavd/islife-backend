<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base model class for table "report".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $play_id
 * @property string $report_photo
 * @property string $report_thumb
 * @property string $report_comment
 * @property string $report_created_at
 *
 * @property \app\models\Play $play
 * @property \app\models\User $user
 */
class Report extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Ид.'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'play_id' => Yii::t('app', 'Акция'),
            'report_photo' => Yii::t('app', 'Ссылка на фото'),
            'report_thumb' => Yii::t('app', 'Ссылка на маленькое фото'),
            'report_comment' => Yii::t('app', 'Коммент'),
            'report_created_at' => Yii::t('app', 'Время создания'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlay()
    {
        return $this->hasOne(\app\models\Play::className(), ['id' => 'play_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }

/**
     * @inheritdoc
     * @return type array
     */ 
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'report_created_at',
                'updatedAtAttribute' =>'report_created_at',
                'value' => new \yii\db\Expression( date("'Y-m-d H:i:s'"))
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\ReportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ReportQuery(get_called_class());
    }
}
