<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "purchase".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $service_id
 * @property integer service_param
 * @property integer $purchase_amount
 * @property integer $purchase_play_id
 * @property string $purchase_status
 * @property string $purchase_valid_to
 * @property string $purchase_valid_from
 * @property string $purchase_created_at
 * @property string $purchase_updated_at
 *
 * @property \app\models\User $user
 * @property \app\models\Service $service
 */
class Purchase extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase';
    }

    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [ [ 'user_id', 'service_id', 'service_param' ,'purchase_amount', 'purchase_play_id' ], 'integer' ],
            [ [ 'purchase_status' ], 'string' ],

            [ [ 'purchase_valid_to', 'purchase_valid_from', 'purchase_created_at', 'purchase_updated_at' ], 'safe' ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Ид.',
            'user_id' => 'Игрок',
            'service_id' => 'Услуга',
            'purchase_amount' => 'Стоимость',
            'purchase_play_id' => 'Игра',
            'purchase_status' => 'Статус',
            'purchase_valid_to' => 'Время окончания действия',
            'purchase_valid_from' => 'Время начала действия',
            'purchase_created_at' => 'Дата покупки',
            'purchase_updated_at' => 'Время изменения',
            'serviceName' => 'Название',
            'userName' => 'Имя',
        ];
    }


    public function getUserName()
    {
        return $this->user->user_name;
    }

    public function getServiceName()
    {
        return $this->service->service_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), [ 'id' => 'user_id' ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(\app\models\Service::className(), [ 'id' => 'service_id' ]);
    }

    public function behaviors()
    {
        $d=[
            'class' => TimestampBehavior::className(),
            'createdAtAttribute' => 'purchase_updated_at',
            'updatedAtAttribute' => 'purchase_updated_at',
            'value' => new \yii\db\Expression(date("'Y-m-d H:i:s'"))
        ];

        return [$d];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\PurchaseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PurchaseQuery(get_called_class());
    }
}
