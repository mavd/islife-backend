<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base model class for table "login_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $device_id
 * @property string $login_ip
 * @property string $login_created_at
 */
class LoginLog extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'login_log';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'device_id' => Yii::t('app', 'Device ID'),
            'login_ip' => Yii::t('app', 'Login Ip'),
            'login_created_at' => Yii::t('app', 'Login Created At'),
            'userName'=>'Имя',
            'deviceName'=>'Название',
        ];
    }

/**
     * @inheritdoc
     * @return type array
     */ 
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'login_created_at',
                'updatedAtAttribute' => 'login_created_at',
                'value' => new \yii\db\Expression( date("'Y-m-d H:i:s'") )
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(\app\models\Device::className(), ['id' => 'device_id']);
    }
    public function getUserName(){
        return $this->user->user_name;
    }
    public function getDeviceName(){
        return $this->device->device_name;
    }

    /**
     * @inheritdoc
     * @return \app\models\LoginLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\LoginLogQuery(get_called_class());
    }
}
