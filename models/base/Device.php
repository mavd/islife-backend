<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "device".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $device_access_token
 * @property string $device_access_token_expire
 * @property string $device_name
 * @property string $device_os
 * @property string $device_api_id
 * @property string $device_created_at
 * @property string $device_last_login_at
 *
 * @property \app\models\User $user
 * @property \app\models\LoginLog[] $loginLogs
 */
class Device extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'device';
    }

    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'device_access_token', 'device_access_token_expire', 'device_name', 'device_os', 'device_api_id', 'device_created_at'], 'required'],
            [['user_id'], 'integer'],
            [['device_access_token_expire', 'device_created_at', 'device_last_login_at'], 'safe'],
            [['device_access_token'], 'string', 'max' => 50],
            [['device_name', 'device_os', 'device_api_id'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Ид.',
            'user_id' => 'Ид Пользователя',
            'device_access_token' => 'Ид Сеанса',
            'device_access_token_expire' => 'Время окончания валидности сеанса',
            'device_name' => 'Марка и модель',
            'device_os' => 'ОС',
            'device_api_id' => 'Ид устройства от производителя',
            'device_created_at' => 'Время регистрации',
            'device_last_login_at' => 'Время последнего логина',
            'userName'=>'Имя'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }

    public function getUserName(){
        return $this->user->user_name;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoginLogs()
    {
        return $this->hasMany(\app\models\LoginLog::className(), ['device_id' => 'id']);
    }



    /**
     * @inheritdoc
     * @return \app\models\DeviceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\DeviceQuery(get_called_class());
    }
}
