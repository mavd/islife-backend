<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base model class for table "play".
 *
 * @property integer $id
 * @property integer $task_id
 * @property integer $user_id
 * @property integer $user2_id
 * @property string $play_time_start
 * @property string $play_time_end
 * @property string $play_status
 * @property string $play_result
 * @property string $play_type
 * @property integer $play_purchase_id
 * @property integer $play_purchase2_id
 * @property integer $play_estimate_sum
 * @property integer $play_estimate_count
 * @property integer $play_estimate
 * @property string $play_bonuses
 * @property string $play_comment
 * @property string $play_created_at
 * @property string $play_updated_at
 *
 * @property \app\models\Task $task
 * @property \app\models\User $user
 * @property \app\models\Estimate[] $estimates
 * @property \app\models\Report[] $reports
 * @property \app\models\RatingLogsК[] $ratingLogs
 */
class Play extends \yii\db\ActiveRecord {

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'play';
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['task_id', 'user_id', 'play_estimate_sum', 'play_purchase_id', 'play_estimate_count'], 'integer'],
            [['play_time_start', 'play_time_end', 'play_created_at', 'play_updated_at','play_estimate'], 'safe'],
            [['play_status', 'play_result', 'play_type'], 'string'],
            [['play_bonuses'], 'string', 'max' => 50],
            [['play_comment'], 'string', 'max' => 255],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'                  => 'Ид.',
            'task_id'             => 'Задача',
            'user_id'             => 'Игрок 1',
            'user2_id'            => 'Игрок 2 ',
            'play_time_start'     => 'Время начала задания',
            'play_time_end'       => 'Время окончания',
            'play_result'         => 'Результат',
            'play_status'         => 'Статус',
            'play_type'           => 'Тип игры',
            'play_purchase_id'    => 'X2 для 1',
            'play_purchase2_id'   => 'X2 для 2',
            'play_estimate_sum'   => 'Общее количество баллов',
            'play_estimate'       => 'Оценка',
            'play_estimate_count' => 'Количество оценок',
            'play_bonuses'        => 'Бонусы',
            'play_comment'        => 'Комменты',
            'play_created_at'     => 'Время создания задания',
            'play_updated_at'     => 'Время последнего изменения',

            'taskName' => 'Название',
            'userName' => 'Имя',
            'user2Name  '  =>'Имя',
            'playEstimate'=>'Оценка'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask() {
        return $this->hasOne(\app\models\Task::className(), ['id' => 'task_id']);
    }


    public function getTaskName() {
        return $this->task->task_name;
    }

    public function getUserName() {
        return $this->user->user_name;
    }
    public function getUser2Name() {
        return $this->user2->user_name;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(\app\models\UserX::className(), ['id' => 'user_id'])
            ->select('id,email, user_name,user_photo,user_gender,user_birthday,user_city,user_path,user_status,blocked_at,user_health,user_determination,user_communication,user_skill,user_amount,user_bonuses')
            ->from(['u'=>\app\models\UserX::tableName()])
        ;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser2() {
        return $this->hasOne(\app\models\UserX::className(), ['id' => 'user2_id'])
            ->select('id,email, user_name,user_photo,user_gender,user_birthday,user_city,user_path,user_status,blocked_at,user_health,user_determination,user_communication,user_skill,user_amount,user_bonuses')
            ->from(['u2'=>\app\models\UserX::tableName()])
            ;

    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstimates() {
        return $this->hasMany(\app\models\Estimate::className(), ['play_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReports() {
        return $this->hasMany(\app\models\Report::className(), ['play_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingLogs() {
        return $this->hasMany(\app\models\RatingLog::className(), ['play_id' => 'id']);
    }
    public function behaviors() {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'play_created_at',
                'updatedAtAttribute' => 'play_updated_at',
                'value'              => new \yii\db\Expression(date("'Y-m-d H:i:s'"))
            ],

        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PlayQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\PlayQuery(get_called_class());
    }
}
