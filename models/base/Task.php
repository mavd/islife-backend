<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "task".
 *
 * @property integer $id
 * @property string $task_name
 * @property string $task_name2
 * @property string $task_description
 * @property string $task_type
 * @property integer $task_timer_hour
 * @property integer $task_timer_minute
 * @property string $task_status
 * @property string $task_comment_reject
 * @property integer $task_author_id
 * @property integer $task_health
 * @property integer $task_determination
 * @property integer $task_communication
 * @property integer $task_skill
 * @property string $task_created_at
 * @property string $task_updated_at
 *
 * @property \app\models\Play[] $plays
 */
class Task extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [ [ 'task_type', 'task_status' ], 'string' ],
            [ [ 'task_timer_hour', 'task_timer_minute','task_author_id', 'task_health', 'task_determination', 'task_communication', 'task_skill' ], 'integer' ],
            [ [ 'task_timer_hour'  ], 'integer', 'max'=>24, 'min'=>0 ],
            [ [ 'task_timer_minute'  ], 'integer', 'max'=>60, 'min'=>0 ],
            [ [ 'task_created_at', 'task_updated_at' ], 'safe' ],
            [['task_name', 'task_name2'], 'string', 'max' => 50],
            [ [ 'task_description' ], 'string', 'max' => 8096 ],
            [ [ 'task_comment_reject' ], 'string', 'max' => 256 ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Ид.',
            'task_name' => 'Название',
            'task_description' => 'Описание',
            'task_name2' => 'Название 2',
            'task_type' => 'Тип',
            'task_timer_hour' => 'Время на выполнение (час)',
            'task_timer_minute' => 'Время на выполнение (минут)',
            'task_status' => 'статус',
            'task_comment_reject' => 'Коммент',
            'task_author_id' => 'Автор',
            'task_health' => 'Здоровье',
            'task_determination' => 'Смелость',
            'task_communication' => 'Общительность',
            'task_skill' => 'Опыт',
            'task_created_at' => 'Время создания',
            'task_updated_at' => 'Время последнего редактирования',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlays()
    {
        return $this->hasMany(\app\models\Play::className(), [ 'task_id' => 'id' ]);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'task_created_at',
                'updatedAtAttribute' => 'task_updated_at',
                'value' => new \yii\db\Expression(date("'Y-m-d H:i:s'"))
            ],


        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\TaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TaskQuery(get_called_class());
    }
}
