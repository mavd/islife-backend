<?php

namespace app\models;

use Yii;
use \app\models\base\Report as BaseReport;

/**
 * This is the model class for table "report".
 */
class Report extends BaseReport
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'play_id'], 'required'],
            [['user_id', 'play_id'], 'integer'],
            [['report_created_at'], 'safe'],
            [['report_photo', 'report_thumb', 'report_comment'], 'string', 'max' => 255]
        ];
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => Yii::t('app', 'Ид.'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'play_id' => Yii::t('app', 'Акция'),
            'report_photo' => Yii::t('app', 'Ссылка на фото'),
            'report_thumb' => Yii::t('app', 'Ссылка на маленькое фото'),
            'report_comment' => Yii::t('app', 'Коммент'),
            'report_created_at' => Yii::t('app', 'Время создания'),
        ];
    }
}
