<?php

namespace app\models;

use Yii;
use \app\models\base\SkillLevel as BaseSkillLevel;

/**
 * This is the model class for table "skill_level".
 */
class SkillLevel extends BaseSkillLevel
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sl_level', 'sl_low_edge', 'sl_high_edge',  'sl_created_at', 'sl_updated_at'], 'integer'],
            [['sl_name'], 'string', 'max' => 50],

        ];
    }
	
    /**
     * @inheritdoc
     *
     *
     */
    public function attributeHints()
    {
        return [
            'id' => 'Ид.',
            'sl_name' => 'Название уровня',
            'sl_level' => 'Уровень',
            'sl_low_edge' => 'Нижняя граница',
            'sl_high_edge' => 'Верхняя граница',
            'sl_note' => 'Примечание',
            'sl_created_at' => 'Дата создания',
            'sl_updated_at' => 'Дата последнего изменения',
        ];
    }
}
