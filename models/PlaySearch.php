<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Play;

/**
 * app\models\PlaySearch represents the model behind the search form about `app\models\Play`.
 */
class PlaySearch extends Play
{

    public $taskName;
    public $userName;
    public $user2Name;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [ [ 'id', 'task_id', 'user_id', 'user2_id', 'play_purchase_id', 'play_purchase2_id', 'play_estimate_sum', 'play_estimate_count','play_estimate' ], 'integer' ],
            [ [ 'play_time_start', 'play_time_end', 'play_status', 'play_result', 'play_type', 'play_bonuses', 'play_comment', 'play_created_at', 'play_updated_at' ], 'safe' ],
            [ [ 'taskName', 'userName', 'user2Name', ], 'safe' ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Play::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes['taskName'] =
            [
            'asc' => [ 'task.task_name' => SORT_ASC ],
            'desc' => [ 'task.task_name' => SORT_DESC ],
            'label' => '�������'
            ];
        $sort->attributes['userName'] =
            [
                'asc' => [ 'u.user_name' => SORT_ASC ],
                'desc' => [ 'u.user_name' => SORT_DESC ],
                'label' => '���'
            ];
        $sort->attributes['user2Name'] =
            [
                'asc' => [ 'u2.user_name' => SORT_ASC ],
                'desc' => [ 'u2.user_name' => SORT_DESC ],
                'label' => '���'
            ];

        $dataProvider->setSort([
            'attributes' => $sort->attributes]);


        if (!($this->load($params) && $this->validate())) {
            /**
             * ������ �������� ������ ������ ������
             * ��� ������ ����������.
             */
            $query->joinWith([ 'task' ]);

            $query->joinWith([ 'user' ]);

            $query->joinWith([ 'user2' ]);
            return $dataProvider;
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'task_id' => $this->task_id,
            'user_id' => $this->user_id,
            'user2_id' => $this->user2_id,
            'play_time_start' => $this->play_time_start,
            'play_time_end' => $this->play_time_end,
            'play_purchase_id' => $this->play_purchase_id,
            'play_purchase2_id' => $this->play_purchase2_id,
            'play_estimate_sum' => $this->play_estimate_sum,
            'play_estimate_count' => $this->play_estimate_count,
            //'play_estmate' => $this->play_estimate,
            'play_created_at' => $this->play_created_at,
            'play_updated_at' => $this->play_updated_at,
        ]);

        $query->andFilterWhere([ 'like', 'play_status', $this->play_status ])
              ->andFilterWhere([ 'like', 'play_result', $this->play_result ])
              ->andFilterWhere([ 'like', 'play_type', $this->play_type ])
              ->andFilterWhere([ 'like', 'play_bonuses', $this->play_bonuses ])
              ->andFilterWhere([ 'like', 'play_comment', $this->play_comment ]);

        // ������
        if ($this->taskName)
        $query->joinWith([ 'task' => function ($q) {
            $q->where('task.task_name LIKE "%' . $this->taskName . '%"');
        } ]);
        if ($this->userName)
        $query->joinWith([ 'user' => function ($q) {
            $q->where('u.user_name LIKE "%' . $this->userName . '%"');
        } ]);
        if ($this->user2Name)
        $query->joinWith([ 'user2' => function ($q) {
            $q->where('u2.user_name LIKE "%' . $this->user2Nam . '%"');
        } ]);
        if ($this->play_estimate){
            $query->andFilterWhere(['>=', 'play_estimate', $this->play_estimate-0.5])
            ->andFilterWhere(['<=', 'play_estimate', $this->play_estimate+0.5])
            ;
        }
        return $dataProvider;
    }
}
