<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * LoginLogSearch represents the model behind the search form about `app\models\LoginLog`.
 */
class LoginLogSearch extends LoginLog
{
    public $userName;
    public $seviceName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [ [ 'id', 'user_id', 'device_id' ], 'integer' ],
            [ [ 'login_ip', 'login_created_at' ], 'safe' ],
            [ [ 'deviceName', 'userName' ], 'safe' ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoginLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes['deviceName']
              = [
            'asc' => [ 'device.device_name' => SORT_ASC ],
            'desc' => [ 'device.device_name' => SORT_DESC ],
            'label' => 'Название'
        ];
        $sort->attributes['userName']
              = [
            'asc' => [ 'user_name' => SORT_ASC ],
            'desc' => [ 'user_name' => SORT_DESC ],
            'label' => 'Имя'
        ];

        $dataProvider->setSort([
            'attributes' => $sort->attributes ]);


        if (!($this->load($params) && $this->validate())) {
            /**
             * Жадная загрузка данных модели Страны
             * для работы сортировки.
             */
            $query->joinWith([ 'device' ]);

            $query->joinWith([ 'user' ]);

            return $dataProvider;
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'device_id' => $this->device_id,
            'login_created_at' => $this->login_created_at,
        ]);

        $query->andFilterWhere([ 'like', 'login_ip', $this->login_ip ]);
        // Фильтр
        if ($this->deviceName) {
            $query->joinWith([ 'device' => function ($q) {
                $q->where('device.device_name LIKE "%' . $this->deviceName . '%"');
            } ]);
        }
        if ($this->userName) {
            $query->joinWith([ 'user' => function ($q) {
                $q->where('u.user_name LIKE "%' . $this->userName . '%"');
            } ]);
        }
        return $dataProvider;
    }
}
