<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MessageTemplate;

/**
 * app\models\MessageTemplateSearch represents the model behind the search form about `app\models\MessageTemplate`.
 */
 class MessageTemplateSearch extends MessageTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['mt_code', 'mt_type', 'mt_template', 'mt_subject_template', 'mt_created_at', 'mt_updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MessageTemplate::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'mt_created_at' => $this->mt_created_at,
            'mt_updated_at' => $this->mt_updated_at,
        ]);

        $query->andFilterWhere(['like', 'mt_code', $this->mt_code])
            ->andFilterWhere(['like', 'mt_type', $this->mt_type])
            ->andFilterWhere(['like', 'mt_template', $this->mt_template])
            ->andFilterWhere(['like', 'mt_subject_template', $this->mt_subject_template]);

        return $dataProvider;
    }
}
