<?php

namespace app\models;

use Yii;
use \app\models\base\Device as BaseDevice;

/**
 * This is the model class for table "device".
 */
class Device extends BaseDevice
{
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'Ид.',
            'user_id' => 'Ид Пользователя',
            'device_access_token' => 'Ид Сеанса',
            'device_access_token_expire' => 'Время окончания валидности сеанса',
            'device_name' => 'Марка и модель',
            'device_os' => 'ОС',
            'device_api_id' => 'Ид устройства от производителя',
            'device_created_at' => 'Время регистрации',
            'device_last_login_at' => 'Время последнего логина',
        ];
    }
}
