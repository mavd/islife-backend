<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * PurchaseSearch represents the model behind the search form about `app\models\Purchase`.
 */
class PurchaseSearch extends Purchase
{

    public $userName;
    public $serviceName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'service_id', 'purchase_amount', 'purchase_play_id'], 'integer'],
            [['purchase_status', 'purchase_valid_to', 'purchase_valid_from', 'purchase_created_at', 'purchase_updated_at'], 'safe'],
            [ [ 'serviceName', 'userName' ], 'safe' ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Purchase::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes['serviceName'] =
            [
                'asc' => [ 'service.service_name' => SORT_ASC ],
                'desc' => [ 'service.service_name' => SORT_DESC ],
                'label' => 'Название'
            ];
        $sort->attributes['userName'] =
            [
                'asc' => [ 'u.user_name' => SORT_ASC ],
                'desc' => [ 'u.user_name' => SORT_DESC ],
                'label' => 'Имя'
            ];

        $dataProvider->setSort([
            'attributes' => $sort->attributes]);


        if (!($this->load($params) && $this->validate())) {
            /**
             * Жадная загрузка данных модели Страны
             * для работы сортировки.
             */
            $query->joinWith([ 'service' ]);

            $query->joinWith([ 'user' ]);

            return $dataProvider;
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'service_id' => $this->service_id,
            'purchase_amount' => $this->purchase_amount,
            'purchase_play_id' => $this->purchase_play_id,
            'purchase_valid_to' => $this->purchase_valid_to,
            'purchase_valid_from' => $this->purchase_valid_from,
            'purchase_created_at' => $this->purchase_created_at,
            'purchase_updated_at' => $this->purchase_updated_at,
        ]);

        $query->andFilterWhere(['like', 'purchase_status', $this->purchase_status]);
        // Фильтр
        if ( $this->serviceName ) {
            $query->joinWith([ 'service' => function ($q) {
                $q->where('service.service_name LIKE "%' . $this->serviceName . '%"');
            } ]);
        }
        if ( $this->userName ) {
            $query->joinWith([ 'user' => function ($q) {
                $q->where('u.user_name LIKE "%' . $this->userName . '%"');
            } ]);
        }

        return $dataProvider;
    }
}
