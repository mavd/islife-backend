<?php
namespace app\models;

use Yii;
use \app\models\base\UserX as BaseUserX;

/**
 * This is the model class for table "user".
 */
class UserX extends BaseUserX
{
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'Ид.',
            'username' => 'Логин',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'confirmed_at' => 'Confirmed At',
            'unconfirmed_email' => 'Unconfirmed Email',
            'blocked_at' => 'Время блокировки',
            'registration_ip' => 'IP адрес',
            'flags' => 'Flags',
            'user_isAdmin' => 'Является администратором',
            'user_name' => 'Имя',
            'user_photo' => 'Фото',
            'user_gender' => 'Пол',
            'user_birthday' => 'Дата рождения',
            'user_city' => 'Город',
            'user_path' => 'Путь',
            'user_status' => 'Статус',
            'user_blocked_comment' => 'Комментарий к блокировке',
            'user_health' => 'Здоровье',
            'user_determination' => 'Смелость',
            'user_communication' => 'Общительность',
            'user_skill' => 'Опыт',
            'user_amount' => 'Баланс',
            'user_bonuses' => 'Бонусы',
            'user_last_device_id' => 'Последнее устройство, с которого заходил пользователь',
        ];
    }


}
