<?php

/**
 * Created by PhpStorm.
 * User: mike
 * Date: 20.07.2015
 * Time: 14:55
 */

namespace yii\helpers;

use Yii;

/**
 * Class Utils
 * @package yii\helpers
 */
class Utils
{
    /**
     * @param $d1  string with date in format 'Y-m-d H:i:s'
     * @param $d2  string with date in format 'Y-m-d H:i:s'
     *
     * @return int difference in seconds
     */
    public static function dateDiff($d1, $d2)
    {
        $dt1 = \DateTime::createFromFormat('Y-m-d H:i:s', $d1);
        $dt2 = \DateTime::createFromFormat('Y-m-d H:i:s', $d2);
        return $dt1->getTimestamp() - $dt2->getTimestamp();
    }

    public static function textTemplate($txt, $subst)
    {
        $rpls = [ ];
        foreach ($subst as $key => $val) {
            $rpls['{' . $key . '}'] = $val;
        }
        return strtr($txt, $rpls);
    }

    public static function timeToSec($tm)
    {
        if (!$tm) {
            return 0;
        }
        list($h, $m, $s) = explode(':', $tm);
        if (substr($tm, 0, 1) == '-') {
            return $h * 3600 - $m * 60 - $s;
        }
        return $h * 3600 + $m * 60 + $s;
    }

    public static function dateAddTime($dt, $tm)
    {
        $d = \DateTime::createFromFormat('Y-m-d H:i:s', $dt);
        return date('Y-m-d H:i:s', $d->getTimestamp() + self::timeToSec($tm));
    }

    public static function dateAddHours($dt, $hrs,$template='Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat('Y-m-d H:i:s', $dt);
        return date($template, $d->getTimestamp() + $hrs*3600);
    }
    public static function dateSubTime($dt, $tm)
    {
        $d = \DateTime::createFromFormat('Y-m-d H:i:s', $dt);
        return date('Y-m-d H:i:s', $d->getTimestamp() - self::timeToSec($tm));
    }

    public static function implodeRec($arr)
    {
        $d = '';
        if ($string) {
            $d = ', ';
        }
        if (!is_array($arr)) {

            return $string . $d . $arr;
        }

        foreach($arr as $el){
            if (!is_array($el)){
                $string .=  $d . $el;
            }  else {
                $string .=  $d . self::implodeRec($el);
            }
            $d = ', ';
        }
        return $string;
    }
}