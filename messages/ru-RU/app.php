<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 10.09.2015
 * Time: 5:04
 */
return [
    'Services' => 'Платные услуги',
    'service' => 'услуга',
    'Service' => 'Услуга',
    'Update Service: ' => 'Редактирование услуги: ',
    'Update {modelClass}: ' => 'Редактирование {modelClass}: ',
    'Purchase' => 'Продажа',
    'Message Template' => 'Шаблон сообщений',
    'Messages' => 'Сообщения',
    'Update' => 'Редактировать',
    'Create' => 'Добавить',
    'UpdateSave' => 'Сохранить изменения',
    'UpdateSave' => 'Сохранить',
    'Create Message' => 'Создать сообщение',
    'Advance Search' => 'Поиск',
    'Plays' => 'Игры',
    'Play' => 'Игра',
    'Estimate' => 'Оценка',
    'Rating Log' => 'Рейтинг',
    'Report' => 'Отчет',
    'Reports'=> 'Отчеты',
    'Delete' => 'Удалить',
    'Message' => 'Сообщение',
    'User' => 'Игрок',
    'Create Report'=>'Новый отчет',
    'Purchases'=>'Продажи',
    'dirty'=>'Требует модерации',
    'draft' => 'Черновик',
    'rejected' => 'Отклонен',
    'active' => 'Active',
    'archived' => 'Archived' ,
    'Estimates'=>'Оценки',
    'Create Purchase'=>'Новая покупка',
    'Login Logs' => 'Лог Входов',
    'Login Log' => 'Вход',
    'ID'=>'Ид.',
    'User ID'=>'Игрок',
    'Device ID'=>'Устройство',
    'Login Ip'=>'IP адрес'       ,
    'Login Created At'=>'Дата',
    'new'=>'Новый',
    'send'=>'Послано',

    'verify' => 'Проверен',
    'blocked' => 'Блокирован',
    'common' => 'Общая',
    'one' => 'Одиночная',
    'pair' => 'Парная',
    'Create Service'=>'Добавить тип услуги' ,
    'Estimate Status'=>'Статус'
];