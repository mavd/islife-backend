<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 26.08.2015
 * Time: 21:27
 */

namespace app\components;

use app\models\Device;
use app\models\Message;
use app\models\UserX;
use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\base\View;
use app\models\MessageTemplate;

/**
 * Компонени используется для рассылки сообщений
 * Class Messenger
 * @package app\components
 */
class Messenger Extends Component
{

    /**
     * @var UserX
     */
    private $user;

    /**
     * Messenger constructor.
     *
     * @param int $user_id
     */
    public function __construct($user_id = null)
    {
        $this->user = UserX::find()->where([ 'id' => $user_id ])->one();
    }


    /**
     * рендер сообщений по указанному коду шаблона
     *
     * @param $template       MessageTemplate
     * @param array $_params_ the parameters (name-value pairs) that will be extracted and made available in the view
     *                        file.
     *
     * @return array
     */
    public function renderTemplate(MessageTemplate $template, $_params_ = [ ])
    {
        $vb              = new View();
        $subject         = '';
        $body            = '';
        $subjectTemplate = $_SERVER['DOCUMENT_ROOT'] . '/views/messenger/templates/template_subject_' . $template->id . '.php';

        if (file_exists($subjectTemplate)) {
            $subject = $vb->renderPhpFile($subjectTemplate, $_params_);
        }
        $bodyTemplate = $_SERVER['DOCUMENT_ROOT'] . '/views/messenger/templates/template_body_' . $template->id . '.php';
        if (file_exists($bodyTemplate)) {
            $body = $vb->renderPhpFile($bodyTemplate, $_params_);
        }
        return [ 'subject' => $subject, 'body' => $body ];
    }

    /**
     * Отсылает сообщения на устройства пользователей через PUSH
     *
     *
     * @param $message string
     *
     */
    public function sendPush($message)
    {
        $apnsGcm = Yii::$app->apnsGcm;
        $devices = Device::find()
            ->where([ 'user_id' => $this->user->id])
            ->asArray()
            ->all();
        if ($devices) {
            Yii::info('sendPush','api');
            try {
                foreach ($devices as $device) {
                    if (strpos($device['device_os'], 'Android') !== false) {
                        Yii::info('sendPush Android', 'api');
                        $apnsGcm->send(\bryglen\apnsgcm\ApnsGcm::TYPE_GCM, $device['device_api_id'], $message,
                            [ 'customerProperty' => 1 ],
                            [ 'timeToLive' => 3 ]
                        );
                    } else {
                        if (!preg_match('~^[a-f0-9]{64}$~i',  $device['device_api_id']))  {
                            continue;
                        }
                        Yii::info('sendPush ios', 'api');

                        $apnsGcm->send(\bryglen\apnsgcm\ApnsGcm::TYPE_APNS, $device['device_api_id'], $message,
                            [ 'customerProperty' => 1 ],
                            [
                                'sound' => 'default',
                                'badge' => 1
                            ]
                        );
                    }
                }
            }catch(ApnsPHP_Message_Exception $e){
                print_r($e);
            }
        }
    }

    /**
     * @param $body
     */
    public function sendInform(&$body)
    {
        if (!$this->user) {
            return;
        }
        $message                 = new Message();
        $message->message_header = '';
        $message->message_text   = $body;
        $message->message_mode   = 'private';
        $message->user_id        = $this->user->id;
        $message->message_type   = 'inform';
        $message->message_status = 'new';
        $message->save();

    }

    /**
     * Отсылает сообщения на  пользователям через Email
     *
     * @param $subject
     * @param $body
     *
     * @return bool
     */
    public function sendEmail(&$subject, &$body)
    {

        if ($this->user->confirmed_at) {
            return Yii::$app->mailer->compose([ 'html' => $body, 'text' => 'text/' . $body ])
                                    ->setTo($this->user->email)
                                    ->setFrom(Yii::$app->params['adminEmail'])
                                    ->setSubject($subject)
                                    ->send();
        }

    }

    //TODO public

    /**
     * Отправка сообщений в соответствии с шаблоном
     *
     * @param $user_id        integer Ид. игрока, получателя
     * @param $template_code  string код шаблона
     * @param array $_params_ the parameters (name-value pairs) that will be extracted and made available in the view
     *                        file.
     * @param bool|false $straight
     *
     * @return array
     */
    public function sendByTemplate(
        $user_id,
        $template_code,
        $_params_ = [ ],
        $straight = false)
    {
        $template = MessageTemplate::find()
                                   ->where([ 'mt_code' => $template_code ])->one();
        if (!$user_id) {
            return [ 'result' => 'fail', 'message' => 'user missing' ];
        }
        $this->user = UserX::find()->where([ 'id' => $user_id ])->one();
        if (!$this->user) {
            return [ 'result' => 'fail', 'message' => 'user not found' ];
        }
        if ($template) {
            $msg                     = $this->renderTemplate($template, $_params_);
            $message                 = new Message();
            $message->message_header = $msg['subject'];
            $message->message_text   = $msg['body'];
            $message->message_mode   = 'private';
            $message->user_id        = $user_id;
            $message->message_type   = $template->mt_type;
            $message->message_status = 'new';
            $message->save();
//            $errors = $message->getErrors();
//                return [
//                    'result'=>'fail','message'=>'error', 'errors'=>$errors,
//                    'msg'=>  $msg
//                ];

            if ($straight) {
                switch ($template->mt_type) {
                    case 'push':
                        $this->sendPush($message['body']);
                        $message->message_status = 'send';
                        $message->save();
                        break;
                    case 'email':
                        $this->sendEmail($message['subject'], $message['body']);
                        $message->message_status = 'send';
                        $message->save();
                }

            }
            return [ 'result' => 'ok' ];
        }
        return [ 'result' => 'fail', 'message' => 'template not found' ];
    }

}