<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 26.08.2015
 * Time: 21:27
 */

namespace app\components;

use app\models\Play;
use app\models\Purchase;
use app\models\RatingLog;
use app\models\Task;
use app\models\UserX;
use Yii;
use yii\base\Component;
use app\components\Messenger;

/**
 * Компонент инкапсулирует работу с Игрой
 * Class PlayManager
 * @package app\components
 */
class PlayUtils Extends Component
{

    private $balls = [ ];
    /**
     * @var Userx
     */
    private $user;
    /**
     * @var Task
     */
    private $task;
    /**
     * @var Play
     */
    private $play;

    /**
     * @var Messenger
     */
    private $messenger;

    /**
     * Найти зависшие, просроченный игры, и прервать их работу.
     */
    public function findAndCloseDepricated()
    {
        //TODO Code
    }

    /**
     * Понижение рейтинга игрока
     */
    private function subRate()
    {
        $this->user->user_health -= round($this->task->task_health * Yii::$app->params['withdrawal_percentage'] / 100);
        $this->user->user_determination -= round($this->task->task_determination * Yii::$app->params['withdrawal_percentage'] / 100);
        $this->user->user_communication -= round($this->task->task_communication * Yii::$app->params['withdrawal_percentage'] / 100);
        $this->user->user_skill -= round($this->task->task_skill * Yii::$app->params['withdrawal_percentage'] / 100);
        $this->user->save();
    }

    /**
     * Добавление баллов игроку
     *
     * @param $x2 - множитель , если есть X2 - то =2, иначе 1
     */
    private function addRate($x2)
    {
        $this->user->user_health += ($this->task->task_health * $x2);
        $this->user->user_determination += ($this->task->task_determination * $x2);
        $this->user->user_communication += ($this->task->task_communication * $x2);
        $this->user->user_skill += ($this->task->task_skill * $x2);
        $this->user->save();

    }

    /**
     * запись в лог рейтинга
     *
     * @param $delta integer - изменение рейтинга
     */
    private function logRate($delta)
    {
        $rl                 = new RatingLog();
        $rl->isNewRecord    = 1;
        $rl->play_id        = $this->play->id;
        $rl->user_id        = $this->play->user_id;
        $rl->rate_value     = $delta;
        $rl->rate_sum       = $this->user->user_skill;
        $rl->rate_play_type = $this->play->play_type;
        $rl->save();

    }

    /**
     * Отправка сообщений
     *
     * @param $push   string , код шаблона , если пустое то push сообщение не отправляется
     * @param $inform string, код шаблона , если пустое то inform сообщение не отправляется
     */
    private function send($push, $inform)
    {
        if ($push) {
            $this->messenger->sendByTemplate($this->play->user_id, $push,
                [ 'play' => $this->play,
                  'task' => $this->task,
                  'user' => $this->user
                ], false
            );
        }
        if ($inform) {
            $this->messenger->sendByTemplate($this->play->user_id, $inform,
                [ 'play' => $this->play,
                  'task' => $this->task,
                  'user' => $this->user
                ]
            );
        }
    }

    /**
     *  обработка отказа игрока от игры
     */
    private function rejectPlay()
    {
        $this->play
            ->play_result
                  = 'reject';
        $this->play
            ->play_status
                  = 'done';
        if ($this->play->play_type==='pair'){
            $purchase = Purchase::find()
                                ->with('service')
                                ->where([
                                    'purchase_play_id' => $this->play->id,
                                    'user_id' => $this->play->user_id

                                ])
                                ->andWhere("service_id in (SELECT id FROM service WHERE service_type='pair' AND service_code ='srv_save_rate')")
                                ->one();
            if (!$purchase) {
                $this->subRate();
                $this->logRate(-round($this->task->task_skill * Yii::$app->params['withdrawal_percentage'] / 100));
            }
            $this->user = UserX::find()
                               ->where([ 'id' => $this->play->user2_id ])->one();
            $purchase = Purchase::find()
                                ->with('service')
                                ->where([
                                    'purchase_play_id' => $this->play->id,
                                    'user_id' => $this->play->user2_id

                                ])
                                ->andWhere("service_id in (SELECT id FROM service WHERE service_type='pair' AND service_code ='srv_save_rate')")
                                ->one();
            if (!$purchase) {
                $this->subRate();
                $this->logRate(-round($this->task->task_skill * Yii::$app->params['withdrawal_percentage'] / 100));
            }
        }   else {
            $purchase = Purchase::find()
                                ->with('service')
                                ->where([
                                    'purchase_play_id' => $this->play->id,
                                    'user_id' => $this->play->user_id

                                ])
                                ->andWhere("service_id in (SELECT id FROM service WHERE service_type='one' AND service_code ='srv_save_rate')")
                                ->one();
            if (!$purchase) {
                $this->subRate();
                $this->logRate(-round($this->task->task_skill * Yii::$app->params['withdrawal_percentage'] / 100));
            }
        }
        $this->send('pushReject', 'informReject');
    }

    /**
     * обработка прерывания игры, напрмер по таймауту
     */
    private function cancelPlay()
    {
        $this->play->play_result = 'canceled';
        $this->play->play_status = 'done';
        $this->subRate();
        $this->logRate(-round($this->task->task_skill * Yii::$app->params['withdrawal_percentage'] / 100));

        $this->send('pushRejectPlay', 'informRejectPlay');

        if ($this->play->play_type === 'pair' && $this->play->user2_id) {
            $this->user = UserX::find()
                               ->where([ 'id' => $this->play->user2_id ])->one();
            $this->subRate();
            $this->logRate(-round($this->task->task_skill * Yii::$app->params['withdrawal_percentage'] / 100));
            $this->send('pushCancel', 'informCancel');
        }
    }

    /**
     * обработка игры , оцененной отицательно
     */
    private function failPlay()
    {
        $this->play->play_result = 'fail';
        $this->play->play_status = 'done';

        $this->subRate();
        $this->logRate(-round($this->task->task_skill * Yii::$app->params['withdrawal_percentage'] / 100));

        $this->send('pushFail', 'informFail');

        if ($this->play->play_type === 'pair' && $this->play->user2_id) {
            $this->user = UserX::find()
                               ->where([ 'id' => $this->play->user2_id ])->one();
            $this->subRate();
            $this->logRate(-round($this->task->task_skill * Yii::$app->params['withdrawal_percentage'] / 100));
            $this->send('pushFail', 'informFail');
        }
    }

    /**
     * обработка успешного окончания игры
     *
     */
    private function successPlay()
    {
        $this->play->play_result = 'success';
        $this->play->play_status = 'done';
        $x2                      = $this->play->play_purchase_id ? 2 : 1;
        $this->addRate($x2);
        $this->logRate($this->task->task_skill * $x2);

        $this->send('pushSuccess', 'informSuccess');

        if ($this->play->play_type === 'pair' && $this->play->user2_id) {
            $this->user = UserX::find()
                               ->where([ 'id' => $this->play->user2_id ])->one();
            $x2         = $this->play->play_purchase2_id ? 2 : 1;
            $this->addRate($x2);
            $this->logRate($this->task->task_skill * $x2);
            $this->send('pushSuccess', 'informSuccess');
        }
    }

    /**
     * Анализ результатов игры, раздача рейтингов, рассылка оповещений, запись в лог
     *
     *
     * @param Play $play
     */
    public function  saveResult(Play &$play)
    {
        $this->play      = $play;
        $this->user      = UserX::find()
                                ->where([ 'id' => $play->user_id ])->one();
        $this->task      = Task::find()
                               ->where([ 'id' => $play->task_id ])->one();
        $this->messenger = new Messenger();

        switch ($play->play_status) {
            case 'rejected':
                $this->rejectPlay();
                break;
            case 'canceled':
                $this->cancelPlay();
                break;
            case 'estimated' :
                if ($play->play_estimate_sum / $play->play_estimate_count < 2.5) {
                    $this->failPlay();
                } else {
                    $this->successPlay();
                }
        }
        $play->save();

    }
}