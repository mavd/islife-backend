<?php

namespace app\controllers\app\models;

use Yii;
use app\models\UserX;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserXController implements the CRUD actions for UserX model.
 */
class UserXController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => [ 'post' ],
                ],
            ],
        ];
    }

    /**
     * Lists all UserX models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => UserX::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserX model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $model            = $this->findModel($id);
        $providerAction   = new \yii\data\ArrayDataProvider([
            'allModels' => $model->actions,
        ]);
        $providerDevice   = new \yii\data\ArrayDataProvider([
            'allModels' => $model->devices,
        ]);
        $providerEstimate = new \yii\data\ArrayDataProvider([
            'allModels' => $model->estimates,
        ]);
        $providerMessage  = new \yii\data\ArrayDataProvider([
            'allModels' => $model->messages,
        ]);
        $providerPurchase = new \yii\data\ArrayDataProvider([
            'allModels' => $model->purchases,
        ]);
        $providerReport   = new \yii\data\ArrayDataProvider([
            'allModels' => $model->reports,
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerAction' => $providerAction,
            'providerDevice' => $providerDevice,
            'providerEstimate' => $providerEstimate,
            'providerMessage' => $providerMessage,
            'providerPurchase' => $providerPurchase,
            'providerReport' => $providerReport,

        ]);
    }

    /**
     * Creates a new UserX model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserX();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect([ 'view', 'id' => $model->id ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserX model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect([ 'view', 'id' => $model->id ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserX model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithChildren();

        return $this->redirect([ 'index' ]);
    }
    
    /**
     *
     * untuk export pdf pada saat actionView
     *
     * @param int $id
     *
     * @return type
     */
    public function actionPdf($id)
    {
        $model            = $this->findModel($id);
        $providerAction   = new \yii\data\ArrayDataProvider([
            'allModels' => $model->actions,
        ]);
        $providerDevice   = new \yii\data\ArrayDataProvider([
            'allModels' => $model->devices,
        ]);
        $providerEstimate = new \yii\data\ArrayDataProvider([
            'allModels' => $model->estimates,
        ]);
        $providerMessage  = new \yii\data\ArrayDataProvider([
            'allModels' => $model->messages,
        ]);
        $providerPurchase = new \yii\data\ArrayDataProvider([
            'allModels' => $model->purchases,
        ]);
        $providerReport   = new \yii\data\ArrayDataProvider([
            'allModels' => $model->reports,
        ]);


        $content = $this->renderAjax('_pdf', [
            'model' => $model,
            'providerAction' => $providerAction,
            'providerDevice' => $providerDevice,
            'providerEstimate' => $providerEstimate,
            'providerMessage' => $providerMessage,
            'providerPurchase' => $providerPurchase,
            'providerReport' => $providerReport,

        ]);

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE,
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => [ 'title' => \Yii::$app->name ],
            'methods' => [
                'SetHeader' => [ \Yii::$app->name ],
                'SetFooter' => [ '{PAGENO}' ],
            ]
        ]);

        return $pdf->render();
    }
    
    /**
     * Finds the UserX model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return UserX the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserX::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Action to load a tabular form grid
     * for Play
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAddPlay()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Play');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [ ];
            return $this->renderAjax('_formPlay', [ 'row' => $row ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Action to load a tabular form grid
     * for Device
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAddDevice()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Device');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [ ];
            return $this->renderAjax('_formDevice', [ 'row' => $row ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Action to load a tabular form grid
     * for Estimate
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     */
    public function actionAddEstimate()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Estimate');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [ ];
            return $this->renderAjax('_formEstimate', [ 'row' => $row ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Action to load a tabular form grid
     * for Message
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     */
    public function actionAddMessage()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Message');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [ ];
            return $this->renderAjax('_formMessage', [ 'row' => $row ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Action to load a tabular form grid
     * for Purchase
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     */
    public function actionAddPurchase()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Purchase');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [ ];
            return $this->renderAjax('_formPurchase', [ 'row' => $row ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Action to load a tabular form grid
     * for Report
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     */
    public function actionAddReport()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Report');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [ ];
            return $this->renderAjax('_formReport', [ 'row' => $row ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Action to load a tabular form grid
     * for SocialAccount
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     */
    public function actionAddSocialAccount()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('SocialAccount');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [ ];
            return $this->renderAjax('_formSocialAccount', [ 'row' => $row ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Action to load a tabular form grid
     * for Token
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     */
    public function actionAddToken()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Token');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [ ];
            return $this->renderAjax('_formToken', [ 'row' => $row ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
