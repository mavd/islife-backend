<?php

namespace app\controllers\admin;

use app\components\Messenger;
use Yii;
use app\models\Message;
use app\models\UserX;
use app\models\MessageSearch;

use app\controllers\gen\MessageController as genController;

/**
 * MessageController implements the CRUD actions for Message model.
 */
class MessageController extends genController
{
    /**
     * Creates a new Message model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model               = new Message();
        $model->message_mode = 'public';
        $user                = null;
        if (array_key_exists('user_id', $_REQUEST)) {
            $user_id = $_REQUEST['user_id'];
            $user    = UserX::find()
                            ->select('id,id,email,user_name')
                            ->where([ 'id' => $user_id ])->one();
            if ($user) {
                $model->user_id      = $user_id;
                $model->message_mode = 'private';
            }
        }
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            if ($model->message_type == 'push' && $model->message_mode ='private'){
                $messenger = new Messenger($user_id);
                $messenger->sendPush($model->message_text);
                $model->message_status ='send';
                $model->save();
            }
            return $this->redirect([ 'view', 'id' => $model->id ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'user' => $user
            ]);
        }
    }

    /**
     * Updates an existing Message model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user  = null;
        if ($model->message_mode === 'private') {
            $user = $model->getUser()->one();
        }
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect([ 'view', 'id' => $model->id ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'user' => $user
            ]);
        }
    }
    /**
     * Lists all Message models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Message model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $user  = null;
        if ($model->message_mode === 'private') {
            $user = $model->getUser()->one();
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'user' => $user
        ]);
    }
}
