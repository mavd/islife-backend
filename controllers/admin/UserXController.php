<?php

namespace app\controllers\admin;
use app\models\Device;
use app\components\Messenger;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\controllers\gen\UserXController as genController;
use yii\filters\AccessControl;
/**
 * UserXController implements the CRUD actions for UserX model.
 */
class UserXController extends genController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post','get'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update','delete', 'block','deblock'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    public function actionBlock($id,$message){
        $model = $this->findModel($id);
        \Yii::$app->response->format =  Response::FORMAT_JSON;
        if ($model){
            $model->user_status = 'blocked';
            $model->user_blocked_comment = $message;
            if ($model->save()){
                $messenger = new Messenger($id);
                $messenger->sendPush($message);
                $messenger->sendInform($message);
                return ['result' => 'ok'];
            } else {
                return ['result' => 'fail', 'message'=>print_r($model->getErrors(),1)];
            }

        }
        return ['result' => 'fail','message'=>'notFound'];
    }

    /**
     * @param $id
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDeblock($id){
        $model = $this->findModel($id);
        \Yii::$app->response->format =  Response::FORMAT_JSON;
        if ($model){
            $model->user_status = 'active';
            if ($model->save()){

                return ['result' => 'ok'];
            } else {
                return ['result' => 'fail', 'message'=>print_r($model->getErrors(),1)];
            }

        }
        return ['result' => 'fail','message'=>'notFound'];
    }
    /**
     * Updates an existing UserX model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldStatus = $model->user_status;
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            if ($oldStatus!==$model->user_status && $model->user_status==='blocked'){
                $messenger = new Messenger($id);
                $messenger->sendPush($model->user_blocked_comment);
                $messenger->sendInform($model->user_blocked_comment);
            }
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            $device=Device::find()->where(['id'=>$model->user_last_device_id]);
            return $this->render('update', [
                'model' => $model,
                'device'=>$device
            ]);
        }
    }
}
