<?php

namespace app\controllers\admin;

use Yii;

use app\controllers\gen\EstimateController as genEstimateController;
/**
 * EstimateController implements the CRUD actions for Estimate model.
 */
class EstimateController extends genEstimateController
{
}
