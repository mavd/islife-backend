<?php

namespace app\controllers\admin;

use Yii;

use yii\filters\VerbFilter;
use app\controllers\gen\TaskController as genController;
use app\components\Messenger;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends genController
{


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post','get'],
                ],
            ],
            'access' => [
                'class' =>  AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [ 'index', 'view', 'create', 'update', 'delete', 'public', 'archive' ],
                        'roles' => [ '@' ]
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    public function actionPublic($id)
    {
        $model                       = $this->findModel($id);
        Yii::$app->response->format =  Response::FORMAT_JSON;
        if ($model) {
            $model->task_status = 'active';
            if ($model->save()) {

                return [ 'result' => 'ok' ];
            } else {
                return [ 'result' => 'fail', 'message' => print_r($model->getErrors(), 1) ];
            }

        }
        return [ 'result' => 'fail', 'message' => 'notFound' ];
    }

    public function actionArchive($id)
    {
        $model                       = $this->findModel($id);
        Yii::$app->response->format =  Response::FORMAT_JSON;
        if ($model) {
            $model->task_status = 'archived';
            if ($model->save()) {

                return [ 'result' => 'ok' ];
            } else {
                return [ 'result' => 'fail', 'message' => print_r($model->getErrors(), 1) ];
            }

        }
        return [ 'result' => 'fail', 'message' => 'notFound' ];
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model           = $this->findModel($id);
        $old_task_status = $model->task_status;
        if ($model->loadAll(Yii::$app->request->post())) {
            if ($model->task_status == 'rejected' &&
                $model->task_status != $old_task_status
            ) {
                if ($model->task_comment_reject) {
                    $messenger = new Messenger($model->task_author_id);
                    $messenger->sendInform($model->task_comment_reject);
                }
            }
            if ($model->saveAll()) {
                return $this->redirect([ 'view', 'id' => $model->id ]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
