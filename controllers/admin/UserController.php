<?php

namespace app\controllers\admin;

use app\components\Messenger;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\controllers\gen\UserXController as genController;
use yii\filters\AccessControl;
/**
 * UserXController implements the CRUD actions for UserX model.
 */
class UserController extends genController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post','get'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update','delete', 'block','deblock'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }



    /**
     * Updates an existing UserX model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldStatus = $model->user_status;
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            if ($oldStatus!==$model->user_status && $model->user_status==='blocked'){
                $messenger = new Messenger($id);
                $messenger->sendPush($model->user_blocked_comment);
                $messenger->sendInform($model->user_blocked_comment);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
}
