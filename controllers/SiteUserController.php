<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use dektrium\user\controllers\AdminController as BaseAdminController;

use app\models\User;

use yii\base\ExitException;
use yii\base\Model;
use yii\helpers\Url;


class SiteUserController extends  BaseAdminController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }



    public function actionIndex()
    {
        //return $this->render('index');
    }


    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndexList()
    {
        Url::remember('', 'actions-redirect');
        $searchModel  = new \app\models\UserSearch();
       // $dataProvider = $searchModel->search(Yii::$app->request->get());

       // return $this->render('index', [
       //     'dataProvider' => $dataProvider,
       //     'searchModel'  => $searchModel,
       // ]);
    }

}
