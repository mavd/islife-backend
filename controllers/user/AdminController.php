<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\controllers\user;

use dektrium\user\controllers\AdminController as BaseAdminController;
use app\models\UserSearch;
use app\models\User;
use Yii;
use yii\base\ExitException;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\Controller;
class AdminController extends BaseAdminController
{
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionList()
    {
        Url::remember('', 'actions-redirect');
        $searchModel  = Yii::createObject(UserSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    public function actionIndex (){
        return false;
    }
    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var User $user */
        $user = Yii::createObject([
            'class'    => User::className(),
            'scenario' => 'create',
        ]);

        $this->performAjaxValidation($user);

        if ($user->load(Yii::$app->request->post())) {
            $user->user_isAdmin = 1;

            if ($user->create()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('user', 'User has been created'));
                return $this->redirect(['update', 'id' => $user->id]);
            }
        }
        return $this->render('create', [
            'user' => $user
        ]);
    }


}
