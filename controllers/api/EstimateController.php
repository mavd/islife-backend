<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 11.08.2015
 * Time: 1:17
 */

namespace app\controllers\api;

use app\components\PlayUtils;
use app\models\Estimate;
use app\models\Play;
use app\models\Report;
use app\models\User;
use dektrium\user\traits\AjaxValidationTrait;
use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;

//use dektrium\user\models\User;

/**
 * Class EstimateController
 * Обеспечивает доступ к получению двнных для оценки задания, и выставление оценки
 *
 * @package app\controllers\api
 */
class EstimateController extends ActiveController
{

    use AjaxValidationTrait;

    /**
     * Базовая модель
     *
     * @var string
     */
    public $modelClass = 'app\models\Estimate';

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        $behaviors                            = parent::behaviors();
        $behaviors['authenticator']['class']  = QueryParamAuth::className();
        $behaviors['authenticator']['except'] = [ 'views' ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'next' => [ 'get', 'post' ],
                'view' => [ 'get' ],
                'd' => [ 'get' ],
            ],
        ];
        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset(
            $actions['index'],
            $actions['view'],
            $actions['create'],
            $actions['update'],
            $actions['delete']
        );
        return $actions;
    }

    /**
     * Подготовка данных для новой оценки
     *
     * Поиск неоцененной игры, регистрация оценки
     * @return array
     */
    protected function createEstimate()
    {
        $early = Estimate::find()
                         ->select('play_id')
                         ->where([ 'user_id' => Yii::$app->user->id ]);
//        $report = Report::find()
//                         ->distinct()
//                         ->select('play_id')
//                         ->where(['!=', 'user_id','Yii::$app->user->id' ]);

        $play  = Play::find()
                     ->where([ '!=', 'user_id', Yii::$app->user->id ])
                     ->andWhere([ '!=', 'user2_id', Yii::$app->user->id ])
                     ->andWhere([ 'play_status' => 'reported' ])
                     ->andWhere([ 'not in ', 'id', $early ])
//                     ->andWhere([ 'in ', 'id', $report ])
                     ->one();
        if (!$play) {
            return [ 'result' => 'not found', 'message' => 'нет отчетов для оценки' ];
        }
        $photos = Report::find()
                        ->where([ 'play_id' => $play->id ])
                        ->select('report_photo')
                        ->asArray()
                        ->all();


//        $user1 = User::find()
//                     ->where([ 'id' => $play->user_id ])
//                     ->select('username')
//                     ->scalar();
//        $user2 = '';
//        if ($play->user2_id) {
//            $user2 = User::find()
//                         ->where([ 'id' => $play->user2_id ])
//                         ->select('username')
//                         ->scalar();
//        }

        $estimate                  = new Estimate;
        $estimate->isNewRecord     = true;
        $estimate->play_id         = $play->id;
        $estimate->user_id         = Yii::$app->user->id;
        $estimate->estimate_status = 'new';
        if ($estimate->save()) {

            return [ 'result' => 'ok',
                     'play' => [ 'play_id' => $play->id,
                                 'task' => $play->task_id,

                                 //'task_name2' => $play->task->task_name2,
                                 //'task_description' => $play->task->task_description,
                                 'user_id' => $play->user_id,
                                 'user2_id' => $play->user2_id,
                                 'photos' => $photos ]
            ];
        }
        return [ 'result' => 'fails','message'=>'db error','error'=>$estimate->getErrors()];
    }




    /**
     * Сохранение оценки и
     * подготовка данных для новой оценки
     *
     * Данные об оценке передаются методом POST, если оценки нет,
     * то запросить следующий отчет для оценки  методом GET
     *
     * @api  /api/estimate/next?access-token=1
     *
     * @param $play_id
     * @param $estimate
     *
     * @return array
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionNext($play_id='',$estimate='')
    {
        if ($_POST) {
            if (
                array_key_exists('play_id', $_POST) && $_POST['play_id'] &&
                array_key_exists('estimate', $_POST) && $_POST['estimate']
            ) {
                $estimate    = Estimate::find()->where([
                    'play_id' => $_REQUEST['play_id'],
                    'user_id' => Yii::$app->user->id,
                    'estimate_status' => 'new'
                ])->one();
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($estimate) {
                        $play = Play::find()->where([ 'id' => $estimate->play_id ])->one();
                        if ($play) {
                            $estimate->estimate_value  = $_REQUEST['estimate'];
                            $estimate->estimate_status = 'ready';
                            $estimate->save();
                            if ($play->play_estimate_count < 5) {
                                $play->play_estimate_sum += $_REQUEST['estimate'];
                                $play->play_estimate_count += 1;
                                $play->play_estimate =   $play->play_estimate_sum  / $play->play_estimate_count;
                                if ($play->play_estimate_count === 5) {
                                    $play->play_status = 'estimated';
                                    $pu                = new PlayUtils();
                                    $pu->saveResult($play);
                                } else {
                                    $play->save();
                                }
                            }

                        }
                    }
                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->createEstimate();
    }
}
