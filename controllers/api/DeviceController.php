<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 11.08.2015
 * Time: 1:17
 */

namespace app\controllers\api;

use app\models\Play;
use app\models\Device;
use Yii;
use yii\base\Exception;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;


/**
 * Class DeviceController
 * Изменить данные об устройсте пользователя
 *
 * Доступные методы АПИ и методы доступа
 * 'reset'  => ['get', 'post'],
 *
 * @package app\controllers\api
 */
class DeviceController extends ActiveController
{

    public $modelClass = 'app\models\Device';

    public function behaviors()
    {
        $behaviors                            = parent::behaviors();
        $behaviors['authenticator']['class']  = QueryParamAuth::className();
        $behaviors['authenticator']['except'] = [ ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'reset' => [ 'get', 'post' ],
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],
            $actions['create'],
            $actions['view'],
            $actions['update'],
            $actions['delete']);
        return $actions;
    }

    /**
     * Метод записывает новый API_ID (device token),
     * Метод нужно вызывать после перезагрузки программы на клиенте( когда изменется device token)
     * ##api - GET,POST
     *
     * @uses  /api/device/reset?access-token=1
     *     *
     * @param string $api_id
     *
     * @return array
     */
    public function actionReset($api_id=''){
        Yii::info($api_id,'api');
        $token = $_GET['access-token'];
        $device = Device::find()
            ->where(['device_access_token'=>$token])->one();
        if (!$device){
            return [
                'result'=>'fail','message'=>'device not found'
            ];
        }
        if (array_key_exists('api_id',$_POST)){
            $api_id = $_POST['api_id'];
        }
        $device->device_api_id = $api_id;
        if ($device->save()){
            return ['result'=>'ok'];
        }
        return [
            'result'=>'fail','message'=>'db error', 'errors'=>$device->getErrors()
        ];
    }
}
