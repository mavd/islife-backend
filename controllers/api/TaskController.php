<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 11.08.2015
 * Time: 1:17
 */

namespace app\controllers\api;

use app\models\Play;
use app\models\Task;
use Yii;
use yii\base\Exception;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;


/**
 * Class TaskController
 * выбор задания для исполнеия, предложение о новом задании
 *
 * Доступные методы АПИ и методы доступа
 * 'find'  => ['get', 'post'],
 * 'offer' => ['post'],
 * 'view'  => ['get'],
 *
 * @package app\controllers\api
 */
class TaskController extends ActiveController
{

    public $modelClass = 'app\models\Task';

    public function behaviors()
    {
        $behaviors                            = parent::behaviors();
        $behaviors['authenticator']['class']  = QueryParamAuth::className();
        $behaviors['authenticator']['except'] = [ ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'find' => [ 'get', 'post' ],
                'offer' => [ 'post' ],
                'view' => [ 'get' ],
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],
            $actions['create'],
            $actions['update'],
            $actions['delete']);
        return $actions;
    }

    /**
     * Записать задание предложенное игроком
     *
     * @api
     * @uses   query  /api/task/offer?access-token=1
     *
     * param  taskOffer , JSON string со следующими обязательными полями
     * string  task_name
     * string  task_description
     * string  task_type
     * integer task_timer_hour
     * integer task_timer_minute
     * integer task_health
     * integer task_determination
     * integer task_communication
     * integer task_skill
     */
    public function actionOffer()
    {
        $taskOffer = json_decode($_REQUEST['taskOffer']);
        try {
            $task                   = new Task();
            $task->isNewRecord      = true;
            $task->task_name        = $taskOffer->task_name;
            $task->task_description = $taskOffer->task_description;
            $task->task_timer_hour       = $taskOffer->task_timer_hour;
            $task->task_timer_minute       = $taskOffer->task_timer_minute;
            $task->task_health        = $taskOffer->task_health;
            $task->task_determination = $taskOffer->task_determination;
            $task->task_communication = $taskOffer->task_name;
            $task->task_skill         = $taskOffer->task_skill;
            $task->task_status        = 'dirty';
            if ($task->save()) {
                return [ 'result' => 'ok' ];
            }
            Yii::info('Task save error ' . print_r($task->getErrors()),'api');
            return [ 'result' => 'fail', 'message' => 'db error' ];
        } catch (Exception $e) {
            return [ 'result' => 'fail', 'message' => 'JSON structure error' ];
        }

    }

    /**
     * Поиск заданий
     *
     * @api
     * @uses   query  /api/task/find?access-token=1
     *
     * Необязательный параметр cnt - количество заданий на вкладке, по умолчанию 5
     * cnt может быть пердан в GET или POST
     *
     * @uses   query  /api/task/find?access-token=1&cnt=6
     *
     * @return array
     */
    public function actionFind()
    {
        $cnt = 5;
        if (array_key_exists('cnt', $_REQUEST)) {
            $cnt = $_REQUEST['cnt'];
        }
        $subTask          = Play::find()
                                ->select('task_id')
                                ->where([ 'user_id' => Yii::$app->user->id ]);
        $subTaskAgreement = Play::find()
                                ->select('task_id')
                                ->where([ 'user2_id' => Yii::$app->user->id ])
                                ->andWhere([ 'play_status' => 'invited' ]);
        $tasksOne         = Task::find()
                                ->select(' id,task_name,task_timer_hour,task_timer_minute,task_name2,task_description,task_health,task_determination,task_communication,task_skill')
                                ->andWhere([ 'not in', 'id', $subTask ])
                                ->andWhere([ ' = ', 'task_type', 'one' ])
                                ->andWhere([ 'task_status' => 'active' ])
                                ->limit($cnt + 5)
                                ->asArray()
                                ->all();
        $tasksPair        = Task::find()
                                ->select(' id,task_name,task_timer_hour,task_timer_minute,task_name2,task_description,task_health,task_determination,task_communication,task_skill')
                                ->andWhere([ 'not in', 'id', $subTask ])
                                ->andWhere([ '<>', 'task_type', 'one' ])
                                ->andWhere([ 'task_status' => 'active' ])
                                ->limit($cnt)
                                ->asArray()
                                ->all();
        $tasksAgreement   = Task::find()
                                ->select(' id,task_name,task_timer_hour,task_timer_minute,task_name2,task_description,task_health,task_determination,task_communication,task_skill')
                                ->andWhere([ 'in', 'id', $subTaskAgreement ])
                                ->andWhere([ 'task_status' => 'active' ])
                                ->limit($cnt)
                                ->asArray()
                                ->all();
        return [
            'one' => $tasksOne,
            'pair' => $tasksPair,
            'invite' => $tasksAgreement
        ];
    }


}
