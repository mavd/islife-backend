<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 11.08.2015
 * Time: 1:17
 */

namespace app\controllers\api;

use app\models\Purchase;
use app\models\Service;
use app\models\UserX;
use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;

//use dektrium\user\models\User;

/**
 * Class PurchaseController
 * Методы для обеспечения возможности покупки
 *
 * Доступные методы АПИ и методы доступа
 *
 *   'on-store' => 'post',
 *   'on-cash'  => 'post',
 *   'cash'     => 'post',
 *   'services' => 'get'
 *   'list'     => 'get','post'
 * @package app\controllers\api
 */
class PurchaseController extends ActiveController
{
    public $modelClass = 'app\models\Purchase';

    public function behaviors()
    {
        $behaviors                            = parent::behaviors();
        $behaviors['authenticator']['class']  = QueryParamAuth::className();
        $behaviors['authenticator']['except'] = [ 'views' ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'on-store' => [ 'get', 'post' ],
                'on-cash' => [ 'get', 'post' ],
                'cash' => [ 'get', 'post' ],
                'list' => [ 'get', 'post' ],
                'services' => [ 'get' ]
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'], $actions['create'], $actions['update'], $actions['delete']);
        return $actions;
    }

    /**
     * Регистрация покупки выполненной на **Store
     *
     * @api
     * @uses   query  /api/purchase/on-store?access-token=q-1&service_code=2&date_purchase=...&date_start=...&date_end
     * #Метод   POST
     *
     * @param string $service_code
     * @param bool|string $date_purchase
     * @param string $date_start
     * @param string $date_end
     * @param int $service_param
     *
     * @return array
     */
    public function actionOnStore($service_code = '', $service_param = 0, $date_purchase = '', $date_start = '', $date_end = '')
    {

        $service_code  = $_REQUEST['service_code'];
        $date_purchase = $_REQUEST['date_purchase'];
        $date_start    = $_REQUEST['date_start'];
        $date_end      = $_REQUEST['date_end'];
        $service_param = $_REQUEST['service_param'];

        $service = Service::find()->where([ 'service_code' => $service_code ])->one();


        if (!$service) {
            return [ 'result' => 'fail', 'message' => 'service not found' ];
        }
        $purchase = Purchase::find()
                            ->where([
                                'user_id'=>  Yii::$app->user->id,
                                'service_id' => $service->id,
                                'service_param' => $service_param,
                                'purchase_created_at' => $date_purchase
                            ])->one();
        if (!$purchase) {
            $purchase = new Purchase();
        }   else{
            $purchase->isNewRecord=false;
        }

        $purchase->user_id             = Yii::$app->user->id;
        $purchase->service_id          = $service->id;
        $purchase->service_param       = $service_param;
        $purchase->purchase_amount     = $service->service_amount;
        $purchase->purchase_valid_from = $date_start;
        $purchase->purchase_valid_to   = $date_end;
        $purchase->purchase_created_at = $date_purchase;
        if ($purchase->save()) {
            return [ 'result' => 'ok' ];
        }
        return [ 'result' => 'fail', 'message' => 'db error', 'errors' => $purchase->getErrors() ];
    }

    /**
     * Возвращает список покупок игрока сделанных после указанной даты.
     * Если дата не указана, то всех.
     *
     * @api
     * @uses   query /api/purchase/list?access-token=q-1
     * #Метод   POST , GET
     *
     * @param string $date_start
     *
     * @return array
     */
    public function actionList($date_start = '')
    {
        $purchasesQuery = Purchase::find()
                                  ->with('service')
                                  ->join('LEFT JOIN', 'service', 'service.id = purchase.service_id')
                                  ->select(' purchase.id,purchase_valid_to,purchase_valid_from,purchase_created_at,service.service_code,service_param')
                                  ->where([ 'user_id' => Yii::$app->user->id ]);
        if (array_key_exists('date_start', $_REQUEST)) {
            $purchasesQuery->andWhere([ 'purchase_created_at' ] >= $_REQUEST['date_start']);
        }
        return [ 'result' => 'ok', 'purchases' => $purchasesQuery->asArray()->all() ];
    }

    /**
     * Покупка услуги за ранее воложеннй в кошелек средства
     *
     * @api
     * @uses   query /api/purchase/on-cash?access-token=q-1&service_id=2
     * #Метод   POST
     * @return array
     * @throws \Exception
     * @throws \yii\db\Exception
     *
     * @param int $service_id
     * @param int $service_param
     * @param int $play_id , необязательный параметр
     */
    public function actionOnCash($service_id = 0, $service_param = 0, $play_id = 0)
    {
        $service_id    = $_REQUEST['service_id'];
        $service_param = $_REQUEST['service_param'];
        $play_id       = 0;
        if (array_key_exists('play_id', $_REQUEST)) {
            $play_id = $_REQUEST['play_id'];
        }
        $service = Service::find()->where([ 'id' => $service_id ])->one();
        if (!$service) {
            return [ 'result' => 'fail', 'message' => 'service not found' ];
        }
        $user = UserX::find()->where([ 'id' => Yii::$app->user->id ])->one();
        if ($user->user_amount < $service->service_amount) {
            return [ 'result' => 'fail', 'message' => 'noy enough money' ];
        }
        $user->user_amount -= $service->service_amount;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $purchase                   = new Purchase();
            $purchase->isNewRecord      = 1;
            $purchase->user_id          = Yii::$app->user->id;
            $purchase->service_id       = $service_id;
            $purchase->service_param    = $service_param;
            $purchase->purchase_amount  = $service->service_amount;
            $purchase->purchase_play_id = $play_id;
            if ($purchase->save() && $user->save()) {
                $transaction->commit();
                return [ 'result' => 'ok' ];
            }
            $transaction->rollBack();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return [ 'result' => 'fail', 'message' => 'db error' ];
    }


    /**
     * Положить в кошелек пользователя средства, купленные на PlayStore
     *
     * @api
     * @uses   query  /api/purchase/cash?access-token=q-1&debit=222
     *
     * @param $debit
     * #Метод   POST
     *
     * @return array
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionCash($debit = 0)
    {
        $debit   = $_REQUEST['debit'];
        $service = Service::find()->where([ 'service_code' => 'add_cash' ])->one();
        if (!$service) {
            return [ 'result' => 'fail', 'message' => 'service not found' ];
        }
        $user = UserX::find()->where([ 'id' => Yii::$app->user->id ])->one();
        $user->user_amount += $debit;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $purchase                   = new Purchase();
            $purchase->user_id          = Yii::$app->user->id;
            $purchase->isNewRecord      = 1;
            $purchase->service_id       = $service->id;
            $purchase->purchase_amount  = $service->service_amount;
            $purchase->purchase_play_id = 0;
            if ($purchase->save() && $user->save()) {
                $transaction->commit();
                return [ 'result' => 'ok' ];
            }
            $transaction->rollBack();
            return [ 'result' => 'fail', $purchase->getErrors(), $user->getErrors() ];

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return [ 'result' => 'fail', 'message' => 'db error' ];
    }

    /**
     * Получить список платных услуг с сервиса
     *
     * @api
     * @uses   query  /api/purchase/services?access-token=1
     *
     * @return array
     */
    public function actionServices()
    {
        $services = Service::find()
                           ->select(' id,service_type,service_play_type,service_play_value,service_code,service_amount,service_amount_save')
                           ->where([ 'service_status' => 'active' ])
                           ->asArray()
                           ->orderBy('service_order')
                           ->all();
        if ($services) {
            return [ 'result' => 'ok', 'services' => $services ];
        }
        return [ 'result' => 'fail', 'message' => 'db error' ];
    }
}