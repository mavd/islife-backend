<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 11.08.2015
 * Time: 1:17
 */

namespace app\controllers\api;

use app\components\Messenger;
use app\models\Play;
use app\models\Purchase;
use app\models\Report;
use app\models\Service;
use app\models\Task;
use app\models\UserX;
use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;
use app\components\PlayUtils;
use yii\imagine\Image;


/**
 * Class PlayController
 * выбор задания, управление статусами игры
 *
 * Доступные методы АПИ и методы доступа
 *
 * 'estimate'      => ['get', 'post'],
 * 'report'        => ['get', 'post'],
 * 'list'         => ['get'],
 * 'details'       => ['get'],
 * 'go'            => ['post', 'get'],
 * 'set-ready'     => ['get'],
 * 'set-started'   => ['get'],
 * 'set-finished'  => ['get'],
 * 'set-reported'  => ['get'],
 * 'set-rejected'  => ['get'],
 * 'set-canceled'  => ['get'],
 * 'search-second' => ['get'],
 * 'send-invite'   => ['post'],
 * 'send-confirm'  => ['post'],
 * 'send-reject'  => ['post'],
 * 'record'=>['post'],
 * 'reject' => ['post'] ,
 * 'send-task-invite'=> ['post']
 * 'send-task-confirm'=> ['post']
 * @package app\controllers\api
 */
class PlayController extends ActiveController
{

    public $modelClass = 'app\models\Play';

    public function actions()
    {
        $actions = parent::actions();
        unset(
            $actions['index'],
            // $actions['view'],
            $actions['create'],
            $actions['update'],
            $actions['delete']
        );
        return $actions;
    }

    public function behaviors()
    {

        //  site?access-token=FFFF70it7tzNsHddEiq0BZ0i-OU8S3xV
        $behaviors                            = parent::behaviors();
        $behaviors['authenticator']['class']  = QueryParamAuth::className();
        $behaviors['authenticator']['except'] = [ 'no' ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'estimate' => [ 'get', 'post' ],
                'report' => [ 'get', 'post' ],
                'view' => [ 'get' ],
                'list' => [ 'get' ],
                'details' => [ 'get' ],
                'go' => [ 'post', 'get' ],
                'set-ready' => [ 'get' ],
                'set-started' => [ 'get' ],
                'set-finished' => [ 'get' ],
                'set-reported' => [ 'get' ],
                'set-rejected' => [ 'get' ],
                'set-canceled' => [ 'get' ],
                'search-second' => [ 'get' ],
                'am-online' => [ 'get' ],
                'send-invite' => [ 'get', 'post' ],   //
                'send-confirm' => [ 'get', 'post' ],  //
                'send-reject' => [ 'get', 'post' ],   //
                'record' => [ 'get', 'post' ],        //
                'reject' => [ 'get', 'post' ],        //
                'send-task-invite' => [ 'get', 'post' ], //
                'send-task-confirm' => [ 'get', 'post' ] //
            ],
        ];


        return $behaviors;
    }

    /**
     * Регистрация завершенной игры.
     *
     * Контроль игры выполнялся на клиенте
     *
     * @api
     * @uses   query  query /api/play/record?access-token=1&task_id=1
     * #Метод   POST
     *
     * @param  int $task_id       ,
     * @param  string $mode       , 'one' or 'pair'             ,required
     * @param  string $time_start , format 'Y-m-d H:i:s'        ,required
     * @param  string $time_end   , format 'Y-m-d H:i:s'        ,required
     * @param  int $user2_id      ,optional
     *
     * @return array
     */
    public function actionRecord(
        $task_id = 0, //Задача
        $mode = '',
        $time_start = '',
        $time_end = '',
        $user2_id = 0
    )
    {
        if (
            !array_key_exists('task_id', $_REQUEST) ||
            !array_key_exists('mode', $_REQUEST) ||
            !array_key_exists('time_start', $_REQUEST) ||
            !array_key_exists('time_end', $_REQUEST)

        ) {
            return [ 'result' => 'fail', 'message' => 'param missing' ];
        }

        $task = Task::find()->where([ 'id' => $_REQUEST['task_id'] ])->one();
        if (!$task) {
            return [ 'result' => 'fail', 'message' => 'task not found' ];
        }

        $play = new Play();

        $play->isNewRecord         = true;
        $play->task_id             = $_REQUEST['task_id'];
        $play->user_id             = Yii::$app->user->id;
        $play->play_status         = 'new';
        $play->play_type           = $task->task_type;
        $play->play_estimate_count = 0;
        $play->play_estimate_sum   = 0;

        //------------------------------------------
        $play->play_type       = $_REQUEST['mode'];
        $play->play_status     = 'finished';
        $play->play_time_start = $_REQUEST['time_start'];
        $play->play_time_end   = $_REQUEST['time_end'];

        if ($play->play_type !== 'one' && $play->play_type !== 'pair') {
            if ($play->play_type === 'both') {
                return [ 'result' => 'fail', 'message' => 'need to choose play type' ];
            }
        }
        if ($play->play_type !== 'one') {
            if (!array_key_exists('user2_id', $_REQUEST)) {
                return [ 'result' => 'fail', 'message' => 'need to select second gamer' ];
            }
            $play->user2_id = $_REQUEST['user2_id'];
        }
        if ($play->play_type === 'one') {
            $play->user2_id = 0;
        }
        $this->searchX2Purchase($play);

        if ($play->save()) {
            return [ 'result' => 'ok', 'play' => $play ];
        } else {
            return [ 'result' => 'fail', 'errors' => $play->getErrors() ];
        }
    }

    /**
     * Регистрация отмененной игры.
     *
     * Контроль игры выполнялся на клиенте
     *
     * @api
     * @uses   query  query /api/play/reject?access-token=1&task_id=1
     * #Метод   POST
     *
     * @param  int $task_id       , required
     * @param  string $mode       , 'one' or 'pair'             ,required
     * @param  string $time_start , format 'Y-m-d H:i:s'        ,required
     * @param  string $time_end   , format 'Y-m-d H:i:s'        ,required
     * @param  int $fine          , required
     * @param  int $user2_id      , optional
     * @param  int $fine2         , optional
     *
     * @return array
     */
    public function actionReject(
        $task_id = 0, //Задача
        $mode = '',
        $time_start = '',
        $time_end = '',
        $fine = 0,
        $user2_id = 0,
        $fine2 = 0
    )
    {
        if (
            !array_key_exists('task_id', $_REQUEST) ||
            !array_key_exists('mode', $_REQUEST) ||
            !array_key_exists('time_start', $_REQUEST) ||
            !array_key_exists('time_end', $_REQUEST)

        ) {
            return [ 'result' => 'fail', 'message' => 'param missing' ];
        }

        $task = Task::find()->where([ 'id' => $_REQUEST['task_id'] ])->one();
        if (!$task) {
            return [ 'result' => 'fail', 'message' => 'task not found' ];
        }

        $play = new Play();

        $play->isNewRecord         = true;
        $play->task_id             = $_REQUEST['task_id'];
        $play->user_id             = Yii::$app->user->id;
        $play->play_status         = 'new';
        $play->play_type           = $task->task_type;
        $play->play_estimate_count = 0;
        $play->play_estimate_sum   = 0;

        //------------------------------------------
        $play->play_type       = $_REQUEST['mode'];
        $play->play_time_start = $_REQUEST['time_start'];
        $play->play_time_end   = $_REQUEST['time_end'];

        if ($play->play_type !== 'one' && $play->play_type !== 'pair') {
            if ($play->play_type === 'both') {
                return [ 'result' => 'fail', 'message' => 'need to choose play type' ];
            }
        }
        if ($play->play_type !== 'one') {
            if (!array_key_exists('user2_id', $_REQUEST)) {
                return [ 'result' => 'fail', 'message' => 'need to select second gamer' ];
            }
            $play->user2_id = $_REQUEST['user2_id'];
        }
        if ($play->play_type === 'one') {
            $play->user2_id = 0;
        }
        $this->searchX2Purchase($play);
        $play->play_status = 'rejected';
        if ($play->save()) {
            $pu = new PlayUtils();
            $pu->saveResult($play);
            return [ 'result' => 'ok', 'play' => $play ];
        } else {
            return [ 'result' => 'fail', 'errors' => $play->getErrors() ];
        }
    }

    /**
     * Регистрация новой игры
     * @api
     *
     * @uses   query  query /api/play/go?access-token=1&task_id=1
     *
     * @param int $task_id
     *
     * @return array
     */
    public function actionGo($task_id = 0)
    {
        if (array_key_exists('task_id', $_REQUEST) &&
            $_REQUEST['task_id']
        ) {
            $task = Task::find()->where([ 'id' => $_REQUEST['task_id'] ])->one();
            $play = new Play();

            $play->isNewRecord         = true;
            $play->task_id             = $_REQUEST['task_id'];
            $play->user_id             = Yii::$app->user->id;
            $play->play_status         = 'new';
            $play->play_type           = $task->task_type;
            $play->play_estimate_count = 0;
            $play->play_estimate_sum   = 0;
            $play->save();
            return [ 'result' => 'ok', 'play' => $play ];
        }
        return [ 'result' => 'fail' ];
    }

    /**
     * Поиск незавершенных, непросроченных игр
     *
     * @api
     *
     * @uses   query  query /api/play/list?access-token=1
     *
     * @return array
     */
    public function actionList()
    {
        $count = Play::find()
                     ->where([ 'or', 'user_id = ' . Yii::$app->user->id, 'user2_id = ' . Yii::$app->user->id ])
                     ->andWhere(
                         "((play_status in  ('ready','new','invited'))
                 OR
                (  play_status in ('started','reported') AND
                   play_time_end<='" . date('Y-m-d H:i:s') . "'
                ))"
                     )
                     ->count();
        if ($count === "0") {
            return [ 'result' => 'ok', 'plays' => [ ] ];
        }
        $plays = Play::find()
                     ->where([ 'or', 'user_id = ' . Yii::$app->user->id, 'user2_id = ' . Yii::$app->user->id ])
                     ->andWhere(
                         "((play_status in  ('ready','new','invited'))
                 OR
                (  play_status in ('started','reported') AND
                   play_time_end<='" . date('Y-m-d H:i:s') . "'
                ))"
                     )
                     ->select('id')
                     ->asArray()
                     ->all();
        if ($plays) {
            return [ 'result' => 'ok', 'plays' => $plays ];
        }
        return [ 'result' => 'fail', 'message' => 'db error' ];

    }

    /**
     * Запрос деталей игры по Id
     *
     * @api
     * @uses   query  /api/play/details?access-token=1&play_id=2
     *
     * @param int $play_id
     *
     * @return array  [id,task_id,user_id,user2_id,play_time_start,play_time_end,play_status,play_type,play_purchase_id,play_purchase2_id,play_estimate_sum,play_estimate_count,play_created_at]
     */
    public
    function actionDetails($play_id)
    {
        $play = Play::find()
                    ->select('id,task_id,user_id,user2_id,play_time_start,play_time_end,play_status,play_type,play_purchase_id,play_purchase2_id,play_estimate_sum,play_estimate_count,play_created_at')
                    ->where([ 'id' => $play_id ])
                    ->one();
        if (!$play) {
            return [ 'result' => 'fail', 'message' => 'not found' ];
        }
        if (
            $play->user_id === Yii::$app->user->id ||
            $play->user2_id === Yii::$app->user->id
        ) {
            return [ $play ];
        }
        return [ 'result' => 'fail', 'message' => 'access denied' ];
    }


    /**
     * Устанавливает статус игры в Ready
     *
     * @api
     * @uses   query  /api/play/set-ready?access-token=1&play_id=2
     *
     * param $play_id integer
     *
     * @return array
     */
    public
    function actionSetReady()
    {
        $play_id = $_REQUEST['play_id'];
        $play    = Play::find()
                       ->where([ 'or', 'user_id = ' . Yii::$app->user->id, 'user2_id = ' . Yii::$app->user->id ])
                       ->andWhere([ 'id' => $play_id ])
                       ->one();
        if (!$play) {
            return [ 'result' => 'fail', 'message' => 'not found' ];
        }
        if ($play->play_status === 'new' || $play->play_status === 'invited') {
            $play->play_status = 'ready';
            if ($play->save()) {
                return [ 'result' => 'ok' ];
            }
            return [ 'result' => 'fail', 'message' => 'db error' ];
        }
        return [ 'result' => 'fail', 'message' => 'current status is ' . $play->play_status ];
    }



    /**
     * Поиск действующей или новой записи о бонусе по времени,
     * если нет,
     * поиск бонуса  на одну игру и отметка в коде бонуса, на какую игру истрачен
     * То же самое для вторго игрока, если игра парная.
     *
     * изменяет переданный по ссылке объект Play
     *
     * @param Play $play
     */
    private function searchX2Purchase(Play $play)
    {
        $p = $this->searchX2PurchaseByUser(
            $play->user_id,
            $play->play_type,
            $play);
        if ($p) {
            $play->play_purchase_id = $p->id;
        }
        if ($play->play_type === 'both') {
            $p = $this->searchX2PurchaseByUser(
                $play->user2_id,
                $play->play_type,
                $play);
            if ($p) {
                $play->play_purchase2_id = $p->id;
            }
        }
    }

    /**
     * Поиск X2 для игрока
     *
     * @param $user_id
     * @param $play_type
     * @param Play $play
     *
     * @return Purchase|array|bool|null
     */
    private function searchX2PurchaseByUser($user_id, $play_type, $play)
    {
        //поиск действующего безлимита
        $qService = Service::find()
                           ->select('id')
                           ->where([ 'service_type' => $play_type,
                                     'service_code' => 'X2' ])
                           ->andWhere([ '!=', 'service_play_type', 'action' ]);
        $p        = Purchase::find()
                            ->where([
                                'user_id' => $user_id,
                                'purchase_status' => 'active',

                            ])
                            ->andWhere([ 'in', 'service_id', $qService ])
                            ->andWhere([ '>=', 'purchase_valid_to', $play->play_time_start ])->one();
        if ($p) {
            return $p;
        }
        //поиск и активизация нового безлимита
        $p = Purchase::find()
                     ->with('service')
                     ->where([
                         'user_id' => $user_id,
                         'purchase_status' => 'new',
                     ])
                     ->andWhere([ 'in', 'service_id', $qService ])->one();

        if ($p) {
            $p->purchase_status     = 'active';
            $p->purchase_valid_from = $play->play_time_start;
            if ($p->service->service_play_type === 'hours') {
                $p->purchase_valid_to = Utils::dateAddHours(
                    $play->play_time_start, $p->service->service_play_value
                );
            } else {
                $p->purchase_valid_to = Utils::dateAddHours(
                    $play->play_time_start, $p->service->service_play_value * 24, 'Y-m-d 00:00:00'
                );
            }
            $p->save();
            return $p;
        }
        //поиск x2 на одну игру
        //поиск действующего безлимита
        $qService = Service::find()
                           ->select('id')
                           ->where([ 'service_type' => $play_type,
                                     'service_code' => 'X2',
                                     'service_play_type' => 'action' ]);
        $p        = Purchase::find()
                            ->with('service')
                            ->where([
                                'user_id' => $user_id,
                                'purchase_status' => 'new',
                            ])
                            ->andWhere([ 'in', 'service_id', $qService ])->one();
        if ($p) {
            $p->purchase_status  = 'used';
            $p->purchase_play_id = $play->id;
            $p->save();
            return $p;
        }
        return false;
    }

    /**
     *  Устанавливает статус игры в Rejected
     *
     * @api
     * @uses   query  /api/play/set-rejected?access-token=1&play_id=2
     *
     * @param int $play_id
     *
     *
     * @return array
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionSetRejected($play_id = 0)
    {

        $play = Play::find()
                    ->where([ 'user_id' => Yii::$app->user->id ])
                    ->andWhere([ 'id' => $play_id ])
                    ->one();
        if (!$play) {
            return [ 'result' => 'fail', 'message' => 'not found' ];
        }
        if ($play->play_status === 'started' || $play->play_status === 'finished') {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $play->play_status = 'rejected';
                if ($play->save()) {

                    $pu = new PlayUtils();
                    $pu->saveResult($play);
                    $transaction->commit();
                    return [ 'result' => 'ok' ];
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;//TODO remove from production
            }
            return [ 'result' => 'fail', 'message' => 'db error' ];
        }
        return [ 'result' => 'fail', 'message' => 'current status is ' . $play->play_status ];
    }

    /**
     * Устанавливает статус игры в Canceled
     *
     * @api
     * @uses   query  /api/play/set-cancel?access-token=1&play_id=2
     *
     * @param int $play_id
     *
     * @return array
     */
    public
    function actionSetCanceled($play_id)
    {

        $play = Play::find()
                    ->where([ 'user_id' => Yii::$app->user->id ])
                    ->andWhere([ 'id' => $play_id ])
                    ->one();
        if (!$play) {
            return [ 'result' => 'fail', 'message' => 'not found' ];
        }
        if ($play->play_status !== 'rejected' && $play->play_status !== 'done') {
            $play->play_status = 'cancel';
            if ($play->save()) {
                $pu = new PlayUtils();
                $pu->saveResult($play);
                return [ 'result' => 'ok' ];
            }
            return [ 'result' => 'fail', 'message' => 'db error' ];
        }
        return [ 'result' => 'fail', 'message' => 'current status is ' . $play->play_status ];
    }


    /**
     * Устанавливает статус игры в Reported
     *
     * @api
     * @uses   query  /api/play/set-reported?access-token=1&play_id=2
     *
     * @param int $play_id
     *
     * @return array
     */
    public
    function actionSetReported($play_id)
    {
        $play = Play::find()
                    ->where([ 'user_id' => Yii::$app->user->id ])
                    ->andWhere([ 'id' => $play_id ])
                    ->one();
        if (!$play) {
            return [ 'result' => 'fail', 'message' => 'not found' ];
        }
        if ($play->play_status === 'finished') {
            if ($play->play_time_end > time()) {
                return [ 'result' => 'fail', 'message' => 'time is out' ];
            }
            $play->play_status = 'reported';
            if ($play->save()) {
                return [ 'result' => 'ok' ];
            }
            return [ 'result' => 'fail', 'message' => 'db error' ];
        }
        return [ 'result' => 'fail', 'message' => 'current status is ' . $play->play_status ];
    }

    /**
     * Устанавливает статус игры в Finished
     *
     * @api
     * @uses   query  /api/play/set-finished?access-token=1&play_id=2
     *
     * @param int $play_id
     *
     * @return array
     */
    public function actionSetFinished($play_id)
    {
        $play = Play::find()
                    ->where([ 'user_id' => Yii::$app->user->id ])
                    ->andWhere([ 'id' => $play_id ])
                    ->one();
        if (!$play) {
            return [ 'result' => 'fail', 'message' => 'not found' ];
        }
        if ($play->play_status === 'started') {

            if ($play->play_time_end < date('Y-m-d H:i:s')) {
                return [ 'result' => 'fail', 'message' => 'timeout' ];
            }
            $play->play_status = 'finished';
            if ($play->save()) {
                return [ 'result' => 'ok' ];
            }
            return [ 'result' => 'fail', 'message' => 'db error' ];
        }
        return [ 'result' => 'fail', 'message' => 'current status is ' . $play->play_status ];
    }

    /**
     * Добавляет фото в отчет
     *
     * @api
     * @uses   query  /api/play/report?access-token=1
     *
     * Параметры пердавать через пост запрос     *
     *
     * @param int $play_id
     * param file $file
     *
     * @return array
     */
    public
    function actionReport($play_id = 0)
    {
        $path    = '/web/uploads/tests/';
        $dir     = $_SERVER['DOCUMENT_ROOT'] . $path;
        $play_id = $_REQUEST['play_id'];
        $play    = Play::find()
                       ->where([ 'user_id' => Yii::$app->user->id ])
                       ->andWhere([ 'id' => $play_id ])
                       ->one();
        if (!$play) {
            return [ 'result' => 'fail', 'message' => 'not found' ];
        }
        if ($play->play_status === 'finished') {
            if ($_FILES) {
                if (array_key_exists('file', $_FILES)) {
                    Yii::info('REPORT FILES:: ' . print_r($_FILES, 1), 'api');
                    if (!file_exists($dir . '/play/' . $play->id)) {
                        mkdir($dir . '/play/' . $play->id . '/thumb', 0777, true);
                    }
                    $file  = 'play/' . $play->id . '/' . $_FILES['file']['name'];
                    $thumb = 'play/' . $play->id . '/thumb/' . $_FILES['file']['name'];
                    Yii::info('REPORT FILE:: ' . $dir . $path . $file, 'api');
                    move_uploaded_file($_FILES['file']['tmp_name'], $dir . $file);

                    $img = Image::getImagine()->open(Yii::getAlias($dir . $file));

                    $size  = $img->getSize();
                    $ratio = $size->getWidth() / $size->getHeight();

                    $width  = 200;
                    $height = round($width / $ratio);

                    $box = new \imagine\Image\Box($width, $height);
                    $img->resize($box)->save($dir . $thumb);

                    $report               = new Report();
                    $report->isNewRecord  = true;
                    $report->user_id      = $play->user_id;
                    $report->play_id      = $play->id;
                    $report->report_photo = $path . $file;
                    $report->report_thumb = $path . $thumb;


                    if ($report->save()) {
                        return [ 'result' => 'ok' ];
                    } else {
                        return [
                            'result' => 'fail',
                            'message' => 'db error',
                            'error' => $report->getErrors()
                        ];
                    }
                }

            }
            return [ 'result' => 'fail', 'message' => 'attach file not found' ];
        }
        return [ 'result' => 'fail', 'message' => 'current status is ' . $play->play_status ];
    }

    /**
     * Поиск второго участника игры, если успешен, то модель потенциального пратнера
     *
     * #api - GET
     * @uses   /api/play/search-second?access-token=1
     *
     * @param string $gender 'M' or 'F'
     *
     * @return UserX|array|null
     */
    public function actionSearchSecond($gender = '')     //TODO сделать Task версию
    {
        $user  = UserX::find()->where([ 'id' => Yii::$app->user->id ])->one();
        $query = UserX::find()
                      ->where([ '!=', 'id', Yii::$app->user->id ])
                      ->andWhere([ 'user_isAdmin' => 0 ])
                      ->andWhere([ '!=', 'user_status', 'blocked' ])
                      ->andWhere([ 'user_city' => $user->user_city ])
                      ->select('id');
        if ($gender) {
            $query->andWhere([ 'user_gender' => $gender ]);
        }
        $query->limit(1)
              ->orderBy('RAND()');
        $id = $query->scalar();
        if ($id) {
            $user = UserX::find()
                         ->where([ 'id' => $id ])
                         ->select('id,username,user_gender,user_birthday,user_photo,user_city,user_path,user_status,user_health,user_determination,user_communication,user_skill')
                         ->asArray()
                         ->one();
            return [ 'result' => 'ok', 'user' => $user ];
        }
        return [ 'result' => 'fail', 'message' => 'not found' ];
    }

    /**
     * Поиск второго участника игры, если успешен,
     * то  потенциальному пратнеру посылаем пуш сообщение
     * {message:areOnline, user_id:123}
     *  Если все получилось, то вернем result:ok
     *
     * #api - GET
     *
     * @uses    /api/play/find-second?access-token=1&lat=1233&long=98.321
     *
     * @param string $gender 'M' or 'F'
     * @param int $lat
     * @param int $long
     *
     * @return array
     */
    public function actionFindSecond($gender = '', $lat = 0, $long = 0)     //TODO сделать Task версию
    {
        $user  = UserX::find()->where([ 'id' => Yii::$app->user->id ])->one();
        //var_dump($user);exit;
        $query = UserX::find()
                      ->where([ '!=', 'id', Yii::$app->user->id ])
                      ->andWhere([ 'user_isAdmin' => 0 ])
                      ->andWhere([ '!=', 'user_status', 'blocked' ])
                      ->andWhere([ 'user_city' => $user->user_city ])
                      ->select('id');
        if ($gender) {
            $query->andWhere([ 'user_gender' => $gender ]);
        }
        $query->limit(5)
              ->orderBy('RAND()');
        $ids = $query->all();
        //$id=$user->id;
        if ($ids) {
            foreach($ids as $idr){
                $id=$idr['id'];
                $data = [
                    "message" => "areOnline",
                    "user_id" => YII::$app->user->id
                ];
                if (!array_key_exists('lat', $_REQUEST) || !array_key_exists('lat', $_REQUEST)) {
                    return [ 'result' => 'fail', 'message' => 'coordinates are missing' ];
                }
                $user->user_lat  = $_REQUEST['lat'];
                $user->user_long = $_REQUEST['long'];
                if ($user->save()) {
                    $messenger = new Messenger($id);
                    $messenger->sendPush(json_encode($data));

                    if ($id) {
                        continue;
                    }
                } else {
                    return [ 'result' => 'fail', 'message' => 'db error', 'error' => $user->getErrors() ];
                }
            }
            return [ 'result' => 'ok' ];
        }
        return [ 'result' => 'fail', 'message' => 'not found' ];
    }

    /**
     * Метод вызывается с устройства игрока , в ответ на пуш  "areOnline",
     * если игрок\устройство технически готовы принять приглашение на участие в парной игре      *
     * Система посылает пуш сообщение  isOnline, инициатору игры
     * Формат сообщения
     * {"message":"isOnline", "user_id":234,
     *      "midpoint":{"lat":1223, "long":345}}
     *
     * #api - GET
     *
     * @uses    /api/play/am-online?access-token=1&user_id=1&lat=1233&long=98.321
     *
     *
     * @param int $user_id
     * @param int $lat
     * @param int $long
     *
     * @return array
     */
    public function actionAmOnline($user_id = 0, $lat = 0, $long = 0)
    {
        Yii::info($user_id, 'api');
        $data  = [
            'message' => 'isOnline',
            'user_id' => Yii::$app->user->id
        ];
        $user1 = UserX::find()->where([ 'id' => $_REQUEST['user_id'] ])->one();
        //var_dump($user1);exit;
        if ($user1) {

            if (!array_key_exists('lat', $_REQUEST) || !array_key_exists('lat', $_REQUEST)) {
                return [ 'result' => 'fail', 'message' => 'coordinates are missing' ];

            }
            $data['midpoint'] = [
                'lat' => ($user1->user_lat + $_REQUEST['lat']) / 2.0,
                'long' => ($user1->user_long + $_REQUEST['long']) / 2.0,
            ];

            $messenger = new Messenger($_REQUEST['user_id']);
            $messenger->sendPush(json_encode($data));

            return [ 'result' => 'ok' ];
        }

        return [ 'result' => 'fail', 'message' => 'user not found' ];

    }

    /**
     * Посылка предложение присоединится к игре
     *
     * Перевод игры в состояние invited
     *
     * @api
     * @uses   query  /api/play/send-invite?access-token=1
     *
     * @param int $play_id
     * @param int $user_id
     * #Метод   POST, PUT
     *
     * @return array
     */
    public function actionSendInvite($play_id = 0, $user_id = 0)
    {
        $play_id = $_REQUEST['play_id'];
        $user_id = $_REQUEST['user_id'];
        $play    = Play::find()
                       ->where([ 'user_id' => Yii::$app->user->id ])
                       ->andWhere([ 'id' => $play_id ])
                       ->one();
        if (!$play) {
            return [ 'result' => 'fail', 'message' => 'play not found' ];
        }
        if ($play->play_status === 'new' || $play->play_status === 'invited') {


            $play->play_status = 'invited';
            $play->user2_id    = $user_id;
            $messenger         = new Messenger();

            $messenger->sendByTemplate(
                $user_id,
                'pushInvite',
                [ 'play' => $play, 'user' => UserX::find()->me()->player()->one() ]
            );
            $messenger->sendByTemplate(
                $user_id,
                'informInvite',
                [ 'play' => $play, 'user' => UserX::find()->me()->player()->one() ]
            );

            if ($play->save()) {
                return [ 'result' => 'ok' ];
            }
            return [ 'result' => 'fail', 'message' => 'db error' ];
        }
        return [ 'result' => 'fail', 'message' => 'current status is ' . $play->play_status ];
    }

    /**
     * Послать приглашение игроку, о совместной игре
     * метод должен вызывать инициатор парной игры.
     * Система посылает пуш сообщение игроку в формате
     * {"message":"invite", "user1":123,"task":234,
     *      "midpoint":{"lat":1223, "long":345}
     *       }
     *
     * @api    V2
     *
     * @uses   query  /api/play/send-task-invite?access-token=1&task=222&lat=123&long=23
     *
     * #Метод   POST , (GET)
     *
     * @param int $task_id
     * @param int $user_id , вызываемй игрок
     * @param int $lat
     * @param int $long
     *
     * @return array
     */
    public function actionSendTaskInvite($task_id = 0, $user_id = 0, $lat = 0, $long = 0)
    {
        $task_id = $_REQUEST['task_id'];
        $user_id = $_REQUEST['user_id'];
        $task    = Task::find()
                       ->Where([ 'id' => $task_id ])
                       ->one();
        if (!$task) {
            return [ 'result' => 'fail', 'message' => 'task not found' ];
        }
        $point1 = 0;
        if (array_key_exists('lat', $_REQUEST) && array_key_exists('lat', $_REQUEST)) {
            $midpoint = [ "lat" => $_REQUEST['lat'],
                          "long" => $_REQUEST['long'] ];
        } else {
            return [ 'result' => 'fail', 'message' => 'coordinates are missing' ];
        }
        $messenger = new Messenger($user_id);
        $data      = [
            'message' => 'invite',
            'user1' => YII::$app->user->id,
            'task' => $task_id,
            'midpoint' => $midpoint
        ];


        $messenger->sendPush(json_encode($data));

        $messenger->sendByTemplate(
            $user_id,
            'informInvite',
            [ 'task' => $task, 'user' => UserX::find()->me()->player()->one(),
              'midpoint' => $midpoint
            ]
        );
        return [ 'result' => 'ok' ];
    }

    /**
     * Посылка согласия присоединится к игре
     * Перевод игры в состояние ready
     *
     * @api
     * @uses   query  /api/play/send-confirm?access-token=1&play_id=2
     *
     * @param int $play_id
     * #Метод   POST, PUT
     *
     * @return array
     */
    public function actionSendConfirm($play_id = 0)
    {
        $play_id = $_REQUEST['play_id'];
        $play    = Play::find()
                       ->where([ 'user2_id' => Yii::$app->user->id ])
                       ->andWhere([ 'id' => $play_id ])
                       ->one();
        if (!$play) {
            return [ 'result' => 'fail', 'message' => 'not found' ];
        }
        if ($play->play_status === 'invited') {
            $play->play_status = 'ready';
            $messenger         = new Messenger();

            $messenger->sendByTemplate(
                $play->user_id,
                'confirmPush',
                [ 'play' => $play, 'user' => Yii::$app->user ]
            );
            $messenger->sendByTemplate(
                $play->user_id,
                'confirmInform',
                [ 'play' => $play, 'user' => Yii::$app->user ]
            );

            if ($play->save()) {
                return [ 'result' => 'ok' ];
            }
            return [ 'result' => 'fail', 'message' => 'db error' ];
        }
        return [ 'result' => 'fail', 'message' => 'current status is ' . $play->play_status ];
    }

    /**
     * Посылка согласия присоединится к игре
     * Перевод игры в состояние ready
     *
     * #Метод   POST, GET
     * @api    V2
     * @uses   query  /api/play/send-task-confirm?access-token=1
     *
     * @param int $user_id
     * @param int $task_id
     *
     * @return array
     */


    public function actionSendTaskConfirm($user_id = 0, $task_id = 0)
    {
        $task_id = $_REQUEST['task_id'];
        $task    = Task::find()
                       ->where([ 'id' => $task_id ])
                       ->one();
        if (!$task) {
            return [ 'result' => 'fail', 'message' => 'not found' ];
        }

        $messenger = new Messenger($user_id);
        $data      = [
            'message' => 'confirm',
            'user1' => $user_id,
            'user2' => YII::$app->user->id,

        ];
        $messenger->sendPush(json_encode($data));
        $messenger->sendByTemplate(
            $user_id,
            'confirmInform',
            [ 'task' => $task, 'user' => Yii::$app->user ]
        );
        return [ 'result' => 'ok' ];
    }

    /**
     * Посылка отказа присоединится к игре     *
     * Перевод игры в состояние new
     *
     * @api
     * @uses   query  /api/play/send-task-reject?access-token=1&user_id=212&task_id=23
     *
     * @param int $user_id
     * @param int $task_id
     *
     * @return array
     */
    public function actionSendTaskReject($user_id = 0, $task_id = 0)
    {

        $messenger = new Messenger($user_id);
        $data      = [
            'message' => 'reject',
            'task_id' =>   $task_id,
            'user1' => $user_id,
            'user2' => YII::$app->user->id,

        ];
        $messenger->sendPush(json_encode($data));
        $messenger->sendByTemplate(
            $user_id,
            'rejectPush',
            [ 'task_id' => $task_id ]
        );
        return [ 'result' => 'ok' ];

    }
}
