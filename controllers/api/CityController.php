<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 11.08.2015
 * Time: 1:17
 */

namespace app\controllers\api;

use app\models\Cities;
use app\models\Regions;
use Yii;
use yii\base\Exception;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;


/**
 * Class Cityontroller
 * Изменить данные об устройсте пользователя
 *
 * Доступные методы АПИ и методы доступа
 * 'regions'  => ['get'],
 * 'list'=> ['get'],
 *
 * @package app\controllers\api
 */
class CityController extends ActiveController
{

    public $modelClass = 'app\models\Device';

    public function behaviors()
    {
        $behaviors                            = parent::behaviors();
        $behaviors['authenticator']['class']  = QueryParamAuth::className();
        $behaviors['authenticator']['except'] = [ ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'regions' => [ 'get' ],
                'list' => [ 'get' ],
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],
            $actions['create'],
            $actions['view'],
            $actions['update'],
            $actions['delete']);
        return $actions;
    }

    /**
     * список регионов
     * #api - GET
     * @uses /api/city/regions?access-token=q-1
     * @return array
     */
    public function actionRegions()
    {
        $regions = Regions::find()->asArray()->all();
        return [ 'result' => 'ok', 'regions' => $regions ];
    }

    /**
     * список городов в регионе
     * #api - GET
     * @uses /api/city/list?access-token=q-1&region_id=75
     * @param $region_id
     *
     * @return array
     */
    public function actionList($region_id)
    {
        $cities = Cities::find()
            ->select('city,state')
           ->where([ 'region_id' => $region_id ])->asArray()->all();
        return [ 'result' => 'ok', 'cities' => $cities ];
    }
}
