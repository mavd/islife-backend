<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 11.08.2015
 * Time: 1:17
 */

namespace app\controllers\api;

use yii\db\Query;
use app\models\base\Play;
use app\models\Device;
use app\models\LoginLog;
use app\models\Message;
use app\models\Purchase;
use app\models\RatingLog;
use app\models\User;
use app\models\UserX;
use dektrium\user\models\LoginForm;
use dektrium\user\models\RecoveryForm;
use dektrium\user\models\ResendForm;
use dektrium\user\traits\AjaxValidationTrait;
use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;
use yii\helpers\Utils;
use app\models\SkillLevel;
use app\components\Messenger;

//use dektrium\user\models\User;
/**
 * Class UserXController
 * Регистрация, Логин, восстановления пароля через API
 *
 * Доступные методы АПИ и методы доступа
 * 'index'    => ['get'],
 * 'view'     => ['get'],
 * 'create'   => ['get', 'post'],
 * 'put-cash'   => ['get', 'post'],
 * 'register' => ['post'],
 * 'login'    => ['get', 'post'],
 * 'recovery' => ['get', 'post'],
 * 'check'    => ['get', 'post'],
 * 'update'   => ['get', 'put', 'post'],
 * 'delete'   => ['post', 'delete'],
 * 'profile'  => ['get', 'post'],
 * 'rate-list'  => ['get'],
 * 'rate-hist'  => ['get'],
 * 'play-reports'  => ['get'],
 * 'notifications' =>['get']
 * 'send-inform'   =>['get','post']
 * 'send-push'   =>['get','post']
 *
 * @package app\controllers\api
 */
class UserXController extends ActiveController
{

    use AjaxValidationTrait;

    public $modelClass = 'app\models\UserX';

    public function behaviors()
    {

        $behaviors                            = parent::behaviors();
        $behaviors['authenticator']['class']  = QueryParamAuth::className();
        $behaviors['authenticator']['except'] = [ 'login', 'register', 'check', 'recovery', 'resend' ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => [ 'get' ],
                'view' => [ 'get' ],
                'create' => [ 'get', 'post' ],
                'put-cash' => [ 'get', 'post' ],
                'register' => [ 'post' ],
                'login' => [ 'get', 'post' ],
                'recovery' => [ 'get', 'post' ],
                'check' => [ 'get', 'post' ],
                'update' => [ 'get', 'put', 'post' ],
                'delete' => [ 'post', 'delete' ],
                'profile' => [ 'get', 'post' ],
                'rate-list' => [ 'get' ],
                'rate-hist' => [ 'get' ],
                'play-reports' => [ 'get' ],
                'notifications' => [ 'get' ],
                'send-inform' => [ 'get', 'post' ],
                'send-push' => [ 'get', 'post' ],
                'find-users' => [ 'get' ],
                'view-profile' => [ 'get' ],
                'play-reports-city' => [ 'get' ]
            ],
        ];
        return $behaviors;
    }

    /**
     * Запрет обработки стандартных запросов
     *
     * @return array
     */
    public function actions()
    {
        Yii::info("API", 'api');
        $actions = parent::actions();


        // customize the data provider preparation with the "prepareDataProvider()" method

        unset($actions['index'], $actions['create'], $actions['update'], $actions['delete']);
        return $actions;
    }

    /**
     * Пополнить кошелек пользователя, рубли
     * ##api - POST or PUT #
     *
     * @uses   /api/user-x/put-cash?access-token=1
     *
     * @param  $amount
     *
     * @return array
     */
    public function actionPutCash($amount = 0)
    {
        if (array_key_exists('amount', $_REQUEST)) {
            $user = UserX::find()->where([ 'id' => Yii::$app->user->id ])->one();
            $user->user_amount += $_REQUEST['amount'] * 3;
            if ($user->save()) {
                return [ 'result' => 'ok' ];
            } else {
                return [ 'result' => 'fail', 'message' => 'db error' ];
            }
        }
        return [ 'result' => 'fail', 'message' => 'param is missing' ];
    }

    /**
     * Регистрация устройств
     *
     * @param \stdClass $device параметры устройства полученные из JSON данных присданных с клиента
     * @param UserX $user
     *
     * @return Device|array|bool|null
     */
    protected function regDevice($device, UserX $user)
    {
        $dev = Device::find()->byUserAndApi($user->id, $device->api_id)->one();
        if (!$dev) {
            $dev                    = new Device();
            $dev->isNewRecord       = 1;
            $dev->user_id           = $user->id;
            $dev->device_api_id     = $device->api_id;
            $dev->device_created_at = date('Y-m-d H:i:s');
        }
        $dev->device_name                = $device->name;
        $dev->device_os                  = $device->os;
        $dev->device_access_token        = \Yii::$app->security->generateRandomString();
        $dev->device_access_token_expire = date('Y-m-d H:i:s', time() + 4 * 3600);
        $dev->device_last_login_at       = date('Y-m-d H:i:s');
        if ($dev->save()) {
            return $dev;
        }

        return false;
    }

    /**
     * Логин
     * ##api - POST
     *
     * @uses  /api/user-x/login?access-token=1
     *
     * @param string $email
     *
     * @param string $password
     *
     * @param string $device
     *
     * @return array
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogin($email = '', $password = '', $device = '')
    {
        $model = \Yii::createObject(LoginForm::className());
        //if (array_key_exists('login', $_REQUEST)) {
        //    $model->login = filter_var($_REQUEST['login'], FILTER_SANITIZE_STRING);
        //}
        if (array_key_exists('email', $_REQUEST)) {
            $model->login = filter_var($_REQUEST['email'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('password', $_REQUEST)) {
            $model->password = filter_var($_REQUEST['password'], FILTER_SANITIZE_STRING);
        }
        if (!array_key_exists('device', $_REQUEST)) {
            return [ 'result' => 'fail', 'message' => 'device is missing' ];
        }

        $device = json_decode($_REQUEST['device']);
        Yii::info("LOGIN DEVICE " . print_r($device, 1), 'api');
        if ($model->login()) {

            $user = UserX::find()
                         ->andWhere([ 'email' => filter_var($_REQUEST['email'], FILTER_SANITIZE_STRING) ])
                         ->one();

            if (!$user) {

                return [ 'result' => 'fail', 'message' => 'device db error :: 001' ];
            }
            $deviceModel = $this->regDevice($device, $user);
            if (!$deviceModel) {

                return [ 'result' => 'fail', 'message' => 'device db error :: 002',
                         'device' => $device
                ];
            }

            $user->user_last_device_id = $deviceModel->id;
            $user->save();
            $loginLog                   = new LoginLog();
            $loginLog->isNewRecord      = 1;
            $loginLog->user_id          = $user->id;
            $loginLog->device_id        = $deviceModel->id;
            $loginLog->login_created_at = date('Y-m-d H:i:s');
            if (!$loginLog->save()) {

                return [ 'result' => 'fail', 'message' => 'login log db error' ];
            }
            return [
                'result' => 'ok',
                'access_token' => $deviceModel->device_access_token,
                'user_id' => $user->id
            ];
        }

        return [ 'result' => 'fail', 'message' => Utils::implodeRec($model->errors) ];

    }

    /**
     * @param User $model
     *
     * @return int
     */
    private function photoSave(User $model)
    {
        $path = '/web/uploads/tests/';
        $dir  = $_SERVER['DOCUMENT_ROOT'] . $path;
        if ($_FILES) {
            if (array_key_exists('file', $_FILES)) {
                Yii::info('FILES:: ' . print_r($_FILES, 1), 'api');
                if (!file_exists($dir . 'users/' . $model->id)) {
                    mkdir($dir . 'users/' . $model->id, 0777, true);
                }
                $file = 'users/' . $model->id . '/' . $_FILES['file']['name'];
                Yii::info('FILE:: ' . $dir . $path . $file, 'api');
                move_uploaded_file($_FILES['file']['tmp_name'], $dir . $file);
                $model->scenario   = 'settings';
                $model->user_photo = $path . $file;
                $model->save();
                return 1;
            }
        }
        return 0;
    }

    /**
     * Ищет id игроков по части имени
     * #api - GET
     *
     * @link   http://islife.mobissa.ru/api/user-x/find-users?access-token=q-1&user_name=я
     *
     * @param string $user_name
     *
     * @return array
     */
    public function actionFindUsers($user_name)
    {
        $users = UserX::find()
                      ->select('id')
                      ->where([ 'like', 'user_name', $user_name ])
                      ->andWhere([ 'user_isAdmin' => 0 ])
                      ->andWhere([ '!=', 'user_status', 'blocked' ])
                      ->asArray()->all();

        return [ 'result' => 'ok', 'users' => $users ];
    }

    /**
     * Возвращает профиль пользователя по id, присоединяет сведения об X2
     * или обновляет профиль игрока  - обрабатывает POST запрос
     * #api -  GET
     *
     *
     * @link   http://islife.mobissa.ru/api/user-x/view-profile?access-token=q-1&id=-2
     *
     * @param int $id
     *
     * @return array
     */
    public function actionViewProfile($id = 0)
    {
        $user = UserX::find()
                     ->select(
                         'id,user_name,email,user_photo,user_gender,
                user_birthday,user_city,user_path,user_status,
                user_determination,user_health,user_communication,
                user_skill,user_amount,user_bonuses'
                     )
                     ->where([ 'id' => $id ])
                     ->andWhere([ 'user_isAdmin' => 0 ])
                     ->andWhere([ '!=', 'user_status', 'blocked' ])
                     ->asArray()->one();
        if (!$user) {
            return [ 'result' => 'fail', 'notFound' ];
        }
        $x2Recs = Purchase::find()
                          ->with('service')
                          ->andWhere([ 'user_id' => Yii::$app->user->id, ])
                          ->andWhere([ 'in', 'purchase_status', [ 'new', 'active' ] ])
                          ->asArray()->all();
        $x2     = [ 'one' => false, 'pair' => false ];
        foreach ($x2Recs as $x2Rec) {

            $x2[$x2Rec['service']['service_type']] = 'true';
        }
        $query = new Query;

        $query->select('count(*) as cnt, rate_play_type')
              ->where([ 'user_id' => Yii::$app->user->id ])
              ->groupBy('rate_play_type')
              ->from('rating_log');
        $command                 = $query->createCommand();
        $data                    = $command->queryAll();
        $user['completed_games'] = [ 'one' => 0, 'pair' => 0 ];
        foreach ($data as $data_rec) {
            $user['completed_games'][$data_rec['rate_play_type']] = $data_rec['cnt'];
        }

        $level = SkillLevel::find()
                           ->select('sl_name,sl_level,sl_low_edge,sl_high_edge')
                           ->where([ '<=', 'sl_low_edge', 'user_skill' ])
                           ->andWhere([ '<=', 'sl_low_edge', 'user_skill' ])
                           ->one();
        if (!$level) {
            $level = [ 'sl_name' => '', 'sl_level' => '0', 'sl_low_edge' => 0, 'sl_high_edge' => 0 ];
        }

        return [ 'result' => 'ok', 'profile' => $user, 'level' => $level, 'x2' => $x2 ];
    }

    /**
     * Возвращает профиль текущего пользователя, присоединяет сведения об X2 #Метод GET
     * или обновляет профиль игрока  - обрабатывает #POST запрос
     * #api - POST, GET
     *
     * @return User|array ['result'=>'ok']
     *
     * @uses   query  /api/user-x/profile?access-token=1
     *
     * @return UserX|array|null
     */
    public function actionProfile()
    {
        $model = \Yii::$app->user->identity;
        if (!$model) {
            return [ 'result' => 'fail', 'message' => 'Please relogin' ];
        }
        if (isset($_POST) && count($_POST)) {

            //$this->performAjaxValidation($model);
            if (array_key_exists('user_name', $_REQUEST)) {
                $user_name = filter_var($_REQUEST['user_name'], FILTER_SANITIZE_STRING);
                if ($model->user_name !== $user_name) {
                    $model->user_name = $user_name;
                }
            }
            if (array_key_exists('email', $_REQUEST)) {
                $email = filter_var($_REQUEST['email'], FILTER_SANITIZE_EMAIL);
                if ($model->email !== $email) {
                    $model->email = $email;
                }
            }
            if (array_key_exists('password', $_REQUEST)) {
                $model->password = $_REQUEST['password'];
            }
            if (array_key_exists('user_gender', $_REQUEST)) {
                $model->user_gender = $_REQUEST['user_gender'];
            }
            if (array_key_exists('user_birthday', $_REQUEST)) {
                $model->user_birthday = $_REQUEST['user_birthday'];
            }
            if (array_key_exists('user_city', $_REQUEST)) {
                $model->user_city = $_REQUEST['user_city'];
            }
            if (array_key_exists('user_path', $_REQUEST)) {
                $model->user_path = $_REQUEST['user_path'];
            }
            if (array_key_exists('user_bonuses', $_REQUEST)) {
                $model->user_bonuses = $_REQUEST['user_bonuses'];
            }
            //$model->user_isAdmin = 0;
            $model->scenario = 'settings';
            // return $model;
            if (!$model->validate()) {
                return [ 'result' => 'fail', 'message' => Utils::implodeRec($model->errors),

                ];
            }
            if ($model->save(false)) {
                $this->photoSave($model);
                $response = Yii::$app->getResponse();
                $response->setStatusCode(201);
                return [ 'result' => 'ok' ];
            }

            return [ 'result' => 'fail', 'message' => Utils::implodeRec($model->errors) ];
        } else {
            if ($this->photoSave($model)) {
                $response = Yii::$app->getResponse();
                $response->setStatusCode(201);
                return [ 'result' => 'ok' ];
            }
        }
        $user   = UserX::find()
                       ->select(
                           'id,user_name,email,user_photo,user_gender,
                user_birthday,user_city,user_path,user_status,
                user_determination,user_health,user_communication,
                user_skill,user_amount,user_bonuses'
                       )
                       ->where([ 'id' => Yii::$app->user->id ])->asArray()->one();
        $x2Recs = Purchase::find()
                          ->with('service')
                          ->andWhere([ 'user_id' => Yii::$app->user->id, ])
                          ->andWhere([ 'in', 'purchase_status', [ 'new', 'active' ] ])
                          ->asArray()->all();
        $x2     = [ 'one' => false, 'pair' => false ];
        foreach ($x2Recs as $x2Rec) {

            $x2[$x2Rec['service']['service_type']] = 'true';
        }
        $query = new Query;

        $query->select('count(*) as cnt, rate_play_type')
              ->where([ 'user_id' => Yii::$app->user->id ])
              ->groupBy('rate_play_type')
              ->from('rating_log');
        $command                 = $query->createCommand();
        $data                    = $command->queryAll();
        $user['completed_games'] = [ 'one' => 0, 'pair' => 0 ];
        foreach ($data as $data_rec) {
            $user['completed_games'][$data_rec['rate_play_type']] = $data_rec['cnt'];
        }

        $level = SkillLevel::find()
                           ->select('sl_name,sl_level,sl_low_edge,sl_high_edge')
                           ->where([ '<=', 'sl_low_edge', 'user_skill' ])
                           ->andWhere([ '<=', 'sl_low_edge', 'user_skill' ])
                           ->one();
        if (!$level) {
            $level = [ 'sl_name' => '', 'sl_level' => '0', 'sl_low_edge' => 0, 'sl_high_edge' => 0 ];
        }

        return [ 'result' => 'ok', 'profile' => $user, 'level' => $level, 'x2' => $x2 ];

    }

    /**
     * Проверка уникальности пользовательского имени и почты
     * #api - Get, Post
     *
     * @uses   query  /api/user-x/check?access-token=1
     *
     * Поля: username,email
     * @return array
     */
    public function actionCheck()
    {
        $result = [ ];
        if (array_key_exists('username', $_REQUEST)) {
            $cnt = User::find()
                       ->where([ 'username' => filter_var($_REQUEST['username'], FILTER_SANITIZE_STRING) ])
                       ->count();
            if ($cnt) {
                $result[] = [

                    'message' => 'Username «' . filter_var($_REQUEST['username'], FILTER_SANITIZE_STRING) . '» уже занят.'
                ];
            }
        }
        if (array_key_exists('email', $_REQUEST)) {
            $cnt = User::find()
                       ->where([ 'email' => filter_var($_REQUEST['email'], FILTER_SANITIZE_EMAIL) ])
                       ->count();
            if ($cnt) {
                $result[] = [

                    'message' => 'Email «' . filter_var($_REQUEST['email'], FILTER_SANITIZE_EMAIL) . '» уже занят.'
                ];
            }
        }
        if ($result) {
            return [ 'result' => 'fail', 'message' => Utils::implodeRec($result) ];
        }
        return [ 'result' => 'ok' ];
    }

    /**
     * Обработка запроса на восстановление пароля
     * #api - Get,Post
     *
     * @uses   query  /api/user-x/recovery
     *
     * @param  string $email
     *
     * @return array|object
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRecovery($email = '')
    {

        $model = \Yii::createObject([
            'class' => RecoveryForm::className(),
            'scenario' => 'request',
        ]);
        $this->performAjaxValidation($model);

        $model->email = filter_var($_REQUEST['email'], FILTER_SANITIZE_EMAIL);
        if ($model->sendRecoveryMessage()) {
            return [ 'result' => 'ok' ];
        }
        return [ 'result' => 'fail', 'message' => Utils::implodeRec($model->errors) ];

    }

    /**
     * Обработка запроса на отсылку повторно письма для активации аккаунта
     * #api - Get
     *
     * @uses   query  /api/user-x/resend?access-token=1
     *
     * @param  string $email
     *
     * @return array|object
     * @throws \yii\base\InvalidConfigException
     */
    public function actionResend($email = '')
    {

        $model = \Yii::createObject(ResendForm::className());

        $this->performAjaxValidation($model);
        $model->email = filter_var($_REQUEST['email'], FILTER_SANITIZE_EMAIL);
        if ($model->resend()) {
            return [ 'result' => 'ok' ];
        }
        return [ 'result' => 'fail', 'message' => Utils::implodeRec($model->errors) ];

    }


    /**
     * Регистрация нового игрока
     * #api - Post
     *
     * метод обрабатывает POST запрос на регистрацию
     * Обязательные поля: email,password
     * Необязательные поля: user_photo, user_gender, user_birthday, user_city, user_path
     * Если регистрация завершена успешно, то игроку отправляеи email на подтверждение адреса, и возвращает
     * ['result'=>'ok'] В случае неудачи возвращает список ошибок ,
     *  "[{"field":"username","message":"Необходимо заполнить «Username»."}]"
     *
     * @uses   query  /api/user-x/register?access-token=1
     * @uses   query  $I->sendPOST('user-x/register',  [
     *                   'username' => 'test5',
     *                   'email'     => 'test5@codeception.com',
     *                   'password' => 'qqqq123',
     *                   ],
     *                   ['file'=>$file]
     *           );
     *
     * @return User|array ['result'=>'ok']
     */
    public function actionRegister()
    {
        $model = new User;
        $path  = '/web/uploads/tests/';
        $dir   = $_SERVER['DOCUMENT_ROOT'] . $path;
        //$this->performAjaxValidation($model);
        $model->username = filter_var($_REQUEST['username'], FILTER_SANITIZE_STRING);
        $model->email    = filter_var($_REQUEST['email'], FILTER_SANITIZE_EMAIL);
        $model->password = $_REQUEST['password'];
        if (array_key_exists('user_gender', $_REQUEST)) {
            $model->user_gender = $_REQUEST['user_gender'];
        }
        if (array_key_exists('user_birthday', $_REQUEST)) {
            $model->user_birthday = $_REQUEST['user_birthday'];
        }
        if (array_key_exists('user_city', $_REQUEST)) {
            $model->user_city = $_REQUEST['user_city'];
        }
        if (array_key_exists('user_path', $_REQUEST)) {
            $model->user_city = $_REQUEST['user_path'];
        }
        $model->user_isAdmin = 0;
        $model->scenario     = 'register';
        if ($model->validate()) {
            if ($model->register()) {
                if ($_FILES) {
                    if (array_key_exists('file', $_FILES)) {
                        Yii::info('FILES:: ' . print_r($_FILES, 1), 'api');
                        if (!file_exists($dir . '/users/' . $model->id)) {
                            mkdir($dir . '/users/' . $model->id, 0777, true);
                        }
                        $file = '/users/' . $model->id . '/' . $_FILES['file']['name'];
                        Yii::info('FILE:: ' . $dir . $path . $file, 'api');
                        move_uploaded_file($_FILES['file']['tmp_name'], $dir . $file);
                        $model->user_photo = $path . $file;
                        $model->save();
                    }
                    //$model->user_photo = print_r($_FILES,1);//$file;
                }
                $response = Yii::$app->getResponse();
                $response->setStatusCode(201);
                return [ 'result' => 'ok' ];
            }
        }

        return [ 'result' => 'fail',
                 'message' => Utils::implodeRec($model->errors)
        ];
    }

    /**
     * Подготовка  текущего рейтинга игроков
     * #api - Get
     *
     * @uses   query  /api/user-x/rate-list?access-token=1
     *
     * @param int $cnt количество записей, которые нужно отрпавить
     *
     * @return \app\models\UserX[]|array
     */
    public function actionRateList($cnt = 25)
    {
        $users = Userx::find()
                      ->select(
                          'id,user_name,user_gender,user_birthday,user_photo,user_city,
                user_path,user_status,user_health,user_determination,
                user_communication,user_skill')
                      ->where([ 'user_isAdmin' => 0 ])
                      ->andWhere([ '!=', 'user_status', 'blocked' ])
                      ->limit($cnt)
                      ->orderBy('user_skill desc')
                      ->asArray()
                      ->all();
        return [ 'result' => 'ok', 'users' => $users ];
        //pp 1.7
    }

    /**
     * Подготовка  истории рейтинга игрока
     * #api - Get
     *
     * @uses   query  /api/user-x/rate-hist?access-token=1
     *
     * @param int $cnt $cnt количество записей, которые нужно отрпавить
     *
     * @return \app\models\RatingLog[]|array
     */
    public function actionRateHist($cnt = 25)
    {
        $count = RatingLog::find()
                          ->select('rate_sum,rate_created_at')
                          ->where([ 'user_id' => Yii::$app->user->id ])
                          ->orderBy('id')
                          ->count();
        if ($count === "0") {
            return [ 'result' => 'ok', 'rates' => [ ] ];
        }

        $rates = RatingLog::find()
                          ->select('rate_sum,rate_created_at')
                          ->where([ 'user_id' => Yii::$app->user->id ])
                          ->orderBy('id')
                          ->limit($cnt)
                          ->asArray()
                          ->all();
        if ($rates) {
            return [ 'result' => 'ok', 'rates' => $rates ];
        }
        return [ 'result' => 'fail', 'message' => 'db error' ];

    }

    /**
     * Подготовка списка отчетов игрока
     * #api - Get
     *
     * @uses   query  /api/user-x/play-reports?access-token=1
     *
     * @param int $cnt
     *
     * @return \app\models\Play[]|array
     */
    public function actionPlayReports($cnt = 25)
    {
        $plays = Play::find()
                     ->where([ 'or', 'user_id = ' . Yii::$app->user->id, 'user2_id = ' . Yii::$app->user->id ])
                     ->with('reports')
                     //->with('task')
                     //->with('user')
                     //->with('user2')
                     ->orderBy('id desc')
                     ->limit($cnt)
                     ->asArray()
                     ->all();
        return [ 'result' => 'ok', 'plays' => $plays ];
        //pp 1.6
    }

    /**
     * Подготовка списка отчетов по городу
     * #api - Get
     *
     * @uses   query  /api/user-x/play-reports-city?access-token=1&offset=1&cnt=2
     *
     * @param int $cnt
     * @param int $offset
     *
     * @return \app\models\Play[]|array
     */
    public function actionPlayReportsCity($offset = 0, $cnt = 25)
    {
        $user      = UserX::find()->me()->one();
        $userQuery = UserX::find()->select('id')->where([ 'user_city' => $user->user_city ]);
        $uQ = UserX::find()->select('id, user_name,username')->where([ 'user_city' => $user->user_city ])->asArray()->all();

        $plays     = Play::find()
                         ->where([ 'in', 'user_id', $userQuery ])
                         //->andWhere(['play_result'=>'success'])
                         ->with('reports')
                         ->orderBy('id asc')
                         ->offset($offset)
                         ->limit($cnt)
                         ->asArray()
                         ->all();
        $data=[];
        foreach($plays as $play){
            $photos=[];
            foreach($play['reports'] as $report){
                  $photos[]=['report_photo'=>$report['report_photo']];
            }
            $d=[ 'play_id' => $play['id'],
                  'task'=>$play['task_id'],

                  //'task_name2' => $play->task->task_name2,
                  //'task_description' => $play->task->task_description,
                  'user_id' => $play['user_id'],
                  'user2_id' => $play['user2_id'],
                  'photos' => $photos];
            $data[]=$d;
        }
        return [ 'result' => 'ok', 'plays' => $data ];
        //pp 1.6
    }

    /**
     * Сервер отправляет на клиент, ранее не отпраленные сообщения (новые)
     * Меняет статус соббщений на отправленные
     * #api - Get
     *
     * @link    http://islife/mobissa/ru/api/user-x/notifications?access-token=1
     * @api
     * @return \app\models\Message[]|array|\Exception
     * @throws \yii\db\Exception
     */
    public function actionNotifications()
    {

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $notifications = Message::find()
                                    ->select('id,message_header,message_text,created_at')
                                    ->where([
                                        'message_type' => 'inform',
                                        'user_id' => Yii::$app->user->id,
                                        'message_status' => 'new'
                                    ])->asArray()->all();
            Yii::$app->db->createCommand()
                         ->update(
                             'message',
                             [ 'message_status' => 'sent', 'send_at' => date('Y-m-d H:i:s') ],
                             [
                                 'message_type' => 'inform',
                                 'user_id' => Yii::$app->user->id,
                                 'message_status' => 'new'
                             ]
                         )->execute();
            $transaction->commit();
            return [ 'result' => 'ok', 'notifications' => $notifications ];
        } catch (\Exception $e) {
            $transaction->rollBack();
        }
        return [ 'result' => 'fail', 'message' => 'db error' ];
    }

    /**
     * Отправить Inform Сообщение игроку
     * #api - Post,[Get]
     *
     * @uses   /api/user-x/send-inform?access-token=1&message=qqqqq
     *
     * @param int $user_id
     * @param string $message
     *
     * @return array
     */
    public function actionSendInform($user_id = 0, $message = '')
    {
        if (!$user_id && array_key_exists('user_id', $_POST)) {
            $user_id = $_POST['user_id'];
        }
        if (!$message && array_key_exists('message', $_POST)) {
            $message = $_POST['message'];
        }

        if (!$user_id && !$message) {
            return [ 'result' => 'fail', 'message' => 'bad param' ];
        }
        $messenger = new Messenger();
        return $messenger->sendByTemplate(
            $user_id,
            'informMessage',
            [ 'message' => $message, 'user' => UserX::find()->me()->player()->one() ],
            false
        );
    }

    /**
     * Отправить Push сообщение игроку
     * #api - Post,[Get]
     *
     * @param int $user_id
     * @param string $message
     * @param bool|false $straight true для немедленного отправления, false - для отправки по расписани
     *
     * @return array
     */
    public function actionSendPush($user_id = 0, $message = '', $straight = false)
    {
        if (!$user_id && array_key_exists('user_id', $_POST)) {
            $user_id = $_POST['user_id'];
        }
        if (!$message && array_key_exists('message', $_POST)) {
            $message = $_POST['message'];
        }
        if (array_key_exists('straight', $_POST)) {
            $straight = $_POST['straight'];
        }
        if (!$user_id && !$message) {
            return [ 'result' => 'fail', 'message' => 'bad param' ];
        }
        $messenger = new Messenger();
        return $messenger->sendByTemplate(
            $user_id,
            'pushMessage',
            [ 'message' => $message, 'user' => Yii::$app->user ],
            $straight
        );
    }
}